Simple ev3 compiler
===================

Simple Pascal grammar [ev3pas.g](ev3/ev3pas.g)


```c++
if_stat <TIfStat:TStat> :
  "if"
  cond_expr:expr
  "then"
  then_stat:inner_stat
  (
     <silent>
     "else"
     else_stat:inner_stat
  )? ;

```

Generated parser [ev3pas_parser.py](description/ev3pas_parser.py)

```python
from lexer import Lexer

class Parser (Lexer) :
   def parse_if_stat (self) :
      result = TIfStat ()
      self.storeLocation (result)
      self.check ("if")
      result.cond_expr = self.parse_expr ()
      self.check ("then")
      result.then_stat = self.parse_inner_stat ()
      if self.tokenText == "else" :
         self.check ("else")
         result.else_stat = self.parse_inner_stat ()
      return result

```

Pascal program for EV3 [example.pas](ev3/examples/example.pas)

```pascal
Program Example;

Procedure Step (a: integer);
Var  timer: integer;
Begin
     OUTPUT_POWER (0, 1, 20*a);
     OUTPUT_START (0, 1);
     UI_WRITE (LED, a);

     TIMER_WAIT (500, timer);
     TIMER_READY (timer);

     OUTPUT_STOP (0, 1, 0);

     TIMER_WAIT (500, timer);
     TIMER_READY (timer);
End;

Var a, b, c: integer;
    timer: integer;

Begin
     a := 1;
     b := 7;
     c := a + b;

     for a := 1 to 3 do 
        Step (a);

     OUTPUT_STOP (0, 15, 0);
End.
```


Development environment
-----------------------

Program [view.py](view.py)

```
python view.py
```

Menu "EV3", menu item "Pascal Make"

![example.png](description/example.png)

Binary output
-------------

```
header: 4c 45 47 4f 
program size: 76 00 00 00 
bytecode version: 68 00 
number of objects: 02 00 
global bytes: 10 00 00 00 

proc step
offset to instruction: 52 00 00 00 
object id / trigger count: 00 00 01 00 
local bytes: 0c 00 00 00 
parameters: 01 82 

   param a, 4 bytes, position 0
   var timer, 4 bytes, position 4
   /* call */
   /* register LV(8) */
   0000 1a 14 40 48 MUL32 LC(20), LV(0)/*a*/, LV(8)
   0004 a4 00 01 48 OUTPUT_POWER LC(0), LC(1), LV(8)
   /* call */
   0008 a6 00 01 OUTPUT_START LC(0), LC(1)
   /* call */
   000b 82 1b 40 UI_WRITE LC(27), LV(0)/*a*/
   /* call */
   000e 85 82 f4 01 44 TIMER_WAIT LC(500), LV(4)/*timer*/
   /* call */
   0013 86 44 TIMER_READY LV(4)/*timer*/
   /* call */
   0015 a3 00 01 00 OUTPUT_STOP LC(0), LC(1), LC(0)
   /* call */
   0019 85 82 f4 01 44 TIMER_WAIT LC(500), LV(4)/*timer*/
   /* call */
   001e 86 44 TIMER_READY LV(4)/*timer*/
   0020 08 RETURN
   0021 0a OBJECT_END

var a, 4 bytes, position 0
var b, 4 bytes, position 4
var c, 4 bytes, position 8
var timer, 4 bytes, position 12
proc main
offset to instruction: 28 00 00 00 
object id / trigger count: 00 00 00 00 
local bytes: 04 00 00 00 

   /* assign */
   0000 3a 01 60 MOVE32_32 LC(1), GV(0)/*a*/
   /* assign */
   0003 3a 07 64 MOVE32_32 LC(7), GV(4)/*b*/
   /* assign */
   /* register LV(0) */
   0006 12 60 64 40 ADD32 GV(0)/*a*/, GV(4)/*b*/, LV(0)
   000a 3a 40 68 MOVE32_32 LV(0), GV(8)/*c*/
   /* for */
   000d 3a 01 60 MOVE32_32 LC(1), GV(0)/*a*/
   /* register LV(0) */
   0010 3a 03 40 MOVE32_32 LC(3), LV(0)
   label_1 :
   0013 6a 60 40    82 0c 00 JR_GT32 GV(0)/*a*/, LV(0), label_2
      /* call */
      0019 09 02 01 60 CALL LC(2), LC(1), GV(0)/*a*/
   001d 12 60 01 60 ADD32 GV(0)/*a*/, LC(1), GV(0)/*a*/
   0021 40 82 ee ff JR label_1
   label_2 :
   /* call */
   0025 a3 00 0f 00 OUTPUT_STOP LC(0), LC(15), LC(0)
   0029 0a OBJECT_END

4c 45 47 4f | 76 00 00 00 | 68 00 02 00 | 10 00 00 00 
28 00 00 00 | 00 00 00 00 | 04 00 00 00 | 52 00 00 00 
00 00 01 00 | 0c 00 00 00 | 3a 01 60 3a | 07 64 12 60 
64 40 3a 40 | 68 3a 01 60 | 3a 03 40 6a | 60 40 82 0c 
00 09 02 01 | 60 12 60 01 | 60 40 82 ee | ff a3 00 0f 
00 0a 01 82 | 1a 14 40 48 | a4 00 01 48 | a6 00 01 82 
1b 40 85 82 | f4 01 44 86 | 44 a3 00 01 | 00 85 82 f4 
01 44 86 44 | 08 0a 

```

C++ variant
-----------

Simple C++ grammar [ev3cmm.g](ev3/ev3cmm.g)

C++ program for EV3 [c4.cc](tutorial/c4.cc)


Fedora 25 packages
------------------

dnf install PyQt4 PyQt4-webkit pyusb


References
----------

http://github.com/mindboards/ev3sources/blob/master/lms2012/lms2012/source/bytecodes.h

