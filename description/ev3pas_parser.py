
from lexer import Lexer

class Parser (Lexer) :
   def parse_label_ident (self) :
      result = TLabelIdent ()
      self.storeLocation (result)
      result.id = self.readIdentifier ()
      return result

   def parse_param_type (self) :
      if self.token == self.identifier :
         result = self.parse_simple_alias_type ()
      elif self.tokenText == "string" :
         result = self.parse_simple_string_type ()
      elif self.tokenText == "file" :
         result = self.parse_simple_file_type ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_result_type (self) :
      if self.token == self.identifier :
         result = self.parse_simple_alias_type ()
      elif self.tokenText == "string" :
         result = self.parse_simple_string_type ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_simple_alias_type (self) :
      result = TAliasType ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      return result

   def parse_simple_string_type (self) :
      result = TStringType ()
      self.storeLocation (result)
      self.check ("string")
      return result

   def parse_simple_file_type (self) :
      result = TFileType ()
      self.storeLocation (result)
      self.check ("file")
      return result

   def parse_index_expr_item (self) :
      result = TInxItem ()
      self.storeLocation (result)
      result.ref = self.parse_expr ()
      return result

   def parse_index_expr_list (self) :
      result = TInxList ()
      self.storeLocation (result)
      result.items.append (self.parse_index_expr_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_index_expr_item ())
      return result

   def parse_arg_item (self) :
      result = TArgItem ()
      self.storeLocation (result)
      result.value = self.parse_expr ()
      if self.tokenText == ":" :
         self.check (":")
         result.width = self.parse_expr ()
         if self.tokenText == ":" :
            self.check (":")
            result.digits = self.parse_expr ()
      return result

   def parse_arg_list (self) :
      result = TArgSect ()
      self.storeLocation (result)
      result.items.append (self.parse_arg_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_arg_item ())
      return result

   def parse_elem_sect (self) :
      result = TElemSect ()
      self.storeLocation (result)
      result.items.append (self.parse_elem_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_elem_item ())
      return result

   def parse_elem_item (self) :
      result = TElemItem ()
      self.storeLocation (result)
      result.low = self.parse_expr ()
      if self.tokenText == ".." :
         self.check ("..")
         result.high = self.parse_expr ()
      return result

   def parse_ident_expr (self) :
      result = TIdentExpr ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_ident_expr (result)
      return result

   def parse_int_value_expr (self) :
      result = TIntValueExpr ()
      self.storeLocation (result)
      result.value = self.readNumber ()
      return result

   def parse_float_value_expr (self) :
      result = TFltValueExpr ()
      self.storeLocation (result)
      result.value = self.readReal ()
      return result

   def parse_string_value_expr (self) :
      result = TStrValueExpr ()
      self.storeLocation (result)
      result.value = self.readString ()
      return result

   def parse_nil_expr (self) :
      result = TNilExpr ()
      self.storeLocation (result)
      self.check ("nil")
      return result

   def parse_sub_expr (self) :
      result = TSubExpr ()
      self.storeLocation (result)
      self.check ("(")
      result.value = self.parse_expr ()
      self.check (")")
      return result

   def parse_not_expr (self) :
      result = TNotExpr ()
      self.storeLocation (result)
      self.check ("not")
      result.param = self.parse_factor ()
      return result

   def parse_plus_expr (self) :
      result = TPlusExpr ()
      self.storeLocation (result)
      self.check ("+")
      result.param = self.parse_factor ()
      return result

   def parse_minus_expr (self) :
      result = TMinusExpr ()
      self.storeLocation (result)
      self.check ("-")
      result.param = self.parse_factor ()
      return result

   def parse_adr_expr (self) :
      result = TAdrExpr ()
      self.storeLocation (result)
      self.check ("@")
      result.param = self.parse_factor ()
      return result

   def parse_set_expr (self) :
      result = TSetExpr ()
      self.storeLocation (result)
      self.check ("[")
      result.list = self.parse_elem_sect ()
      self.check ("]")
      return result

   def parse_string_type_expr (self) :
      result = TStringExpr ()
      self.storeLocation (result)
      self.check ("string")
      self.check ("(")
      result.param = self.parse_expr ()
      self.check (")")
      return result

   def parse_variable (self) :
      if self.token == self.identifier :
         result = self.parse_ident_expr ()
      elif self.tokenText == "(" :
         result = self.parse_sub_expr ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_reference (self) :
      result = self.parse_variable ()
      while self.tokenText == "[" or self.tokenText == "(" or self.tokenText == "^" or self.tokenText == "." :
         if self.tokenText == "[" :
            result = self.parse_index_expr (result)
         elif self.tokenText == "(" :
            result = self.parse_call_expr (result)
         elif self.tokenText == "^" :
            result = self.parse_deref_expr (result)
         elif self.tokenText == "." :
            result = self.parse_field_expr (result)
         else :
            self.error ("Unexpected token")
      return result

   def parse_index_expr (self, store) :
      result = TIndexExpr ()
      self.storeLocation (result)
      result.left = store
      self.check ("[")
      result.list = self.parse_index_expr_list ()
      self.check ("]")
      return result

   def parse_call_expr (self, store) :
      result = TCallExpr ()
      self.storeLocation (result)
      result.func = store
      self.check ("(")
      result.list = self.parse_arg_list ()
      self.check (")")
      return result

   def parse_deref_expr (self, store) :
      result = TDerefExpr ()
      self.storeLocation (result)
      result.param = store
      self.check ("^")
      return result

   def parse_field_expr (self, store) :
      result = TFieldExpr ()
      self.storeLocation (result)
      result.param = store
      self.check (".")
      result.name = self.readIdentifier ()
      self.on_field_expr (result)
      return result

   def parse_factor (self) :
      if self.token == self.number :
         result = self.parse_int_value_expr ()
      elif self.token == self.real_number :
         result = self.parse_float_value_expr ()
      elif self.token == self.string_literal :
         result = self.parse_string_value_expr ()
      elif self.tokenText == "not" :
         result = self.parse_not_expr ()
      elif self.tokenText == "+" :
         result = self.parse_plus_expr ()
      elif self.tokenText == "-" :
         result = self.parse_minus_expr ()
      elif self.tokenText == "@" :
         result = self.parse_adr_expr ()
      elif self.tokenText == "nil" :
         result = self.parse_nil_expr ()
      elif self.tokenText == "[" :
         result = self.parse_set_expr ()
      elif self.tokenText == "string" and self.test_1 () :
         result = self.parse_string_type_expr ()
      elif self.token == self.identifier or self.tokenText == "(" :
         result = self.parse_reference ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_term (self) :
      result = self.parse_factor ()
      while self.set_0 [self.token] :
         store = result
         result = TTermExpr ()
         self.storeLocation (result)
         result.left = store
         if self.tokenText == "*" :
            self.check ("*")
            result.fce = result.MulExp
         elif self.tokenText == "/" :
            self.check ("/")
            result.fce = result.RDivExp
         elif self.tokenText == "div" :
            self.check ("div")
            result.fce = result.DivExp
         elif self.tokenText == "mod" :
            self.check ("mod")
            result.fce = result.ModExp
         elif self.tokenText == "shl" :
            self.check ("shl")
            result.fce = result.ShlExp
         elif self.tokenText == "shr" :
            self.check ("shr")
            result.fce = result.ShrExp
         elif self.tokenText == "and" :
            self.check ("and")
            result.fce = result.AndExp
         else :
            self.error ("Unexpected token")
         result.right = self.parse_factor ()
      return result

   def parse_simple_expr (self) :
      result = self.parse_term ()
      while self.set_1 [self.token] :
         store = result
         result = TSimpleExpr ()
         self.storeLocation (result)
         result.left = store
         if self.tokenText == "+" :
            self.check ("+")
            result.fce = result.AddExp
         elif self.tokenText == "-" :
            self.check ("-")
            result.fce = result.SubExp
         elif self.tokenText == "or" :
            self.check ("or")
            result.fce = result.OrExp
         elif self.tokenText == "xor" :
            self.check ("xor")
            result.fce = result.XorExp
         else :
            self.error ("Unexpected token")
         result.right = self.parse_term ()
      return result

   def parse_expr (self) :
      result = self.parse_simple_expr ()
      if self.set_2 [self.token] :
         store = result
         result = TCompleteExpr ()
         self.storeLocation (result)
         result.left = store
         if self.tokenText == "=" :
            self.check ("=")
            result.fce = result.EqExp
         elif self.tokenText == "<>" :
            self.check ("<>")
            result.fce = result.NeExp
         elif self.tokenText == "<" :
            self.check ("<")
            result.fce = result.LtExp
         elif self.tokenText == ">" :
            self.check (">")
            result.fce = result.GtExp
         elif self.tokenText == "<=" :
            self.check ("<=")
            result.fce = result.LeExp
         elif self.tokenText == ">=" :
            self.check (">=")
            result.fce = result.GeExp
         elif self.tokenText == "in" :
            self.check ("in")
            result.fce = result.InExp
         else :
            self.error ("Unexpected token")
         result.right = self.parse_simple_expr ()
      return result

   def parse_goto_stat (self) :
      result = TGotoStat ()
      self.storeLocation (result)
      self.check ("goto")
      result.goto_lab = self.parse_label_ident ()
      return result

   def parse_begin_stat (self) :
      result = TBlockStat ()
      self.storeLocation (result)
      self.check ("begin")
      result.body_seq = self.parse_stat_list ()
      self.check ("end")
      return result

   def parse_stat_list (self) :
      result = TStatSeq ()
      self.storeLocation (result)
      result.items.append (self.parse_stat ())
      while self.tokenText == ";" :
         self.check (";")
         result.items.append (self.parse_stat ())
      return result

   def parse_inner_stat (self) :
      result = self.parse_stat ()
      return result

   def parse_if_stat (self) :
      result = TIfStat ()
      self.storeLocation (result)
      self.check ("if")
      result.cond_expr = self.parse_expr ()
      self.check ("then")
      result.then_stat = self.parse_inner_stat ()
      if self.tokenText == "else" :
         self.check ("else")
         result.else_stat = self.parse_inner_stat ()
      return result

   def parse_case_stat (self) :
      result = TCaseStat ()
      self.storeLocation (result)
      self.check ("case")
      result.case_expr = self.parse_expr ()
      self.check ("of")
      result.case_list = self.parse_case_sect ()
      if self.tokenText == "else" :
         self.check ("else")
         result.else_seq = self.parse_stat_list ()
      self.check ("end")
      return result

   def parse_case_sect (self) :
      result = TCaseSect ()
      self.storeLocation (result)
      result.items.append (self.parse_case_item ())
      while self.tokenText == ";" :
         self.check (";")
         result.items.append (self.parse_case_item ())
      return result

   def parse_case_item (self) :
      result = TCaseItem ()
      self.storeLocation (result)
      result.sel_list = self.parse_elem_sect ()
      self.check (":")
      result.sel_stat = self.parse_inner_stat ()
      return result

   def parse_while_stat (self) :
      result = TWhileStat ()
      self.storeLocation (result)
      self.check ("while")
      result.cond_expr = self.parse_expr ()
      self.check ("do")
      result.body_stat = self.parse_inner_stat ()
      return result

   def parse_repeat_stat (self) :
      result = TRepeatStat ()
      self.storeLocation (result)
      self.check ("repeat")
      result.body_seq = self.parse_stat_list ()
      self.check ("until")
      result.until_expr = self.parse_expr ()
      return result

   def parse_for_stat (self) :
      result = TForStat ()
      self.storeLocation (result)
      self.check ("for")
      result.var_expr = self.parse_variable ()
      self.check (":=")
      result.from_expr = self.parse_expr ()
      if self.tokenText == "to" :
         self.check ("to")
         result.incr = True
      elif self.tokenText == "downto" :
         self.check ("downto")
         result.incr = False
      else :
         self.error ("Unexpected token")
      result.to_expr = self.parse_expr ()
      self.check ("do")
      result.body_stat = self.parse_inner_stat ()
      return result

   def parse_with_stat (self) :
      result = TWithStat ()
      self.storeLocation (result)
      self.check ("with")
      result.with_list = self.parse_with_sect ()
      self.check ("do")
      result.body_stat = self.parse_inner_stat ()
      return result

   def parse_with_sect (self) :
      result = TWithSect ()
      self.storeLocation (result)
      result.items.append (self.parse_with_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_with_item ())
      return result

   def parse_with_item (self) :
      result = TWithItem ()
      self.storeLocation (result)
      result.expr = self.parse_variable ()
      return result

   def parse_empty_stat (self) :
      result = TEmptyStat ()
      self.storeLocation (result)
      return result

   def parse_simple_stat (self) :
      result = self.parse_expr ()
      if self.tokenText == ":=" :
         result = self.parse_assign_stat (result)
      elif True :
         result = self.parse_call_stat (result)
      else :
         self.error ("Unexpected token")
      return result

   def parse_assign_stat (self, store) :
      result = TAssignStat ()
      self.storeLocation (result)
      result.left_expr = store
      self.check (":=")
      result.right_expr = self.parse_expr ()
      return result

   def parse_call_stat (self, store) :
      result = TCallStat ()
      self.storeLocation (result)
      result.call_expr = store
      return result

   def parse_labeled_stat (self) :
      result = TLabeledTStat ()
      self.storeLocation (result)
      result.lab = self.parse_label_ident ()
      self.check (":")
      result.body = self.parse_stat ()
      return result

   def parse_stat (self) :
      if self.tokenText == "goto" :
         result = self.parse_goto_stat ()
      elif self.tokenText == "begin" :
         result = self.parse_begin_stat ()
      elif self.tokenText == "if" :
         result = self.parse_if_stat ()
      elif self.tokenText == "case" :
         result = self.parse_case_stat ()
      elif self.tokenText == "while" :
         result = self.parse_while_stat ()
      elif self.tokenText == "repeat" :
         result = self.parse_repeat_stat ()
      elif self.tokenText == "for" :
         result = self.parse_for_stat ()
      elif self.tokenText == "with" :
         result = self.parse_with_stat ()
      elif self.set_3 [self.token] :
         result = self.parse_simple_stat ()
      elif True :
         result = self.parse_empty_stat ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_param_ident (self) :
      result = TParamIdent ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_param (result)
      return result

   def parse_param_item (self) :
      result = TParamItem ()
      self.storeLocation (result)
      if self.tokenText == "var" :
         self.check ("var")
         result.mode = result.VarParam
      elif self.tokenText == "const" :
         self.check ("const")
         result.mode = result.ConstParam
      elif self.tokenText == "out" :
         self.check ("out")
         result.mode = result.OutParam
      elif True :
         result.mode = result.ValueParam
      else :
         self.error ("Unexpected token")
      result.items.append (self.parse_param_ident ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_param_ident ())
      if self.tokenText == ":" :
         self.check (":")
         result.typ = self.parse_param_type ()
         if self.tokenText == "=" :
            self.check ("=")
            result.ini = self.parse_expr ()
      self.add_param (result)
      return result

   def parse_formal_param_list (self) :
      result = TParamSect ()
      self.storeLocation (result)
      if self.tokenText == "(" :
         self.check ("(")
         result.items.append (self.parse_param_item ())
         while self.tokenText == ";" :
            self.check (";")
            result.items.append (self.parse_param_item ())
         self.check (")")
      return result

   def parse_enum_type (self) :
      result = TEnumType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("(")
      result.elements = self.parse_enum_sect ()
      self.check (")")
      return result

   def parse_enum_sect (self) :
      result = TEnumSect ()
      self.storeLocation (result)
      result.items.append (self.parse_enum_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_enum_item ())
      return result

   def parse_enum_item (self) :
      result = TEnumItem ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      return result

   def parse_string_type (self) :
      result = TStringType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("string")
      return result

   def parse_array_type (self) :
      result = TArrayType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("array")
      self.check ("[")
      result.index_list = self.parse_index_type_sect ()
      self.check ("]")
      self.check ("of")
      result.elem = self.parse_type ()
      return result

   def parse_index_type_sect (self) :
      result = TIndexTypeSect ()
      self.storeLocation (result)
      result.items.append (self.parse_index_type_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_index_type_item ())
      return result

   def parse_index_type_item (self) :
      result = TIndexTypeItem ()
      self.storeLocation (result)
      result.index = self.parse_type ()
      return result

   def parse_record_type (self) :
      result = TRecordType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("record")
      while self.token == self.identifier :
         result.items.append (self.parse_field_decl ())
      self.check ("end")
      return result

   def parse_field_ident (self) :
      result = TFieldItem ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_field (result)
      return result

   def parse_field_decl (self) :
      result = TFieldDecl ()
      self.storeLocation (result)
      result.items.append (self.parse_field_ident ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_field_ident ())
      self.check (":")
      result.typ = self.parse_type ()
      self.check (";")
      self.add_field (result)
      return result

   def parse_pointer_type (self) :
      result = TPointerType ()
      self.storeLocation (result)
      self.check ("^")
      result.elem = self.parse_type ()
      return result

   def parse_set_type (self) :
      result = TSetType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("set")
      self.check ("of")
      result.elem = self.parse_type ()
      return result

   def parse_file_type (self) :
      result = TFileType ()
      self.storeLocation (result)
      self.simple_type (result)
      self.check ("file")
      if self.tokenText == "of" :
         self.check ("of")
         result.elem = self.parse_type ()
      return result

   def parse_range_type (self) :
      result = TSubrangeType ()
      self.storeLocation (result)
      self.simple_type (result)
      result.low = self.parse_simple_expr ()
      self.check ("..")
      result.high = self.parse_simple_expr ()
      return result

   def parse_alias_type (self) :
      result = TAliasType ()
      self.storeLocation (result)
      self.simple_type (result)
      result.name = self.readIdentifier ()
      return result

   def parse_type (self) :
      if self.tokenText == "string" :
         result = self.parse_string_type ()
      elif self.tokenText == "array" :
         result = self.parse_array_type ()
      elif self.tokenText == "set" :
         result = self.parse_set_type ()
      elif self.tokenText == "file" :
         result = self.parse_file_type ()
      elif self.tokenText == "record" :
         result = self.parse_record_type ()
      elif self.tokenText == "^" :
         result = self.parse_pointer_type ()
      elif self.set_3 [self.token] and self.test_2 () :
         result = self.parse_range_type ()
      elif self.tokenText == "(" and self.test_3 () :
         result = self.parse_enum_type ()
      elif self.token == self.identifier :
         result = self.parse_alias_type ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_label_decl (self) :
      result = TLabelDecl ()
      self.storeLocation (result)
      result.name = self.parse_label_ident ()
      return result

   def parse_label_sect (self) :
      result = TLabelSect ()
      self.storeLocation (result)
      self.check ("label")
      result.items.append (self.parse_label_decl ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_label_decl ())
      self.check (";")
      return result

   def parse_const_decl (self) :
      result = TConstDecl ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_const (result)
      if self.tokenText == ":" :
         self.check (":")
         result.typ = self.parse_type ()
      self.check ("=")
      result.val = self.parse_expr ()
      self.check (";")
      self.add_const (result)
      return result

   def parse_const_sect (self) :
      result = TConstSect ()
      self.storeLocation (result)
      self.check ("const")
      result.items.append (self.parse_const_decl ())
      while self.token == self.identifier :
         result.items.append (self.parse_const_decl ())
      return result

   def parse_type_decl (self) :
      result = TTypeDecl ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_type (result)
      self.check ("=")
      result.typ = self.parse_type ()
      self.check (";")
      self.add_type (result)
      return result

   def parse_type_sect (self) :
      result = TTypeSect ()
      self.storeLocation (result)
      self.check ("type")
      result.items.append (self.parse_type_decl ())
      while self.token == self.identifier :
         result.items.append (self.parse_type_decl ())
      return result

   def parse_var_item (self) :
      result = TVarItem ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_var (result)
      return result

   def parse_var_decl (self) :
      result = TVarDecl ()
      self.storeLocation (result)
      result.items.append (self.parse_var_item ())
      while self.tokenText == "," :
         self.check (",")
         result.items.append (self.parse_var_item ())
      self.check (":")
      result.typ = self.parse_type ()
      if self.tokenText == "=" :
         self.check ("=")
         result.ini = self.parse_expr ()
      self.check (";")
      self.add_var (result)
      return result

   def parse_var_sect (self) :
      result = TVarSect ()
      self.storeLocation (result)
      self.check ("var")
      result.items.append (self.parse_var_decl ())
      while self.token == self.identifier :
         result.items.append (self.parse_var_decl ())
      return result

   def parse_proc_head (self, result) :
      if self.tokenText == "procedure" :
         self.check ("procedure")
         result.style = result.ProcedureStyle
      elif self.tokenText == "function" :
         self.check ("function")
         result.style = result.FunctionStyle
      else :
         self.error ("Unexpected token")
      result.name = self.readIdentifier ()
      self.on_proc (result)
      result.param_list = self.parse_formal_param_list ()
      if self.tokenText == ":" :
         self.check (":")
         result.answer = self.parse_result_type ()
      self.check (";")
      return result

   def parse_proc_decl (self) :
      result = TProcDecl ()
      self.storeLocation (result)
      self.open_proc (result)
      self.parse_proc_head (result)
      if self.tokenText == "forward" :
         self.check ("forward")
         result.a_forward = True
      elif self.set_4 [self.token] :
         result.local = self.parse_local_decl_part ()
         self.on_begin_proc (result)
         self.check ("begin")
         result.body = self.parse_stat_list ()
         self.on_end_proc (result)
         self.check ("end")
      else :
         self.error ("Unexpected token")
      self.check (";")
      self.close_proc (result)
      return result

   def parse_proc_intf_decl (self) :
      result = TProcDecl ()
      self.storeLocation (result)
      self.open_proc (result)
      self.parse_proc_head (result)
      self.close_proc (result)
      return result

   def parse_intf_decl (self) :
      if self.tokenText == "const" :
         result = self.parse_const_sect ()
      elif self.tokenText == "type" :
         result = self.parse_type_sect ()
      elif self.tokenText == "var" :
         result = self.parse_var_sect ()
      elif self.tokenText == "function" or self.tokenText == "procedure" :
         result = self.parse_proc_intf_decl ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_intf_decl_part (self) :
      result = TDeclGroup ()
      self.storeLocation (result)
      while self.set_5 [self.token] :
         result.items.append (self.parse_intf_decl ())
      return result

   def parse_local_decl (self) :
      if self.tokenText == "label" :
         result = self.parse_label_sect ()
      elif self.tokenText == "const" :
         result = self.parse_const_sect ()
      elif self.tokenText == "type" :
         result = self.parse_type_sect ()
      elif self.tokenText == "var" :
         result = self.parse_var_sect ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_local_decl_part (self) :
      result = TDeclGroup ()
      self.storeLocation (result)
      while self.set_6 [self.token] :
         result.items.append (self.parse_local_decl ())
      return result

   def parse_decl (self) :
      if self.tokenText == "label" :
         result = self.parse_label_sect ()
      elif self.tokenText == "const" :
         result = self.parse_const_sect ()
      elif self.tokenText == "type" :
         result = self.parse_type_sect ()
      elif self.tokenText == "var" :
         result = self.parse_var_sect ()
      elif self.tokenText == "function" or self.tokenText == "procedure" :
         result = self.parse_proc_decl ()
      else :
         self.error ("Unexpected token")
      return result

   def parse_decl_part (self) :
      result = TDeclGroup ()
      self.storeLocation (result)
      while self.set_7 [self.token] :
         result.items.append (self.parse_decl ())
      return result

   def parse_import_item (self) :
      result = TImportItem ()
      self.storeLocation (result)
      result.name = self.readIdentifier ()
      self.on_import (result)
      return result

   def parse_import_sect (self) :
      result = TImportSect ()
      self.storeLocation (result)
      if self.tokenText == "uses" :
         self.check ("uses")
         result.items.append (self.parse_import_item ())
         while self.tokenText == "," :
            self.check (",")
            result.items.append (self.parse_import_item ())
         self.check (";")
      return result

   def parse_unit_decl (self) :
      result = TUnitModule ()
      self.storeLocation (result)
      self.open_module (result)
      self.check ("unit")
      result.name = self.readIdentifier ()
      self.on_module (result)
      self.check (";")
      self.check ("interface")
      result.intf_imports = self.parse_import_sect ()
      result.intf_decl = self.parse_intf_decl_part ()
      self.check ("implementation")
      result.impl_imports = self.parse_import_sect ()
      result.impl_decl = self.parse_decl_part ()
      if self.tokenText == "begin" :
         self.check ("begin")
         result.init = self.parse_stat_list ()
      self.check ("end")
      self.check (".")
      self.close_module (result)
      return result

   def parse_program_decl (self) :
      result = TProgramModule ()
      self.storeLocation (result)
      self.open_module (result)
      self.check ("program")
      result.name = self.readIdentifier ()
      self.on_module (result)
      self.check (";")
      result.impl_imports = self.parse_import_sect ()
      result.impl_decl = self.parse_decl_part ()
      self.check ("begin")
      result.init = self.parse_stat_list ()
      self.check ("end")
      self.check (".")
      self.close_module (result)
      return result

   def parse_module_decl (self) :
      if self.tokenText == "unit" :
         result = self.parse_unit_decl ()
      elif self.tokenText == "program" :
         result = self.parse_program_decl ()
      else :
         self.error ("Unexpected token")
      return result

   def test_1 (self) :
      result = True
      position = self.mark ()
      if result and self.tokenText != "string" :
         result = False
      if result :
         self.nextToken ()
      if result and self.tokenText != "(" :
         result = False
      if result :
         self.nextToken ()
      self.rewind (position)
      return result

   def test_2 (self) :
      result = True
      position = self.mark ()
      if result :
         if not self.test_range_type () :
            result = False
      self.rewind (position)
      return result

   def test_3 (self) :
      result = True
      position = self.mark ()
      if result :
         if not self.test_enum_type () :
            result = False
      self.rewind (position)
      return result

   def test_range_type (self) :
      if not self.test_simple_expr () :
         return False
      if self.tokenText != ".." :
         return False
      self.nextToken ()
      if not self.test_simple_expr () :
         return False
      return True

   def test_enum_type (self) :
      if self.tokenText != "(" :
         return False
      self.nextToken ()
      if not self.test_enum_sect () :
         return False
      if self.tokenText != ")" :
         return False
      self.nextToken ()
      return True

   def test_simple_expr (self) :
      if not self.test_term () :
         return False
      while self.set_1 [self.token] :
         if self.tokenText == "+" :
            self.nextToken ()
         elif self.tokenText == "-" :
            self.nextToken ()
         elif self.tokenText == "or" :
            self.nextToken ()
         elif self.tokenText == "xor" :
            self.nextToken ()
         else :
            return False
         if not self.test_term () :
            return False
      return True

   def test_enum_sect (self) :
      if not self.test_enum_item () :
         return False
      while self.tokenText == "," :
         self.nextToken ()
         if not self.test_enum_item () :
            return False
      return True

   def test_term (self) :
      if not self.test_factor () :
         return False
      while self.set_0 [self.token] :
         if self.tokenText == "*" :
            self.nextToken ()
         elif self.tokenText == "/" :
            self.nextToken ()
         elif self.tokenText == "div" :
            self.nextToken ()
         elif self.tokenText == "mod" :
            self.nextToken ()
         elif self.tokenText == "shl" :
            self.nextToken ()
         elif self.tokenText == "shr" :
            self.nextToken ()
         elif self.tokenText == "and" :
            self.nextToken ()
         else :
            return False
         if not self.test_factor () :
            return False
      return True

   def test_enum_item (self) :
      if self.token != self.identifier :
         return False
      self.nextToken ()
      return True

   def test_factor (self) :
      if self.token == self.number :
         if not self.test_int_value_expr () :
            return False
      elif self.token == self.real_number :
         if not self.test_float_value_expr () :
            return False
      elif self.token == self.string_literal :
         if not self.test_string_value_expr () :
            return False
      elif self.tokenText == "not" :
         if not self.test_not_expr () :
            return False
      elif self.tokenText == "+" :
         if not self.test_plus_expr () :
            return False
      elif self.tokenText == "-" :
         if not self.test_minus_expr () :
            return False
      elif self.tokenText == "@" :
         if not self.test_adr_expr () :
            return False
      elif self.tokenText == "nil" :
         if not self.test_nil_expr () :
            return False
      elif self.tokenText == "[" :
         if not self.test_set_expr () :
            return False
      elif self.tokenText == "string" and self.test_1 () :
         if not self.test_string_type_expr () :
            return False
      elif self.token == self.identifier or self.tokenText == "(" :
         if not self.test_reference () :
            return False
      else :
         return False
      return True

   def test_int_value_expr (self) :
      if self.token != self.number :
         return False
      self.nextToken ()
      return True

   def test_float_value_expr (self) :
      if self.token != self.real_number :
         return False
      self.nextToken ()
      return True

   def test_string_value_expr (self) :
      if self.token != self.string_literal :
         return False
      self.nextToken ()
      return True

   def test_not_expr (self) :
      if self.tokenText != "not" :
         return False
      self.nextToken ()
      if not self.test_factor () :
         return False
      return True

   def test_plus_expr (self) :
      if self.tokenText != "+" :
         return False
      self.nextToken ()
      if not self.test_factor () :
         return False
      return True

   def test_minus_expr (self) :
      if self.tokenText != "-" :
         return False
      self.nextToken ()
      if not self.test_factor () :
         return False
      return True

   def test_adr_expr (self) :
      if self.tokenText != "@" :
         return False
      self.nextToken ()
      if not self.test_factor () :
         return False
      return True

   def test_nil_expr (self) :
      if self.tokenText != "nil" :
         return False
      self.nextToken ()
      return True

   def test_set_expr (self) :
      if self.tokenText != "[" :
         return False
      self.nextToken ()
      if not self.test_elem_sect () :
         return False
      if self.tokenText != "]" :
         return False
      self.nextToken ()
      return True

   def test_string_type_expr (self) :
      if self.tokenText != "string" :
         return False
      self.nextToken ()
      if self.tokenText != "(" :
         return False
      self.nextToken ()
      if not self.test_expr () :
         return False
      if self.tokenText != ")" :
         return False
      self.nextToken ()
      return True

   def test_reference (self) :
      if not self.test_variable () :
         return False
      while self.tokenText == "[" or self.tokenText == "(" or self.tokenText == "^" or self.tokenText == "." :
         if self.tokenText == "[" :
            if not self.test_index_expr () :
               return False
         elif self.tokenText == "(" :
            if not self.test_call_expr () :
               return False
         elif self.tokenText == "^" :
            if not self.test_deref_expr () :
               return False
         elif self.tokenText == "." :
            if not self.test_field_expr () :
               return False
         else :
            return False
      return True

   def test_elem_sect (self) :
      if not self.test_elem_item () :
         return False
      while self.tokenText == "," :
         self.nextToken ()
         if not self.test_elem_item () :
            return False
      return True

   def test_expr (self) :
      if not self.test_simple_expr () :
         return False
      if self.set_2 [self.token] :
         if self.tokenText == "=" :
            self.nextToken ()
         elif self.tokenText == "<>" :
            self.nextToken ()
         elif self.tokenText == "<" :
            self.nextToken ()
         elif self.tokenText == ">" :
            self.nextToken ()
         elif self.tokenText == "<=" :
            self.nextToken ()
         elif self.tokenText == ">=" :
            self.nextToken ()
         elif self.tokenText == "in" :
            self.nextToken ()
         else :
            return False
         if not self.test_simple_expr () :
            return False
      return True

   def test_variable (self) :
      if self.token == self.identifier :
         if not self.test_ident_expr () :
            return False
      elif self.tokenText == "(" :
         if not self.test_sub_expr () :
            return False
      else :
         return False
      return True

   def test_index_expr (self) :
      if self.tokenText != "[" :
         return False
      self.nextToken ()
      if not self.test_index_expr_list () :
         return False
      if self.tokenText != "]" :
         return False
      self.nextToken ()
      return True

   def test_call_expr (self) :
      if self.tokenText != "(" :
         return False
      self.nextToken ()
      if not self.test_arg_list () :
         return False
      if self.tokenText != ")" :
         return False
      self.nextToken ()
      return True

   def test_deref_expr (self) :
      if self.tokenText != "^" :
         return False
      self.nextToken ()
      return True

   def test_field_expr (self) :
      if self.tokenText != "." :
         return False
      self.nextToken ()
      if self.token != self.identifier :
         return False
      self.nextToken ()
      return True

   def test_elem_item (self) :
      if not self.test_expr () :
         return False
      if self.tokenText == ".." :
         self.nextToken ()
         if not self.test_expr () :
            return False
      return True

   def test_ident_expr (self) :
      if self.token != self.identifier :
         return False
      self.nextToken ()
      return True

   def test_sub_expr (self) :
      if self.tokenText != "(" :
         return False
      self.nextToken ()
      if not self.test_expr () :
         return False
      if self.tokenText != ")" :
         return False
      self.nextToken ()
      return True

   def test_index_expr_list (self) :
      if not self.test_index_expr_item () :
         return False
      while self.tokenText == "," :
         self.nextToken ()
         if not self.test_index_expr_item () :
            return False
      return True

   def test_arg_list (self) :
      if not self.test_arg_item () :
         return False
      while self.tokenText == "," :
         self.nextToken ()
         if not self.test_arg_item () :
            return False
      return True

   def test_index_expr_item (self) :
      if not self.test_expr () :
         return False
      return True

   def test_arg_item (self) :
      if not self.test_expr () :
         return False
      if self.tokenText == ":" :
         self.nextToken ()
         if not self.test_expr () :
            return False
         if self.tokenText == ":" :
            self.nextToken ()
            if not self.test_expr () :
               return False
      return True

   def lookupKeyword (self) :
      s = self.tokenText
      n = len (s)
      if n == 2 :
         if s[0] == 'd' :
            if s[1] == 'o' :
               self.token = self.keyword_do
         elif s[0] == 'i' :
            if s[1] == 'f' :
               self.token = self.keyword_if
            elif s[1] == 'n' :
               self.token = self.keyword_in
         elif s[0] == 'o' :
            if s[1] == 'f' :
               self.token = self.keyword_of
            elif s[1] == 'r' :
               self.token = self.keyword_or
         elif s[0] == 't' :
            if s[1] == 'o' :
               self.token = self.keyword_to
      elif n == 3 :
         if s[0] == 'a' :
            if s[1:3] == "nd" :
               self.token = self.keyword_and
         elif s[0] == 'd' :
            if s[1:3] == "iv" :
               self.token = self.keyword_div
         elif s[0] == 'e' :
            if s[1:3] == "nd" :
               self.token = self.keyword_end
         elif s[0] == 'f' :
            if s[1:3] == "or" :
               self.token = self.keyword_for
         elif s[0] == 'm' :
            if s[1:3] == "od" :
               self.token = self.keyword_mod
         elif s[0] == 'n' :
            if s[1] == 'i' :
               if s[2] == 'l' :
                  self.token = self.keyword_nil
            elif s[1] == 'o' :
               if s[2] == 't' :
                  self.token = self.keyword_not
         elif s[0] == 'o' :
            if s[1:3] == "ut" :
               self.token = self.keyword_out
         elif s[0] == 's' :
            if s[1] == 'e' :
               if s[2] == 't' :
                  self.token = self.keyword_set
            elif s[1] == 'h' :
               if s[2] == 'l' :
                  self.token = self.keyword_shl
               elif s[2] == 'r' :
                  self.token = self.keyword_shr
         elif s[0] == 'v' :
            if s[1:3] == "ar" :
               self.token = self.keyword_var
         elif s[0] == 'x' :
            if s[1:3] == "or" :
               self.token = self.keyword_xor
      elif n == 4 :
         if s[0] == 'c' :
            if s[1:4] == "ase" :
               self.token = self.keyword_case
         elif s[0] == 'e' :
            if s[1:4] == "lse" :
               self.token = self.keyword_else
         elif s[0] == 'f' :
            if s[1:4] == "ile" :
               self.token = self.keyword_file
         elif s[0] == 'g' :
            if s[1:4] == "oto" :
               self.token = self.keyword_goto
         elif s[0] == 't' :
            if s[1] == 'h' :
               if s[2:4] == "en" :
                  self.token = self.keyword_then
            elif s[1] == 'y' :
               if s[2:4] == "pe" :
                  self.token = self.keyword_type
         elif s[0] == 'u' :
            if s[1] == 'n' :
               if s[2:4] == "it" :
                  self.token = self.keyword_unit
            elif s[1] == 's' :
               if s[2:4] == "es" :
                  self.token = self.keyword_uses
         elif s[0] == 'w' :
            if s[1:4] == "ith" :
               self.token = self.keyword_with
      elif n == 5 :
         if s[0] == 'a' :
            if s[1:5] == "rray" :
               self.token = self.keyword_array
         elif s[0] == 'b' :
            if s[1:5] == "egin" :
               self.token = self.keyword_begin
         elif s[0] == 'c' :
            if s[1:5] == "onst" :
               self.token = self.keyword_const
         elif s[0] == 'l' :
            if s[1:5] == "abel" :
               self.token = self.keyword_label
         elif s[0] == 'u' :
            if s[1:5] == "ntil" :
               self.token = self.keyword_until
         elif s[0] == 'w' :
            if s[1:5] == "hile" :
               self.token = self.keyword_while
      elif n == 6 :
         if s[0] == 'd' :
            if s[1:6] == "ownto" :
               self.token = self.keyword_downto
         elif s[0] == 'r' :
            if s[1] == 'e' :
               if s[2] == 'c' :
                  if s[3:6] == "ord" :
                     self.token = self.keyword_record
               elif s[2] == 'p' :
                  if s[3:6] == "eat" :
                     self.token = self.keyword_repeat
         elif s[0] == 's' :
            if s[1:6] == "tring" :
               self.token = self.keyword_string
      elif n == 7 :
         if s[0] == 'f' :
            if s[1:7] == "orward" :
               self.token = self.keyword_forward
         elif s[0] == 'p' :
            if s[1:7] == "rogram" :
               self.token = self.keyword_program
      elif n == 8 :
         if s[0:8] == "function" :
            self.token = self.keyword_function
      elif n == 9 :
         if s[0] == 'i' :
            if s[1:9] == "nterface" :
               self.token = self.keyword_interface
         elif s[0] == 'p' :
            if s[1:9] == "rocedure" :
               self.token = self.keyword_procedure
      elif n == 14 :
         if s[0:14] == "implementation" :
            self.token = self.keyword_implementation

   def processSeparator (self) :
      if self.tokenText == '(' :
         self.token = 52
      if self.tokenText == ')' :
         self.token = 53
      if self.tokenText == '*' :
         self.token = 54
      if self.tokenText == '+' :
         self.token = 55
      if self.tokenText == ',' :
         self.token = 56
      if self.tokenText == '-' :
         self.token = 57
      if self.tokenText == '.' :
         self.token = 58
         if self.ch == '.' :
            self.token = 59
            self.tokenText = self.tokenText + self.ch
            self.nextChar ()
      if self.tokenText == '/' :
         self.token = 60
      if self.tokenText == ':' :
         self.token = 61
         if self.ch == '=' :
            self.token = 62
            self.tokenText = self.tokenText + self.ch
            self.nextChar ()
      if self.tokenText == ';' :
         self.token = 63
      if self.tokenText == '<' :
         self.token = 64
         if self.ch == '=' :
            self.token = 65
            self.tokenText = self.tokenText + self.ch
            self.nextChar ()
         if self.ch == '>' :
            self.token = 66
            self.tokenText = self.tokenText + self.ch
            self.nextChar ()
      if self.tokenText == '=' :
         self.token = 67
      if self.tokenText == '>' :
         self.token = 68
         if self.ch == '=' :
            self.token = 69
            self.tokenText = self.tokenText + self.ch
            self.nextChar ()
      if self.tokenText == '@' :
         self.token = 70
      if self.tokenText == '[' :
         self.token = 71
      if self.tokenText == ']' :
         self.token = 72
      if self.tokenText == '^' :
         self.token = 73
      if self.token == self.separator :
         self.error ("Unknown separator")

   def tokenToString (self, value) :
      if value == 0: return "<end of source text>"
      if value == 1: return "<identifier>"
      if value == 2: return "<number>"
      if value == 3: return "<real_number>"
      if value == 4: return "<character_literal>"
      if value == 5: return "<string_literal>"
      if value == 6: return "<unknown separator>"
      if value == 7: return "<end of line>"
      if value == 8: return "and"
      if value == 9: return "array"
      if value == 10: return "begin"
      if value == 11: return "case"
      if value == 12: return "const"
      if value == 13: return "div"
      if value == 14: return "do"
      if value == 15: return "downto"
      if value == 16: return "else"
      if value == 17: return "end"
      if value == 18: return "file"
      if value == 19: return "for"
      if value == 20: return "forward"
      if value == 21: return "function"
      if value == 22: return "goto"
      if value == 23: return "if"
      if value == 24: return "implementation"
      if value == 25: return "in"
      if value == 26: return "interface"
      if value == 27: return "label"
      if value == 28: return "mod"
      if value == 29: return "nil"
      if value == 30: return "not"
      if value == 31: return "of"
      if value == 32: return "or"
      if value == 33: return "out"
      if value == 34: return "procedure"
      if value == 35: return "program"
      if value == 36: return "record"
      if value == 37: return "repeat"
      if value == 38: return "set"
      if value == 39: return "shl"
      if value == 40: return "shr"
      if value == 41: return "string"
      if value == 42: return "then"
      if value == 43: return "to"
      if value == 44: return "type"
      if value == 45: return "unit"
      if value == 46: return "until"
      if value == 47: return "uses"
      if value == 48: return "var"
      if value == 49: return "while"
      if value == 50: return "with"
      if value == 51: return "xor"
      if value == 52: return "("
      if value == 53: return ")"
      if value == 54: return "*"
      if value == 55: return "+"
      if value == 56: return ","
      if value == 57: return "-"
      if value == 58: return "."
      if value == 59: return ".."
      if value == 60: return "/"
      if value == 61: return ":"
      if value == 62: return ":="
      if value == 63: return ";"
      if value == 64: return "<"
      if value == 65: return "<="
      if value == 66: return "<>"
      if value == 67: return "="
      if value == 68: return ">"
      if value == 69: return ">="
      if value == 70: return "@"
      if value == 71: return "["
      if value == 72: return "]"
      if value == 73: return "^"
      return "<unknown symbol>"

   def storeLocation (self, item) :
      item.fileInx = self.tokenFileInx
      item.line = self.tokenLineNum
      item.column = self.tokenColNum
      item.pos = self.tokenByteOfs
      item.final_pos = self.charByteOfs

   def alloc (self, items) :
      result = [False] * 74
      for item in items :
         result [item] = True
      return result

   def __init__ (self) :
      super (Parser, self).__init__ ()

      # eos = 0
      # identifier = 1
      # number = 2
      # real_number = 3
      # character_literal = 4
      # string_literal = 5
      # separator = 6
      # end_of_line = 7
      self.keyword_and = 8
      self.keyword_array = 9
      self.keyword_begin = 10
      self.keyword_case = 11
      self.keyword_const = 12
      self.keyword_div = 13
      self.keyword_do = 14
      self.keyword_downto = 15
      self.keyword_else = 16
      self.keyword_end = 17
      self.keyword_file = 18
      self.keyword_for = 19
      self.keyword_forward = 20
      self.keyword_function = 21
      self.keyword_goto = 22
      self.keyword_if = 23
      self.keyword_implementation = 24
      self.keyword_in = 25
      self.keyword_interface = 26
      self.keyword_label = 27
      self.keyword_mod = 28
      self.keyword_nil = 29
      self.keyword_not = 30
      self.keyword_of = 31
      self.keyword_or = 32
      self.keyword_out = 33
      self.keyword_procedure = 34
      self.keyword_program = 35
      self.keyword_record = 36
      self.keyword_repeat = 37
      self.keyword_set = 38
      self.keyword_shl = 39
      self.keyword_shr = 40
      self.keyword_string = 41
      self.keyword_then = 42
      self.keyword_to = 43
      self.keyword_type = 44
      self.keyword_unit = 45
      self.keyword_until = 46
      self.keyword_uses = 47
      self.keyword_var = 48
      self.keyword_while = 49
      self.keyword_with = 50
      self.keyword_xor = 51
      # ( 52
      # ) 53
      # * 54
      # + 55
      # , 56
      # - 57
      # . 58
      # .. 59
      # / 60
      # : 61
      # := 62
      # ; 63
      # < 64
      # <= 65
      # <> 66
      # = 67
      # > 68
      # >= 69
      # @ 70
      # [ 71
      # ] 72
      # ^ 73

      self.set_0 = self.alloc ([8, 13, 28, 39, 40, 54, 60]) #  and  div  mod  shl  shr  *  /
      self.set_1 = self.alloc ([32, 51, 55, 57]) #  or  xor  +  -
      self.set_2 = self.alloc ([25, 64, 65, 66, 67, 68, 69]) #  in  <  <=  <>  =  >  >=
      self.set_3 = self.alloc ([1, 2, 3, 5, 29, 30, 41, 52, 55, 57, 70, 71]) #  identifier  number  real_number  string_literal  nil  not  string  (  +  -  @  [
      self.set_4 = self.alloc ([10, 12, 27, 44, 48]) #  begin  const  label  type  var
      self.set_5 = self.alloc ([12, 21, 34, 44, 48]) #  const  function  procedure  type  var
      self.set_6 = self.alloc ([12, 27, 44, 48]) #  const  label  type  var
      self.set_7 = self.alloc ([12, 21, 27, 34, 44, 48]) #  const  function  label  procedure  type  var

   def on_ident_expr (self, item) :
      pass

   def on_field_expr (self, item) :
      pass

   def on_param (self, item) :
      pass

   def add_param (self, item) :
      pass

   def simple_type (self, item) :
      pass

   def on_field (self, item) :
      pass

   def add_field (self, item) :
      pass

   def on_const (self, item) :
      pass

   def add_const (self, item) :
      pass

   def on_type (self, item) :
      pass

   def add_type (self, item) :
      pass

   def on_var (self, item) :
      pass

   def add_var (self, item) :
      pass

   def on_proc (self, item) :
      pass

   def open_proc (self, item) :
      pass

   def on_begin_proc (self, item) :
      pass

   def on_end_proc (self, item) :
      pass

   def close_proc (self, item) :
      pass

   def on_import (self, item) :
      pass

   def open_module (self, item) :
      pass

   def on_module (self, item) :
      pass

   def close_module (self, item) :
      pass

class TLabelIdent (object) :
   _fields_ = ["id"]
   def __init__ (self) :
      self.id = ""

class TType (object) :
   _fields_ = []
   def __init__ (self) :
      pass

class TAliasType (TType) :
   _fields_ = ["name"]
   def __init__ (self) :
      super (TAliasType, self).__init__ ()
      self.name = ""

class TStringType (TType) :
   _fields_ = []
   def __init__ (self) :
      super (TStringType, self).__init__ ()

class TFileType (TType) :
   _fields_ = ["elem"]
   def __init__ (self) :
      super (TFileType, self).__init__ ()
      self.elem = None

class TInxItem (object) :
   _fields_ = ["ref"]
   def __init__ (self) :
      self.ref = None

class TInxList (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TArgItem (object) :
   _fields_ = ["value", "width", "digits"]
   def __init__ (self) :
      self.value = None
      self.width = None
      self.digits = None

class TArgSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TElemSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TElemItem (object) :
   _fields_ = ["low", "high"]
   def __init__ (self) :
      self.low = None
      self.high = None

class TExpr (object) :
   _fields_ = []
   def __init__ (self) :
      pass

class TIdentExpr (TExpr) :
   _fields_ = ["name"]
   def __init__ (self) :
      super (TIdentExpr, self).__init__ ()
      self.name = ""

class TIntValueExpr (TExpr) :
   _fields_ = ["value"]
   def __init__ (self) :
      super (TIntValueExpr, self).__init__ ()
      self.value = ""

class TFltValueExpr (TExpr) :
   _fields_ = ["value"]
   def __init__ (self) :
      super (TFltValueExpr, self).__init__ ()
      self.value = ""

class TStrValueExpr (TExpr) :
   _fields_ = ["value"]
   def __init__ (self) :
      super (TStrValueExpr, self).__init__ ()
      self.value = ""

class TNilExpr (TExpr) :
   _fields_ = []
   def __init__ (self) :
      super (TNilExpr, self).__init__ ()

class TSubExpr (TExpr) :
   _fields_ = ["value"]
   def __init__ (self) :
      super (TSubExpr, self).__init__ ()
      self.value = None

class TNotExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TNotExpr, self).__init__ ()
      self.param = None

class TPlusExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TPlusExpr, self).__init__ ()
      self.param = None

class TMinusExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TMinusExpr, self).__init__ ()
      self.param = None

class TAdrExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TAdrExpr, self).__init__ ()
      self.param = None

class TSetExpr (TExpr) :
   _fields_ = ["list"]
   def __init__ (self) :
      super (TSetExpr, self).__init__ ()
      self.list = None

class TStringExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TStringExpr, self).__init__ ()
      self.param = None

class TIndexExpr (TExpr) :
   _fields_ = ["left", "list"]
   def __init__ (self) :
      super (TIndexExpr, self).__init__ ()
      self.left = None
      self.list = None

class TCallExpr (TExpr) :
   _fields_ = ["func", "list"]
   def __init__ (self) :
      super (TCallExpr, self).__init__ ()
      self.func = None
      self.list = None

class TDerefExpr (TExpr) :
   _fields_ = ["param"]
   def __init__ (self) :
      super (TDerefExpr, self).__init__ ()
      self.param = None

class TFieldExpr (TExpr) :
   _fields_ = ["param", "name"]
   def __init__ (self) :
      super (TFieldExpr, self).__init__ ()
      self.param = None
      self.name = ""

class TTermExpr (TExpr) :
   _fields_ = ["left", "fce", "right"]
   MulExp = 0
   RDivExp = 1
   DivExp = 2
   ModExp = 3
   ShlExp = 4
   ShrExp = 5
   AndExp = 6

   def __init__ (self) :
      super (TTermExpr, self).__init__ ()
      self.left = None
      self.fce = None
      self.right = None

class TSimpleExpr (TExpr) :
   _fields_ = ["left", "fce", "right"]
   AddExp = 0
   SubExp = 1
   OrExp = 2
   XorExp = 3

   def __init__ (self) :
      super (TSimpleExpr, self).__init__ ()
      self.left = None
      self.fce = None
      self.right = None

class TCompleteExpr (TExpr) :
   _fields_ = ["left", "fce", "right"]
   EqExp = 0
   NeExp = 1
   LtExp = 2
   GtExp = 3
   LeExp = 4
   GeExp = 5
   InExp = 6

   def __init__ (self) :
      super (TCompleteExpr, self).__init__ ()
      self.left = None
      self.fce = None
      self.right = None

class TStat (object) :
   _fields_ = []
   def __init__ (self) :
      pass

class TGotoStat (TStat) :
   _fields_ = ["goto_lab"]
   def __init__ (self) :
      super (TGotoStat, self).__init__ ()
      self.goto_lab = None

class TBlockStat (TStat) :
   _fields_ = ["body_seq"]
   def __init__ (self) :
      super (TBlockStat, self).__init__ ()
      self.body_seq = None

class TStatSeq (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TIfStat (TStat) :
   _fields_ = ["cond_expr", "then_stat", "else_stat"]
   def __init__ (self) :
      super (TIfStat, self).__init__ ()
      self.cond_expr = None
      self.then_stat = None
      self.else_stat = None

class TCaseStat (TStat) :
   _fields_ = ["case_expr", "case_list", "else_seq"]
   def __init__ (self) :
      super (TCaseStat, self).__init__ ()
      self.case_expr = None
      self.case_list = None
      self.else_seq = None

class TCaseSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TCaseItem (object) :
   _fields_ = ["sel_list", "sel_stat"]
   def __init__ (self) :
      self.sel_list = None
      self.sel_stat = None

class TWhileStat (TStat) :
   _fields_ = ["cond_expr", "body_stat"]
   def __init__ (self) :
      super (TWhileStat, self).__init__ ()
      self.cond_expr = None
      self.body_stat = None

class TRepeatStat (TStat) :
   _fields_ = ["body_seq", "until_expr"]
   def __init__ (self) :
      super (TRepeatStat, self).__init__ ()
      self.body_seq = None
      self.until_expr = None

class TForStat (TStat) :
   _fields_ = ["var_expr", "from_expr", "incr", "to_expr", "body_stat"]
   def __init__ (self) :
      super (TForStat, self).__init__ ()
      self.var_expr = None
      self.from_expr = None
      self.incr = False
      self.to_expr = None
      self.body_stat = None

class TWithStat (TStat) :
   _fields_ = ["with_list", "body_stat"]
   def __init__ (self) :
      super (TWithStat, self).__init__ ()
      self.with_list = None
      self.body_stat = None

class TWithSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TWithItem (object) :
   _fields_ = ["expr"]
   def __init__ (self) :
      self.expr = None

class TEmptyStat (TStat) :
   _fields_ = []
   def __init__ (self) :
      super (TEmptyStat, self).__init__ ()

class TAssignStat (TStat) :
   _fields_ = ["left_expr", "right_expr"]
   def __init__ (self) :
      super (TAssignStat, self).__init__ ()
      self.left_expr = None
      self.right_expr = None

class TCallStat (TStat) :
   _fields_ = ["call_expr"]
   def __init__ (self) :
      super (TCallStat, self).__init__ ()
      self.call_expr = None

class TLabeledTStat (TStat) :
   _fields_ = ["lab", "body"]
   def __init__ (self) :
      super (TLabeledTStat, self).__init__ ()
      self.lab = None
      self.body = None

class TParamIdent (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = ""

class TParamItem (object) :
   _fields_ = ["mode", "items", "typ", "ini"]
   VarParam = 0
   ConstParam = 1
   OutParam = 2
   ValueParam = 3

   def __init__ (self) :
      self.mode = None
      self.items = [ ]
      self.typ = None
      self.ini = None

class TParamSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TEnumType (TType) :
   _fields_ = ["elements"]
   def __init__ (self) :
      super (TEnumType, self).__init__ ()
      self.elements = None

class TEnumSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TEnumItem (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = ""

class TArrayType (TType) :
   _fields_ = ["index_list", "elem"]
   def __init__ (self) :
      super (TArrayType, self).__init__ ()
      self.index_list = None
      self.elem = None

class TIndexTypeSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TIndexTypeItem (object) :
   _fields_ = ["index"]
   def __init__ (self) :
      self.index = None

class TRecordType (TType) :
   _fields_ = ["items"]
   def __init__ (self) :
      super (TRecordType, self).__init__ ()
      self.items = [ ]

class TFieldItem (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = ""

class TDeclSect (object) :
   _fields_ = []
   def __init__ (self) :
      pass

class TFieldDecl (TDeclSect) :
   _fields_ = ["items", "typ"]
   def __init__ (self) :
      super (TFieldDecl, self).__init__ ()
      self.items = [ ]
      self.typ = None

class TPointerType (TType) :
   _fields_ = ["elem"]
   def __init__ (self) :
      super (TPointerType, self).__init__ ()
      self.elem = None

class TSetType (TType) :
   _fields_ = ["elem"]
   def __init__ (self) :
      super (TSetType, self).__init__ ()
      self.elem = None

class TSubrangeType (TType) :
   _fields_ = ["low", "high"]
   def __init__ (self) :
      super (TSubrangeType, self).__init__ ()
      self.low = None
      self.high = None

class TLabelDecl (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = None

class TLabelSect (TDeclSect) :
   _fields_ = ["items"]
   def __init__ (self) :
      super (TLabelSect, self).__init__ ()
      self.items = [ ]

class TConstDecl (object) :
   _fields_ = ["name", "typ", "val"]
   def __init__ (self) :
      self.name = ""
      self.typ = None
      self.val = None

class TConstSect (TDeclSect) :
   _fields_ = ["items"]
   def __init__ (self) :
      super (TConstSect, self).__init__ ()
      self.items = [ ]

class TTypeDecl (object) :
   _fields_ = ["name", "typ"]
   def __init__ (self) :
      self.name = ""
      self.typ = None

class TTypeSect (TDeclSect) :
   _fields_ = ["items"]
   def __init__ (self) :
      super (TTypeSect, self).__init__ ()
      self.items = [ ]

class TVarItem (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = ""

class TVarDecl (object) :
   _fields_ = ["items", "typ", "ini"]
   def __init__ (self) :
      self.items = [ ]
      self.typ = None
      self.ini = None

class TVarSect (TDeclSect) :
   _fields_ = ["items"]
   def __init__ (self) :
      super (TVarSect, self).__init__ ()
      self.items = [ ]

class TProcDecl (TDeclSect) :
   _fields_ = ["style", "name", "param_list", "answer", "a_forward", "local", "body"]
   ProcedureStyle = 0
   FunctionStyle = 1

   def __init__ (self) :
      super (TProcDecl, self).__init__ ()
      self.style = None
      self.name = ""
      self.param_list = None
      self.answer = None
      self.a_forward = False
      self.local = None
      self.body = None

class TDeclGroup (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TImportItem (object) :
   _fields_ = ["name"]
   def __init__ (self) :
      self.name = ""

class TImportSect (object) :
   _fields_ = ["items"]
   def __init__ (self) :
      self.items = [ ]

class TModule (object) :
   _fields_ = []
   def __init__ (self) :
      pass

class TUnitModule (TModule) :
   _fields_ = ["name", "intf_imports", "intf_decl", "impl_imports", "impl_decl", "init"]
   def __init__ (self) :
      super (TUnitModule, self).__init__ ()
      self.name = ""
      self.intf_imports = None
      self.intf_decl = None
      self.impl_imports = None
      self.impl_decl = None
      self.init = None

class TProgramModule (TModule) :
   _fields_ = ["name", "impl_imports", "impl_decl", "init"]
   def __init__ (self) :
      super (TProgramModule, self).__init__ ()
      self.name = ""
      self.impl_imports = None
      self.impl_decl = None
      self.init = None

