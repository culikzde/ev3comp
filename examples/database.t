Table colors
{
    Column { name = "name"; type = "varchar (80)"; primary_key = true; };
    Column b { name = "red"; type = "int"; };
    Column c () { name = "green"; type = "int"; };
    Column { name = "blue"; type = "int"; };
};

Table messages
{
    string pc;
    date   date;
    string message;
};