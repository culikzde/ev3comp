
class Block
{
   public:
      int x, y;
      int width, height;
      string text;
      string ink;
      strink paper;
      list < Block * > items;
};

typedef list < Block * > BlockList;

tree_model BlockList;
property_table Block;
sql_table BlockList;
xml_file BlockList;
data_file BlockList;
clipboard Block;
drag_and_drop Block;
dbus Block;
html_page BlockList;
dialog "dialog.ui";

QMenuBar mainMenu
{
    QMenu fileMenu
    {
        title = "&File";
        QAction quitMenu
        {
            title = "Quit;
            shortcut = "Ctrl+Q";
        }
    }
}

QTabWidget palette
{
   QToolBar palette_page
   {
       QToolButton palette_button
       {
           title = "Block";
       }
   }
}

class MainWindow : public QMainWindow
{
    mainMenu;

    QSplitter hsplitter
    {
        QTreeWidget tree
        {
        }
        QVBoxLayout
        {
           palette;
           QGraphicsView design
           {
           }
        }
        QTableWidget prop
        {
        }
    }

    setCentralSplitter (hsplitter);
};

int main () /* (int argc, char * * argv) */
{
     QApplication * appl = new QApplication (sys.argv); /* (argc, argc) */
     MainWindow * window = new MainWindow ();
     window->show ();
     appl->exec_ (); /* appl->exec (); */
}

/* ---------------------------------------------------------------------- */

include ("<QXmlStreamReader>");
include ("<QXmlStreamWriter>");

include ("<QFileDialog>");
include ("<QFile>");

class XmlIO
{
public:
    class_type Record;
    string open_dialog_text = "Open XML file";
    string save_dialog_text = "Save XML file";
};

void openXmlFile ()
{
   QString s = QFileDialog::getOpenFileName (this, open_dialog_text); // <- this
   if (s != "")
      readXmlFile (s);
}

void saveXmlFile ()
{
    QString s = QFileDialog::getSaveFileName (this, save_dialog_text);
    if (s != "")
       writeXmlFile (s);
}

void readXmlFile (QString fileName)
{
    QFile file (fileName);
    if (file.open (QFile::ReadOnly | QFile::Text ))
    {
        QXmlStreamReader reader (& file);
        readXml (reader, top_element); // <-- top_element
    }
}

void writeXmlFile (QString fileName)
{
    QFile file (fileName);
    if (file.open (QFile::WriteOnly | QFile::Text))
    {
        QXmlStreamWriter writer (&file);
        writer.setAutoFormatting (true);

        writer.writeStartDocument ();
        writer.writeStartElement ("data"); // <-- top level element

        QList <QGraphicsItem *> items = top_element->childItems ();
        for (QGraphicsItem * t : items)
        {
             writeXml (writer, t);
        }

        writer.writeEndElement ();
        writer.writeEndDocument ();
    }
}

Record * readXml (QXmlStreamReader & reader) (class_type Record)
{
    Record * item = new Record;
    for (QXmlStreamAttribute a : reader.attributes())
    {
       QString attr = a.name().toString();
       QString value = a.value().toString();

       for (field : Record.items)
       {
           if (field.is_scalar)
           {
              // cascade
              if (attr == field.name)
                 field = string_to_field (value)
           }
           // otherwise error ("unknown attribute name");
    }

    reader.readNext();

    while (! reader.isEndElement ())
    {
       for (field : Record.items)
       {
           if (field.is_object)
           {
              // cascade
              if (attr == field.name)
           }
           // otherwise error ("unknown element name");
       }
    }

    reader.readNext();
}


void writeXml (QXmlStreamWriter & writer, Record * item) (class_type Record, string element_name)
{
   writer.writeStartElement (element_name);

   for (field : Record.items)
   {
       if (field.is_scalar)
       {
          QString text = field_to_text (item.field);
          writer.writeAttribute (field.name, text);
       }
       else if (field.is_object)
       {
           writeXml (writer, item.field) (field.type, field.type.name);
       }
   }

   writer.writeEndElement ();
}

void field_to_string ();
void string_to_field ();

/* ---------------------------------------------------------------------- */
