class string { };

#if 1
int a;
int * b, c, d [10];
string f () { int z; }
void g (int x, string y, ...);
void h (int x = 1, something y = "abc");
int * & c (bool x = 1 + 2, string * y = "abc" + b);
typedef float * ( * type [10] ) [20]  (int x, double y) ;
string t;
u ();
v (int i, int j);
#endif

attribute void ink (string);
attribute void paper (string);
attribute void icon (string);
attribute void tooltip (string);
attribute void warning (string);
attribute void error (string);

namespace Example
{
    // paper ("lime");

    enum Color { red, blue, green, yellow, orange, cornflowerblue };

    class Entry
    {
        // paper ("yellow");
        bool boolean;
        int number;
        Color color;
        string text;

        Entry ();
        Entry (int i, int j = 2);
        Entry f ();
        Entry * g ();

        void show (string txt, int size)
        {
           // string t = "";
           int n = 1;
           {
              int i;
              for (i = 1; i <= 10; i++)
                  n = n + i;
              for (i : abc)
                  n = n + i;
           }
           warning ("Show function warning");
           // error ("Show function error");
           tooltip ("Show function");
           add (1, 2);
           ink ("red");
           paper ("cornflowerblue");
           icon ("folder");
           tooltip ( 1 + 1.1 );
           tooltip ( Entry.fields );
           {
              string s;
              for ( s : Entry.fields ) text = s;
              int n;
           }
        }

        void hide () { text = "hide"; show (); }

        QPushButton button
        {
           string text = "Abc";
           int n = 1;
        }

        QLineEdit edit
        {
           text = "Some text";
        }
    };

    string field_to_text (int n) { }
    string field_to_text (Entry e) { }
    string field_to_text (Color c) { }

    void test ()
    {
        Entry v;
        field_to_text (v);
    }

    string * text_ptr;
    // string array [10];
    // string (*ptr_to_array) [10];
    // string * array_of_ptr [10];
    int * number_ptr;


    template < class T >
       class List
       {
          T * first;
          T * last;
       };

    List <int> integer_list;
    List <string> string_list;

    void f ()
    {
       int n;
       int i;
       n = 0;
       for (i = 1; i <= 10; i++)
           n = n + i;
       Entry e;
       Entry.show ();
       e.hide ();
    }

/* ---------------------------------------------------------------------- */

template < class T >
   class Conv
   {
       string field_to_text (T n)
       {
           return str (n);
       }
   };

Conv< int> cnv;

void main ()
{
    cnv.field_to_text (1);
}

/* ---------------------------------------------------------------------- */

template < class Record, string element_name >
void writeXml (QXmlStreamWriter & writer, Record * item)
{
   writer.writeStartElement (element_name);

   for (field : Record.items)
   {
       if (field.is_scalar)
       {
          QString text = field_to_text (item.field);
          writer.writeAttribute (field.name, text);
       }
       else if (field.is_object)
       {
           writeXml < field.type, field.type.name > (writer, item.field);
       }
   }

   writer.writeEndElement ();
}

void writeXmlFile (QString fileName, Entry * data)
{
    QFile file (fileName);
    if (file.open (QFile::WriteOnly | QFile::Text))
    {
        QXmlStreamWriter writer (&file);
        writer.setAutoFormatting (true);

        writer.writeStartDocument ();
        writeXml < Entry.name, "EntryBlock" > (writer, data);
        writer.writeEndDocument ();
    }
}

/* ---------------------------------------------------------------------- */

} // end of namespace
