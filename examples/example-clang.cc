
#if 1
   "examples/simple.cc"
#endif

/* ---------------------------------------------------------------------- */

#if 0
   #pragma gcc_options
   "ckit/examples/example1.cc"
#endif

/* ---------------------------------------------------------------------- */

#if 0
   #pragma gcc_options
   #pragma pkg "gtk+-2.0"
   "ckit/examples/example2.cc"
#endif

/* ---------------------------------------------------------------------- */

#if 0
   #pragma gcc_options
   #pragma pkg "qt-mt"
   "ckit/examples/example3.cc"
#endif

/* ---------------------------------------------------------------------- */

#if 0
   #pragma gcc_options
   #pragma pkg "QtGui"
   "ckit/examples/example4.cc"
#endif

/* ---------------------------------------------------------------------- */

#if 0
   #pragma gcc_options
   #pragma pkg "Qt5Widgets"
   "ckit/examples/example5.cc"
#endif

/* ---------------------------------------------------------------------- */

