
namespace Example
{
    // paper ("lime");

    enum Color { red, blue, green, yellow, orange, cornflowerblue };

    class Entry
    {
        // paper ("yellow");
        bool boolean;
        int number;
        Color color;
        string text;

        void show (string txt, int size)
        {
           // string t = "";
           int n = 1;
           int i;
           for (i = 1; i <= 10; i++)
               n = n + i;
           warning ("Show function warning");
           error ("Show function error");
           tooltip ("Show function");
           add (1, 2);
           ink ("red");
           paper ("cornflowerblue");
           text = "show";
        }

        void hide () { text = "hide"; show (); }

        QPushButton button
        {
           string text = "Abc";
           int n = 1;
        }

        QLineEdit edit
        {
           text = "Some text";
        }
    };

    string * text_ptr;
    string array [10];
    // string (*ptr_to_array) [10];
    string * array_of_ptr [10];
    int * number_ptr;


    template < class T >
       class List
       {
          T * first;
          T * last;
       };

    List <int> integer_list;
    List <string> string_list;

    void f ()
    {
       int n;
       int i;
       n = 0;
       for (i = 1; i <= 10; i++)
           n = n + i;
       Entry e;
       Entry.show ();
       e.hide ();
    }
}
