
#define QT_BEGIN_HEADER
#define QT_END_HEADER
#define QT_BEGIN_NAMESPACE
#define QT_END_NAMESPACE
#define QT_BEGIN_INCLUDE_NAMESPACE
#define QT_END_INCLUDE_NAMESPACE
#define QT_MODULE(M)
#define QT_MOC_COMPAT

#define QDOC_PROPERTY(P)

#define Q_CORE_EXPORT
#define Q_GUI_EXPORT
#define Q_GUI_EXPORT_INLINE

#define Q_OBJECT
#define Q_PROPERTY(P)
#define Q_OVERRIDE(P)
#define Q_SIGNALS public
#define Q_SLOTS
#define Q_ENUMS(A)
#define Q_FLAGS(A)
#define Q_INVOKABLE
#define Q_INTERFACES(A)
#define Q_CLASSINFO(A,B)
#define Q_GADGET

#define Q_PRIVATE_SLOT(A,B)
#define Q_PRIVATE_PROPERTY(A,B)

#define Q_DECLARE_FLAGS(A,B)
#define Q_DECLARE_FLAGS(A,B)
#define Q_DECLARE_OPERATORS_FOR_FLAGS(T)
#define Q_DECLARE_PRIVATE(T)
#define Q_DECLARE_INTERFACE(A,B)
#define Q_DECLARE_PRIVATE_D(A,B)
#define Q_DECLARE_SHARED(A)
#define Q_DECLARE_METATYPE(T)
#define Q_DECLARE_OPERATORS_FOR_FLAGS(T)

#define Q_DISABLE_COPY(T)
#define Q_DUMMY_COMPARISON_OPERATOR(A)
#define Q_INLINE_TEMPLATE
#define Q_REQUIRED_RESULT

#define __attribute__(x)
#define __attribute_pure__
#define __typeof__(t) type_from_typeof
#define __typeof(t) type_from_typeof
#define __asm(x)
#define __extension__
#define __inline inline
#define __restrict

#pragma tolerate_missing_include
#pragma include_dir "/usr/include/QtGui"

#pragma ignore_include "qwidget.h"
// #pragma ignore_include "qtreewidget.h"

// #include "QtGui"

// #include "QApplication"

#include "QObject"
#include "QWidget"
#include "QAbstractButton"
#include "QPushButton"
#include "QTreeWidget"

int main (int argc, char * * argv)
{
     QApplication appl (argc, argv);

     QWidget * window = new QWidget ();
     window->resize (320, 240);
     window->show ();

     QPushButton * button = new QPushButton ("Press me", window);
     button->move (100, 100);
     button->show ();

     appl.exec ();
}
