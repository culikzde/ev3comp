/*
QPushButton button
{
   text = "Abc";
}
*/

class QMainWindow;
class QMenuBar;
class QMenu;
class QAction;
class QToolBar;
class QToolButton;

QMainWindow window
{
   windowTitle = "Example";
   QMenuBar mainMenu
   {
       QMenu file
       {
           title = "&File";
           QAction quit
           {
               text = "&Quit";
               shortcut = "Ctrl+Q";;
           };
       };
   };

   QToolBar toolbar
   {
       QToolButton tool_button
       {
           text = "first";
           icon = "weather-clear";
       };
       QToolButton tool_button_2
       {
           text = "second";
           icon = "applications-toys";
       };
   };

   // QWidget w
   // {
   QVBoxLayout vlayout
   {
      QHBoxLayout hlayout
      {
         QPushButton button
         {
            text = "Abc";
         };
         QCheckBox check_box
         {
            text = "Check";
         };
         QLineEdit edit
         {
            text = "Edit";
         };
      };
      QTreeWidget tree
      {
         QTreeWidgetItem branch
         {
            text = "tree branch";
            foreground = "brown";
            background = "orange";
            icon = "folder";
            expanded = true;

            QTreeWidgetItem node1  { text = "red"; foreground = "red"; };
            QTreeWidgetItem node2  { text = "green"; foreground = "green"; };
            QTreeWidgetItem node3  { text = "blue"; foreground = "blue"; };
         };
      };
   };
   // }

   QStatusBar status
   {
       QLabel lab
       {
          text = "status bar";
       };
   };
}
