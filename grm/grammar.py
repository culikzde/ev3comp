
# grammar.py

from __future__ import print_function

from lexer import Lexer

# --------------------------------------------------------------------------

class Rule (object) :
   def __init__ (self) :
       self.name = ""
       self.expr = None
       self.rule_mode = ""
       self.rule_type = ""
       self.super_type = ""
       self.tag_name = ""
       self.tag_value = ""
       self.store_name = ""
       self.store_type = ""
       self.add_used = False
       self.choose_item = None
       self.continue_ebnf = None
       self.new_directive = None # local
       self.actual_type = "" # local
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

class Expression (object) :
   def __init__ (self) :
       self.alternatives = [ ]
       self.continue_expr = False
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

class Alternative (object) :
   def __init__ (self) :
       self.items = [ ]
       self.silent = False
       self.select_alt = False
       self.choose_alt = False
       self.continue_alt = False
       self.leading_required = False # local
       self.predicate = None
       self.semantic_predicate = None
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

class Ebnf (object) :
   def __init__ (self) :
       self.mark = ""
       self.expr = None
       self.predicate_inx = 0
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

class Nonterminal (object) :
   def __init__ (self) :
       self.variable = ""
       self.rule_name = ""
       self.add = False
       self.modify = False
       self.select_item = False
       self.choose_item = False
       self.continue_item = False
       # self.rule_ref = None
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

class Terminal (object) :
   def __init__ (self) :
       self.text = ""

       self.variable = ""
       self.multiterminal_name = ""

       # self.symbol_ref = None
       # self.nullable = False
       # self.first = [ ]
       # self.line = 1

# --------------------------------------------------------------------------

class Directive (object) :
   def __init__ (self) :
       pass

class Assign (Directive) :
   def __init__ (self) :
       self.variable = ""
       self.value = ""

class Execute (Directive) :
   def __init__ (self) :
       self.name = ""

class New (Directive) :
   def __init__ (self) :
       self.new_type = ""
       self.new_super_type = ""
       self.store_name = ""
       self.store_type = ""

class Style (Directive) :
   def __init__ (self) :
       self.name = ""

# --------------------------------------------------------------------------

class Options (object) :
   def __init__ (self) :
       self.add = False
       self.modify = False
       self.need_nonterminal = False

# --------------------------------------------------------------------------

class StructDecl (object) :
   def __init__ (self) :
       self.struct_name = ""
       self.super_type = ""
       self.fields = [ ]

class FieldDecl (object) :
   def __init__ (self) :
       self.field_type = ""
       self.field_name = ""

# --------------------------------------------------------------------------

class Grammar (Lexer) :

   def __init__ (self) :
       super (Grammar, self).__init__ ()
       self.rules = [ ]
       self.struct_decl = [ ]
       self.multiterminals = [ "identifier",
                               "number",
                               "real_number",
                               "character_literal",
                               "string_literal" ]

       self.type_list = [ ]
       self.type_dict = { }
       self.methods = [ ]

       self.predicates = [ ]
       self.semantic_predicates = [ ]
       self.test_list = [ ]
       self.test_dict = { }

       self.expression_type_name = ""

       self.execute_on_begin = { }
       self.execute_on_end = { }
       self.execute_on_choose = { }

       self.show_tree = False

       # self.multiterminal_dict = { }
       # self.keyword_dict = { }
       # self.separator_dict = { }
       #
       # self.symbol_cnt = 0
       # self.symbols = [ ]
       # self.symbol_dict = { }
       #
       # self.rule_dict = { }
       # self.nullableChanged = True
       # self.firstChanged = True
       #
       # self.collections = { }

   def skipDirective (self) :
       while self.isSeparator ('<') :
          self.nextToken ()
          while not self.isEndOfSource () and not self.isSeparator ('>') :
             self.nextToken ()
          if self.isEndOfSource () :
             self.error ("Unfinished directive")
          self.nextToken () # skip '>'

   def ruleDirective (self, rule) :
       if self.isSeparator ('<') :
          self.nextToken ()
          if self.isKeyword ("new") :
             rule.rule_mode = "new"
             self.nextToken ()
          elif self.isKeyword ("modify") :
             rule.rule_mode = "modify"
             self.nextToken ()
          elif self.isKeyword ("select") or self.isKeyword ("return"):
             rule.rule_mode = "select"
             self.nextToken ()
          elif self.isKeyword ("choose") :
             rule.rule_mode = "choose"
             self.nextToken ()
          else :
             rule.rule_mode = "new"

          rule.rule_type = self.readIdentifier ("Type name expected")

          if self.isSeparator (':') :
             self.nextToken ()
             rule.super_type = self.readIdentifier ("Super-type expected")

          if self.isSeparator (',') :
             self.nextToken ()
             rule.tag_name = self.readIdentifier ("Tag identifier expected")
             self.checkSeparator ('=')
             rule.tag_value = self.readIdentifier ("Tag value expected")
             if rule.rule_mode != "new" :
                self.error ("Tag only allowed for \"new\" mode")

          self.checkSeparator ('>')

       # if self.isSeparator ('<') :
          # self.nextToken ()
          # if self.isKeyword ("custom") :
             # self.nextToken ()
             # self.readIdentifier ()
             # while self.isSeparator (',') :
                # self.nextToken ()
                # self.readIdentifier ()
          # self.checkSeparator ('>')

   def itemDirective (self, rule, alt, opt) :
       while self.isSeparator ('<') :
          self.nextToken ()

          if self.isKeyword ("new") :
             self.nextToken ()
             # add New to this alternative
             item = New ()
             item.new_type = self.readIdentifier ("Type identifier expected")
             self.checkSeparator (':')
             item.new_super_type = self.readIdentifier ("Type identifier expected")
             alt.items.append (item)
             rule.new_directive = item
             alt.leading_required = False # !?

          elif self.isKeyword ("store") :
             self.nextToken ()
             name = self.readIdentifier ("Field identifier expected")
             self.checkSeparator (':')
             type = self.readIdentifier ("Type identifier expected")
             if rule.new_directive != None :
                rule.new_directive.store_name = name
                rule.new_directive.store_type = type
             else :
                rule.store_name = name
                rule.store_type = type

          elif self.isKeyword ("add") :
             self.nextToken ()
             opt.add = True
             opt.need_nonterminal = True

          elif self.isKeyword ("modify") :
             self.nextToken ()
             opt.modify = True
             opt.need_nonterminal = True

          elif self.isKeyword ("set") :
             self.nextToken ()
             variable = self.readIdentifier ("Variable identifier expected")
             self.checkSeparator ('=')
             value = self.readIdentifier ("Value expected")
             if value == "true" :
                value = "True"
             if value == "false" :
                value = "False"
             # add Assign to this alternative
             item = Assign ()
             item.variable = variable
             item.value = value
             alt.items.append (item)

          elif self.isKeyword ("execute") :
             self.nextToken ()
             name = self.readIdentifier ("Method identifier expected")
             # add Execute to this alternative
             item = Execute ()
             item.name = name
             alt.items.append (item)

          elif self.isKeyword ("silent") :
             self.nextToken ()
             # set silent field in this alternative
             alt.silent = True

          elif ( self.isKeyword ("indent") or
                 self.isKeyword ("unindent") or
                 self.isKeyword ("no_space") or
                 self.isKeyword ("new_line") or
                 self.isKeyword ("empty_line") ) :
             name = self.tokenText
             self.nextToken ()
             # add Style to this alternative
             item = Style ()
             item.name = name
             alt.items.append (item)
          else :
             self.error ("Unknown directive " + self.tokenText)

          self.checkSeparator ('>')

   def setPosition (self, item) :
       item.fileInx = self.tokenFileInx
       item.line = self.tokenLineNum
       item.column = self.tokenColNum
       item.pos = self.tokenByteOfs

   def rememberPosition (self) :
       return self.tokenLineNum

   def storePosition (self, item, pos) :
       item.line = pos

   def updatePosition (self, item) :
       "set position for error reporting"
       inp = self.file_input
       if inp != None :
          # inp.fileInx = item.fileInx
          inp.lineNum = item.line
          # inp.colNum = item.column
          # inp.byteOfs = item.pos

   # -- directives --

   def structDirective (self) :
       struct = StructDecl ()
       struct.struct_name = self.readIdentifier ("Type identifier expected")
       if self.isSeparator (':') :
          self.nextToken ()
          struct.super_type = self.readIdentifier ("Super-type identifier expected")
       self.struct_decl.append (struct)
       self.checkSeparator ('{')
       while not self.isSeparator ('}') :
          field = FieldDecl ()
          field.field_type = self.readIdentifier ("Type identifier expected")
          field.field_name = self.readIdentifier ("Field identifier expected")
          self.checkSeparator (';')
          struct.fields.append (field)
       self.checkSeparator ('}')

   def addMethod (self, func_dict, rule_name, method_name) :
       if rule_name in func_dict :
          self.error ("Cannot register " + method_name, ", rule " + rule_name + "has already registered method " + func_dict [rule_name])
       func_dict [rule_name] = method_name

   def rememberMethods (self, begin_func, end_func) :
       cont = True
       while cont :
          rule_name = self.readIdentifier ("Rule identifier expected")
          if begin_func != "" :
             self.addMethod (self.execute_on_begin, rule_name, begin_func)
          if end_func != "" :
             self.addMethod (self.execute_on_end, rule_name, end_func)
          cont = False
          if self.isSeparator (',') :
             self.nextToken ()
             cont = True

   def executeOnBeginEnd (self) :
       begin_func = ""
       if not self.isSeparator (',') :
          begin_func = self.readIdentifier ("Method identifier expected")
       self.checkSeparator (',')

       end_func = ""
       if not self.isSeparator (':') :
          end_func = self.readIdentifier ("Method identifier expected")
       self.checkSeparator (':')

       self.rememberMethods (begin_func, end_func)

   def executeOnBegin (self) :
       begin_func = self.readIdentifier ("Method identifier expected")
       self.checkSeparator (':')
       self.rememberMethods (begin_func, "")

   def executeOnEnd (self) :
       end_func = self.readIdentifier ("Method identifier expected")
       self.checkSeparator (':')
       self.rememberMethods ("", end_func)

   def executeOnChoose (self) :
       func_name = self.readIdentifier ("Method identifier expected")
       self.checkSeparator (':')

       cont = True
       while cont :
          rule_name = self.readIdentifier ("Rule identifier expected")
          self.addMethod (self.execute_on_choose, rule_name, func_name)
          cont = False
          if self.isSeparator (',') :
             self.nextToken ()
             cont = True

   def globalDirective (self) :
       while self.isSeparator ('<') :
          self.nextToken ()
          if self.isKeyword ("struct") :
             self.nextToken ()
             self.structDirective ()
          elif self.isKeyword ("expression") :
             self.nextToken ()
             self.expression_type_name = self.readIdentifier ("Type identifier expected")
          elif self.isKeyword ("execute_on_begin_end") :
             self.nextToken ()
             self.executeOnBeginEnd ()
          elif self.isKeyword ("execute_on_begin") :
             self.nextToken ()
             self.executeOnBegin ()
          elif self.isKeyword ("execute_on_end") :
             self.nextToken ()
             self.executeOnEnd ()
          elif self.isKeyword ("execute_on_choose") :
             self.nextToken ()
             self.executeOnChoose ()
          else :
             self.error ("Unknown global directive")
          self.checkSeparator ('>')

   # -- rules --

   def parseRules (self) :
       while not self.isEndOfSource () :
          self.globalDirective ()
          if not self.isEndOfSource () :
             self.parseRule ()

   def parseRule (self) :
       rule = Rule ()

       rule.name = self.readIdentifier ("Rule identifier expected")
       self.setPosition (rule)

       self.ruleDirective (rule)
       self.checkSeparator (':')

       rule.expr = self.parseExpression (rule, 1)

       self.checkSeparator (';')
       self.rules.append (rule)

   # -- expression --

   def parseExpression (self, rule, level) :
       expr = Expression ()
       self.setPosition (expr)

       if level == 2 :
          if rule.rule_mode == "choose" :
             expr.continue_expr = True

       alt = self.parseAlternative (rule, level)
       expr.alternatives.append (alt)

       while self.isSeparator ('|') :
          if level == 1 and rule.rule_mode == "choose"  :
             self.error ("Only one alternative allowed (for rules with choose attribute)")
          self.nextToken ()

          alt = self.parseAlternative (rule, level)
          expr.alternatives.append (alt)

       return expr

   def parseAlternative (self, rule, level) :
       alt = Alternative ()
       self.setPosition (alt)

       if level == 1 :
          if rule.rule_mode == "select" :
             alt.select_alt = True
             alt.leading_required = True
          if rule.rule_mode == "choose" :
             alt.choose_alt = True
             alt.leading_required = True

       if level == 2 :
          if rule.rule_mode == "choose" :
             alt.continue_alt = True
             alt.leading_required = True

       choose_inx = 0
       if alt.choose_alt :
          choose_inx = 1

       can_continue = True

       opt = Options ()
       if choose_inx == 0 :
          self.itemDirective (rule, alt, opt)

       while not self.isSeparator ('|') and not self.isSeparator (')') and not self.isSeparator (';') and can_continue:

          if self.token == self.identifier :
             if choose_inx == 2 :
                self.error ("Sub-expression expected (for rule with choose attribute)")
             item = self.parseNonterminal (rule, alt, opt)
             alt.items.append (item)
             if choose_inx == 1 :
                choose_inx = 2

          elif self.token == self.character_literal or self.token == self.string_literal :
             if choose_inx == 1 :
                self.error ("Nonterminal expected (for rule with choose attribute)")
             if choose_inx == 2 :
                self.error ("Sub-expression expected (for rule with choose attribute)")
             if opt.need_nonterminal :
                self.error ("Nonterminal expected")
             item = self.parseTerminal ()
             alt.items.append (item)

          elif self.isSeparator ('(') :
             if choose_inx == 1 :
                self.error ("Nonterminal expected (for rule with choose attribute)")
             if opt.need_nonterminal :
                self.error ("Nonterminal expected")
             ebnf = self.parseEbnf (rule, level)
             if ebnf.mark == '>' :
                alt.predicate = ebnf.expr
                # and discard ebnf class
             else :
                if alt.choose_alt :
                   rule.continue_ebnf = ebnf
                if alt.leading_required :
                   self.error ("Missing nonterminal (for rule with select or choose attribute)")
                alt.items.append (ebnf)
                if choose_inx == 2 :
                   can_continue = False

          elif self.isSeparator ('{') : # !?
             self.nextToken ()
             alt.semantic_predicate = self.readIdentifier ("Function identifier expected")
             if self.isSeparator ('(') :
                self.nextToken ()
                self.checkSeparator (')')
             self.checkSeparator ('}')
             self.checkSeparator ('?')

          else:
             self.error ("Unknown grammar item")

          if can_continue : # inside while loop
             opt = Options ()
             if choose_inx == 0 :
                self.itemDirective (rule, alt, opt)

       if opt.need_nonterminal : # after while lopp
          self.error ("Nonterminal expected")

       if choose_inx == 1 :
          self.error ("Sub-expression expected (for rule with choose attribute)")

       rule.new_directive = None
       return alt

   def parseEbnf (self, rule, level) :
       item = Ebnf ()

       self.setPosition (item)
       self.checkSeparator ('(')
       item.expr = self.parseExpression (rule, level+1)
       self.checkSeparator (')')

       if self.isSeparator ('?') :
          item.mark = '?'
          self.nextToken ()
       elif self.isSeparator ('+') or self.isSeparator ('@') :
          item.mark = '+'
          self.nextToken ()
       elif self.isSeparator ('*') :
          item.mark = '*'
          self.nextToken ()
       elif self.isSeparator ('=') :
          self.nextToken ();
          self.checkSeparator ('>')
          item.mark = '>'

       return item

   def parseNonterminal (self, rule, alt, opt) :

       pos = self.rememberPosition ()
       variable = ""
       rule_name = self.readIdentifier ()

       if self.isSeparator (':') :
          self.nextToken ()
          variable = rule_name
          rule_name = self.readIdentifier ("Rule identifier expected")

       if rule_name in self.multiterminals :
          item = Terminal ()
          self.storePosition (item, pos)
          item.variable = variable
          item.multiterminal_name = rule_name
          if opt.add or opt.modify :
             self.error ("<add> or <modify> not allowed before multiterminal")
          # !?
          # if item.variable == "" :
          #      self.error ("Missing variable identifier before mutiterminal")
          if alt.leading_required :
             self.error ("Missing nonterminal (for rule with select or choose attribute)")
       else :
          item = Nonterminal ()
          self.storePosition (item, pos)

          item.variable = variable
          item.rule_name = rule_name

          item.add = opt.add
          item.modify = opt.modify

          if alt.leading_required  :
             alt.leading_required = False
             if rule.rule_mode == "select" :
                item.select_item = True
             if rule.rule_mode == "choose" :
                if alt.choose_alt :
                   item.choose_item = True
                   rule.choose_item = item
                else :
                   item.continue_item = True
             if item.variable != "" :
                self.error ("Variable identifier not allowed for first nonterminal (in rule with select or choose attribute)")

          if item.add or item.modify :
             if item.variable != "" :
                self.error ("Variable identifier not allowed after <add> or <modify> directive")

          if item.add :
             rule.add_used = True

       return item

   def parseTerminal (self) :
       item = Terminal ()
       self.setPosition (item)
       if self.token != self.character_literal and self.token != self.string_literal :
          self.error ("string expected")
       item.text = self.tokenText
       self.nextToken ()
       return item

# --------------------------------------------------------------------------

if __name__ == "__main__" :
    grammar = Grammar ()
    grammar.openFile ("pas.g")
    grammar.parseRules ()
    for rule in grammar.rules :
       print (rule.name)

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
