
# output.py

from __future__ import print_function

import sys, time

sectionGenerator = None

# --------------------------------------------------------------------------

class Output (object) :

   def __init__ (self) :

       self.indent = 0
       self.addSpace = False
       self.extraSpace = False
       self.startLine = True
       self.outputFile = sys.stdout
       self.sections = None

   def open (self, fileName, with_sections = False) :
       self.indent = 0
       self.addSpace = False
       self.extraSpace = False
       self.startLine = True

       if with_sections and sectionGenerator != None :
          self.sections = sectionGenerator.createSections (fileName)
          self.sections.open ()
       else :
          self.outputFile = open (fileName, "w")

   def close (self) :
       if self.sections != None :
          self.sections.close ()
       else :
          self.outputFile.close ()
       self.outputFile = sys.stdout

   def write (self, txt) :
       if self.sections != None :
          self.sections.write (txt)
       else :
          self.outputFile.write (txt)

   # put

   def incIndent (self) :
       self.indent = self.indent + 3

   def decIndent (self) :
       self.indent = self.indent - 3

   def put (self, txt) :
       if txt != "" :
          if self.startLine :
             self.write (" " * self.indent)
          self.startLine = False
          self.write (txt)

   def putEol (self) :
       self.startLine = True
       self.addSpace = False
       self.extraSpace = False
       self.write ("\n")

   def putCondEol (self) :
       if not self.startLine :
          self.putEol ()

   def putLn (self, txt = "") :
       if txt != "":
          self.put (txt)
       self.putEol ()

   # send

   def isLetterOrDigit (self, c) :
       return c >= 'A' and c <= 'Z' or c >= 'a' and c <= 'z' or c >= '0' and c <= '9' or c == '_'

   def send (self, txt) :
       if txt != "" :
          if self.startLine :
             self.write (" " * self.indent)
             self.addSpace = False
             self.extraSpace = False
          self.startLine = False

          c = txt [0]
          if c == ',' or c == ';' or c == ')' :
             self.extraSpace = False
          if not self.isLetterOrDigit (c) :
             self.addSpace = False

          if self.addSpace or self.extraSpace :
             txt = " " + txt

          self.write (txt)

          c = txt [-1]
          self.addSpace = self.isLetterOrDigit (c)
          self.extraSpace = c != '('

   def style_no_space (self) :
       self.extraSpace = False

   def style_indent (self) :
       self.putCondEol ()
       self.incIndent ()

   def style_unindent (self) :
       self.decIndent ()
       self.putCondEol ()

   def style_new_line (self) :
       self.putCondEol ()

   def style_empty_line (self) :
       self.putCondEol ()
       self.putLn ()

   # sections

   def setSections (self, sections_param) :
       self.sections = sections_param

   def openSection (self, obj) :
       if self.sections != None :
          self.sections.openSection (obj)

   def closeSection (self) :
       if self.sections != None :
          self.sections.closeSection ()

   def simpleSection (self, obj) :
       if self.sections != None :
          self.sections.simpleSection (obj)

   def setInk (self, ink) :
       if self.sections != None :
          self.sections.setInk (ink)

   def setPaper (self, paper) :
       if self.sections != None :
          self.sections.setPaper (paper)

   def addToolTip (self, text, tooltip) :
       if self.sections != None :
          self.sections.addToolTip (text, tooltip)

   def addDefinition (self, text, defn) :
       if self.sections != None :
          self.sections.addDefinition (text, defn)

   def addUsage (self, text, usage) :
       if self.sections != None :
          self.sections.addUsage (text, usage)

# --------------------------------------------------------------------------

if 1 :
   output = Output ()

   def openOutput (fileName) :
       output.open (fileName)

   def closeOutput () :
       output.close ()

   def incIndent () :
       output.incIndent ()

   def decIndent () :
       output.decIndent ()

   def put (txt) :
       output.put (txt)

   def putEol () :
       output.putEol ()

   def putCondEol () :
       output.putCondEol ()

   def putLn (txt = "") :
       output.putLn (txt)

   def send (txt) :
       output.send (txt)

   def style_no_space () :
       output.style_no_space ()

   def style_indent () :
       output.style_indent ()

   def style_unindent () :
       output.style_unindent ()

   def style_new_line () :
       output.style_new_line ()

   def style_empty_line () :
       output.style_empty_line ()

   def setSections (param) :
       output.setSections (param)

   def openSection (obj) :
       output.openSection (obj)

   def closeSection () :
       output.closeSection ()

   def simpleSection (obj) :
       output.simpleSection (obj)

# --------------------------------------------------------------------------

if 1 :
   start = 0

   def note (txt) :
       # txt == "" ... init timer
       if 0 :
          global start
          stop = time.clock ()
          if start == 0 :
             start = stop # first time measurement
          if txt != "" :
             print (txt + ", time %0.4f s" % (stop - start))
          start = stop

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
