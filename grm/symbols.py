
# symbols.py

from __future__ import print_function

from lexer import Separators
from grammar import Grammar, Rule, Expression, Alternative, Ebnf, Nonterminal, Terminal, Directive
from output import incIndent, decIndent, put, putEol, putCondEol, putLn

# --------------------------------------------------------------------------

class Symbol (object) :
   def __init__ (self) :
       self.inx = 0
       self.text = ""
       self.ident = ""
       self.alias = ""
       self.multiterminal = False
       self.keyword = False
       self.separator = False

# --------------------------------------------------------------------------

def addSymbol (grammar, symbol) :
    symbol.inx = grammar.symbol_cnt
    grammar.symbol_cnt = grammar.symbol_cnt + 1
    grammar.symbols.append (symbol)

def symbolsFromRules (grammar) :

    grammar.multiterminal_dict = {  }
    grammar.keyword_dict = { }
    grammar.separator_dict = { }

    # multiterminals

    multiterminal_list = [  ]

    for name in grammar.multiterminals :
        symbol = Symbol ()
        symbol.text = ""
        symbol.ident = name
        symbol.alias = "<" + name + ">"
        symbol.multiterminal = True
        multiterminal_list.append (symbol)
        grammar.multiterminal_dict [name] = symbol

    # setup rule_dict, keyword_dict, separator_dict

    grammar.rule_dict = { }

    for rule in grammar.rules :
        if rule.name in grammar.rule_dict :
           grammar.error ("Rule " + rule.name + " already defined")
        grammar.rule_dict [rule.name] = rule

    for rule in grammar.rules :
        grammar.updatePosition (rule)
        symbolsFromExpression (grammar, rule.expr)

    # list of symbols

    grammar.symbol_cnt= 0
    grammar.symbols = [ ]

    symbol = Symbol ()
    symbol.text = ""
    symbol.ident = "eos";
    symbol.alias = "<end of source text>";
    addSymbol (grammar, symbol)

    for symbol in multiterminal_list :
        addSymbol (grammar, symbol)

    symbol = Symbol ()
    symbol.text = ""
    symbol.ident = "separator";
    symbol.alias = "<unknown separator>";
    addSymbol (grammar, symbol)

    symbol = Symbol ()
    symbol.text = ""
    symbol.ident = "end_of_line";
    symbol.alias = "<end of line>";
    addSymbol (grammar, symbol)

    for key in sorted (grammar.keyword_dict.keys()) :
        symbol = grammar.keyword_dict [key]
        addSymbol (grammar, symbol)

    for key in sorted (grammar.separator_dict.keys()) :
        symbol = grammar.separator_dict [key]
        addSymbol (grammar, symbol)

    # for symbol in grammar.symbols :
    #     print symbol.inx, symbol.text

    if 0 :
       grammar.separator_dict["."].ident = "symbol_dot"
       grammar.separator_dict[","].ident = "symbol_comma"

# --------------------------------------------------------------------------

def symbolsFromExpression (grammar, expr) :
    for alt in expr.alternatives :
       symbolsFromAlternative (grammar, alt)

def symbolsFromAlternative (grammar, alt) :
    if alt.predicate != None :
       symbolsFromExpression (grammar, alt.predicate)
    for item in alt.items :
        if isinstance (item, Terminal) :
           symbolsFromTerminal (grammar, item)
        elif isinstance (item, Nonterminal) :
           symbolsFromNonterminal (grammar, item)
        elif isinstance (item, Ebnf) :
           symbolsFromEbnf (grammar, item)
        elif isinstance (item, Directive) :
            pass
        else :
           grammar.error ("Unknown alternative item " + item.__class__.__name__)

def symbolsFromEbnf (grammar, ebnf) :
    symbolsFromExpression (grammar, ebnf.expr)
    # if ebnf.impl != None :
    #    symbolsFromAlternative (grammar, ebnf.impl)

def symbolsFromNonterminal (grammar, item) :
    name = item.rule_name

    if name not in grammar.rule_dict:
       grammar.error ("Unknown rule: " + name)
    item.rule_ref = grammar.rule_dict [name]
     # print "NONTERMINAL", name

def symbolsFromTerminal (grammar, item) :
    grammar.updatePosition (item)
    if item.multiterminal_name != "" :
       name = item.multiterminal_name
       item.symbol_ref = grammar.multiterminal_dict [name]
       # print "MULTI-TERMINAL", name
    else :
       name = item.text
       if grammar.isLetter (name [0]) :
          if name in grammar.keyword_dict :
             symbol = grammar.keyword_dict [name]
          else :
             symbol = Symbol ()
             symbol.text = name
             symbol.ident = "keyword_" + name
             # symbol.alias = '"' + name + '"'
             symbol.alias = name
             symbol.keyword = True
             grammar.keyword_dict [name] = symbol
       else :
          if name in grammar.separator_dict :
             symbol = grammar.separator_dict [name]
          else :
             symbol = Symbol ()
             symbol.text = name
             symbol.ident = ""
             # symbol.alias = '"' + name + '"'
             symbol.alias = name
             symbol.separator = True
             grammar.separator_dict [name] = symbol
       item.symbol_ref = symbol
       # print "TERMINAL", name

# --------------------------------------------------------------------------

def nullableRules (grammar) :
    for rule in grammar.rules :
        rule.nullable = False

    grammar.nullableChanged = True
    while grammar.nullableChanged :
       grammar.nullableChanged = False
       # print "nullable step"
       for rule in grammar.rules :
           nullableRule (grammar, rule, complete = False)

    for rule in grammar.rules :
        nullableRule (grammar, rule, complete = True)

def nullableRule (grammar, rule, complete) :
    expr = rule.expr
    nullableExpression (grammar, expr, complete)
    if rule.nullable != expr.nullable :
       rule.nullable = expr.nullable
       grammar.nullableChanged = True

def nullableExpression (grammar, expr, complete) :
    # init
    expr.nullable = (len (expr.alternatives) == 0)

    for alt in expr.alternatives :
       nullableAlternative (grammar, alt, complete)

       # one alternative is nullable => expression is nullable
       if alt.nullable :
          expr.nullable = True
          if not complete :
             break

def nullableAlternative (grammar, alt, complete) :
    if alt.predicate != None and complete :
       nullableExpression (grammar, alt.predicate, complete)

    # init
    alt.nullable = True

    for item in alt.items :
        if isinstance (item, Terminal) :
           nullableTerminal (grammar, item)
        elif isinstance (item, Nonterminal) :
           nullableNonterminal (grammar, item)
        elif isinstance (item, Ebnf) :
           nullableEbnf (grammar, item, complete)
        elif isinstance (item, Directive) :
           pass
        else :
           grammar.error ("Unknown alternative item: " + item.__class__.__name__)

        if not isinstance (item, Directive) :
           # one item is not nullable => alternative is also not nullable
           if not item.nullable :
              alt.nullable = False
              if not complete :
                 break

def nullableEbnf (grammar, ebnf, complete) :
    nullableExpression (grammar, ebnf.expr, complete)
    # if ebnf.impl != None :
    #    nullableAlternative (grammar, ebnf.impl)

    expr = ebnf.expr
    # set ebnf according to expression
    ebnf.nullable = expr.nullable
    # ( )? or ( )* => ebnf is nullable
    if ebnf.mark == '?' or ebnf.mark == '*' :
       ebnf.nullable = True

def nullableNonterminal (grammar, item) :
    rule = item.rule_ref
    item.nullable = rule.nullable

def nullableTerminal (grammar, item) :
    item.nullable = False

# --------------------------------------------------------------------------

def firstFromRules (grammar) :
    for rule in grammar.rules :
        rule.first = newArray (grammar)

    # calculate first sets
    grammar.firstChanged = True
    while grammar.firstChanged :
       grammar.firstChanged = False
       # print "first step"
       for rule in grammar.rules :
           firstFromRule (grammar, rule, complete = False)

    for rule in grammar.rules :
        firstFromRule (grammar, rule, complete = True)

def firstFromRule (grammar, rule, complete) :
    expr = rule.expr
    firstFromExpression (grammar, expr, complete)

    equal = True
    for inx in range (grammar.symbol_cnt) :
        if rule.first [inx] != expr.first [inx] :
           equal = False

    if not equal :
       rule.first = expr.first
       grammar.firstChanged = True

def newArray (grammar) :
    return [False] * grammar.symbol_cnt

def firstFromExpression (grammar, expr, complete) :
    # init
    expr.first = newArray (grammar)

    for alt in expr.alternatives :
       firstFromAlternative (grammar, alt, complete)

       # add symbols from alternative to expression
       for inx in range (grammar.symbol_cnt) :
           if alt.first [inx] :
              if expr.first [inx] :
                 pass # grammar.error ("Conflict between alternatives")
              expr.first [inx] = True

def firstFromAlternative (grammar, alt, complete) :
    if alt.predicate != None and complete :
       firstFromExpression (grammar, alt.predicate, complete)

    # init
    alt.first = newArray (grammar)
    add = True

    for item in alt.items :
        if isinstance (item, Terminal) :
           firstFromTerminal (grammar, item)
        elif isinstance (item, Nonterminal) :
           firstFromNonterminal (grammar, item)
        elif isinstance (item, Ebnf) :
           firstFromEbnf (grammar, item, complete)
        elif isinstance (item, Directive) :
           pass
        else :
           grammar.error ("Unknown alternative item: " + item.__class__.__name__)

        if not isinstance (item, Directive) :
           if add :
              for inx in range (grammar.symbol_cnt) :
                  if item.first [inx] :
                     alt.first [inx] = True
           # item is not nullable => stop adding symbols from items
           if not item.nullable :
              add = False
              if not complete :
                 break

    # restrict to predicate set
    if alt.predicate != None and complete :
       if alt.nullable :
          for inx in range (grammar.symbol_cnt) :
              if alt.predicate.first [inx] :
                 alt.first [inx] = True
       else :
          for inx in range (grammar.symbol_cnt) :
              if not alt.predicate.first [inx] :
                 alt.first [inx] = False


def firstFromEbnf (grammar, ebnf, complete) :
    firstFromExpression (grammar, ebnf.expr, complete)
    # if ebnf.impl != None :
    #    firstFromAlternative (grammar, ebnf.impl)

    expr = ebnf.expr
    # set ebnf according to expression
    ebnf.first = expr.first

def firstFromNonterminal (grammar, item) :
    rule = item.rule_ref
    item.first = rule.first

def firstFromTerminal (grammar, item) :
    # first set has one item
    item.first = newArray (grammar)
    inx = item.symbol_ref.inx
    item.first [inx] = True

# --------------------------------------------------------------------------

def initSymbols (grammar) :
    symbolsFromRules (grammar)
    nullableRules (grammar)
    firstFromRules (grammar)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
