#!/usr/bin/env python

from __future__ import print_function

# import sys, os, re

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from util import findColor, findIcon, use_new_api

# --------------------------------------------------------------------------

class Options (QDialog) :
   def __init__ (self, win) :
       super (Options, self).__init__ (win)

       self.win = win
       self.page_count = 0
       self.current_tab = None

       self.setWindowTitle ("Options")
       # self.setStyleSheet ("QTabBar::tab:selected { color: orange; } QLineEdit {border: 1px solid green; } QLineEdit:focus {border: 1px solid red; }")

       "toolBar with buttons"
       self.toolBar = QToolBar ()
       self.toolBar.setOrientation (Qt.Vertical)

       "stackWidget for option pages"
       self.stackWidget = QStackedWidget ()

       "toolBar and stackWidget - central dialog area"
       self.hlayout = QHBoxLayout ()
       self.hlayout.addWidget (self.toolBar)
       self.hlayout.addWidget (self.stackWidget)

       self.buttonBox = QDialogButtonBox (QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
       self.buttonBox.accepted.connect (self.accept)
       self.buttonBox.rejected.connect (self.reject)

       "ok and cancel at the bottom"
       self.vlayout = QVBoxLayout ()
       self.vlayout.addLayout (self.hlayout)
       self.vlayout.addWidget (self.buttonBox)
       self.setLayout (self.vlayout)

       self.examplePage1 ()
       self.examplePage2 ()

   def addPage (self, name, icon, widget) :

       if isinstance (icon, str) :
          icon = findIcon (icon)

       self.stackWidget.addWidget (widget)

       index = self.page_count
       self.page_count = self.page_count + 1

       button = QPushButton ()
       button.setText (name)
       button.setIcon (icon)
       button.setMinimumSize (80, 80)
       button.clicked.connect (lambda param, self=self, index=index: self.stackWidget.setCurrentIndex (index))
       self.toolBar.addWidget (button)

   def addSimplePage (self, name, icon) :
       box = OptionsBox ()
       box.win = self.win
       self.addPage (name, icon, box)
       self.current_page = None
       self.current_tab = box
       return box

   def addTabPage (self, name, icon) :
       page = QTabWidget ()
       self.addPage (name, icon, page)
       self.current_page = page
       self.current_tab = None
       return page

   def addTab (self, name) :
       box = OptionsBox ()
       box.win = self.win
       self.current_page.addTab (box, name)
       return box

   def addLineEdit (self, settings_id, title, default_value, file_dialog = False) :
       layout = OptionsLineEditBox (self.win, self, settings_id, title, default_value, file_dialog)
       self.current_tab.tab_layout.addLayout (layout)

   def examplePage1 (self) :

       page = self.addTabPage  ("edit", "edit-undo")

       tab1 = self.addTab  ("first")

       edit = QLineEdit ()
       tab1.addWidgetItem (edit)

       checkBox = QCheckBox ()
       checkBox.setText ("something")
       tab1.addWidgetItem (checkBox)

       tab2 = self.addTab ("second")

   def examplePage2 (self) :
       page = self.addSimplePage ("other", "folder-new")

# --------------------------------------------------------------------------

class OptionsBox (QWidget) :
   def __init__ (self) :
       super (OptionsBox, self).__init__ ()
       self.tab_layout = QVBoxLayout ()
       self.setLayout (self.tab_layout)

   def addWidgetItem (self, widget) :
       self.tab_layout.addWidget (widget)

   def addLayoutItem (self, layout) :
       self.tab_layout.addLayout (layout)

# --------------------------------------------------------------------------

class OptionsLineEditBox (QHBoxLayout) :
   def __init__ (self, win, dlg, settings_id, title, default_value, file_dialog = False) :
       super (OptionsLineEditBox, self).__init__ ()
       self.win = win
       self.settings_id = settings_id

       self.label = QLabel ()
       self.label.setText (title)

       self.edit = QLineEdit ()

       self.addWidget (self.label)
       self.addWidget (self.edit)

       self.button = None
       if file_dialog :
          self.button = QPushButton ()
          self.button.setText ("...")
          self.addWidget (self.button)
          self.button.clicked.connect (self.buttonClick)

       # recall value
       value = self.win.settings.string (settings_id)
       if value == "" :
          value = default_value
          self.win.settings.setValue (settings_id, value) # write default value to .ini file
       self.edit.setText (value)

       dlg.accepted.connect (self.storeValue)

   def buttonClick (self) :
       value = str (QFileDialog.getOpenFileName ())
       if value != "" :
          self.edit.setText (value)


   def storeValue (self) :
       value = str (self.edit.text ())
       value = value.strip ()
       self.win.settings.setValue (self.settings_id, value)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
