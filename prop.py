#!/usr/bin/env python

from __future__ import print_function

import sys

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

# --------------------------------------------------------------------------

# class DataObject
#
#    _properties_
#

# --------------------------------------------------------------------------

class ItemEditor (QWidget) :
    def __init__ (self, parent = None) :
        super (ItemEditor, self).__init__ (parent)

        line_edit = None
        combo_box = None
        button = None

# --------------------------------------------------------------------------

class ListDelegate (QItemDelegate):

    def __init__ (self, parent=None) :
        super (ListDelegate, self).__init__ (parent)

    def createEditor(self, parent, option, index) :
        editor = ItemEditor (parent)

        layout =  QHBoxLayout (editor)
        layout.setMargin (0)
        layout.setSpacing (0)
        editor.setLayout (layout)

        if True :
           editor.combo_box = QComboBox (editor)
           layout.addWidget (editor.combo_box)
           editor.combo_box.addItems (["abc", "def", "klm"])
        else :
           editor.line_edit = QLineEdit (editor)
           layout.addWidget (editor.line_edit)

        if True :
           print ("Button ...")
           editor.button = QPushButton (editor)
           editor.button.setText ("...")
           editor.button.setMaximumWidth (32)
           layout.addWidget (editor.button)

        return editor

    def setEditorData (self, editor, index) :
        text = index.model().data(index, Qt.DisplayRole).toString()
        if editor.combo_box != None :
           i = editor.combo_box.findText (text)
           if i == -1:
              i = 0
           editor.combo_box.setCurrentIndex (i)

    def setModelData (self, editor, model, index) :
        if editor.combo_box != None :
           model.setData (index, editor.combo_box.currentText())

# --------------------------------------------------------------------------

class Property (QTableWidget) :

   def __init__ (self, parent=None) :
       super (Property, self).__init__ (parent)

       self.setColumnCount (2)
       self.setRowCount (6)

       self.setHorizontalHeaderLabels (["Name", "Value"])
       self.horizontalHeader().setStretchLastSection (True)
       self.verticalHeader().setVisible (False)

       self.setAlternatingRowColors (True)
       self.setEditTriggers (QTableWidget.AllEditTriggers)
       self.setSelectionBehavior (QTableWidget.SelectItems)
       self.setSelectionMode (QTableWidget.SingleSelection)

       # self.setItemDelegate (ListDelegate ())
       self.setItemDelegateForRow (5, ListDelegate ())

       item = QTableWidgetItem ()
       item.setText ("First")
       self.setItem (0, 0, item)

       item = QTableWidgetItem ()
       item.setText ("one")
       self.setItem (0, 1, item)

       item = QTableWidgetItem ()
       item.setText ("Second")
       # item.setData (Qt.BackgroundRole, QColor ("yellow"))
       # item.setData (Qt.ForegroundRole, QColor ("blue"))
       # item.setData (Qt.FontRole, QFont ("Courier", 16))
       self.setItem (1, 0, item)

       item = QTableWidgetItem ()
       item.setText ("two")
       # item.setData (Qt.EditRole, QColor ("yellow"))
       item.setData (Qt.DecorationRole, QColor ("yellow"))
       item.setData (Qt.CheckStateRole, Qt.Checked)
       self.setItem (1, 1, item)

       item = QTableWidgetItem ()
       item.setText ("Boolean")
       self.setItem (2, 0, item)

       item = QTableWidgetItem ()
       # item.setData (Qt.EditRole, QVariant (True))
       item.setData (Qt.EditRole, True)
       self.setItem (2, 1, item)

       item = QTableWidgetItem ()
       item.setText ("Integer")
       self.setItem (3, 0, item)

       item = QTableWidgetItem ()
       item.setData (Qt.EditRole, 1)
       item.setData (Qt.DisplayRole, 100)
       self.setItem (3, 1, item)

       item = QTableWidgetItem ()
       item.setText ("Double")
       self.setItem (4, 0, item)

       item = QTableWidgetItem ()
       item.setData (Qt.EditRole, 3.14)
       self.setItem (4, 1, item)

       item = QTableWidgetItem ()
       item.setText ("List")
       self.setItem (5, 0, item)

       item = QTableWidgetItem ()
       item.setData (Qt.EditRole, QColor ("yellow"))
       item.setData (Qt.EditRole, "klm")
       self.setItem (5, 1, item)

   def showProperty (self, inx, key, value) :

       item = QTableWidgetItem ()
       item.setText (key)
       self.setItem (inx, 0, item)

       item = QTableWidgetItem ()
       item.setText (str (value))
       self.setItem (inx, 1, item)

       if isinstance (value, QPen) :
          value = value.color ()
       if isinstance (value, QBrush) :
          value = value.color ()
       if isinstance (value, QColor) :
          item.setData (Qt.DecorationRole, value)
       if isinstance (value, bool) :
          if value :
             state = Qt.Checked
          else :
             state = Qt.Unchecked
          item.setData (Qt.CheckStateRole, state)

   def showProperties (self, data) :
       prop = { }

       if data != None :

          if hasattr (data, "__dict__") :
             for key in data.__dict__ :
                 value = data.__dict__ [key]
                 prop [key] = value

          if hasattr (data, "_properties_") :
             for key in data._properties_ :
                 value = getattr (data, key)
                 value = value ()
                 prop [key] = value

       last = [ "ink", "paper", "icon",
                "item_bold", "item_expand", "item_transparent"
                "item_label", "item_reference", "jump_table", "jump_label",
                "fileInx", "line", "column", "pos", "final_pos", "region_begin", "region_end",
                "item_name", "item_context", "item_qual", "item_dict", "item_list",
                "item_decl", "item_type", "type_name" ]

       names = [ ]

       keys = prop.keys ()
       keys.sort ()
       for key in keys :
          if key not in last :
             names.append (key)

       for key in last :
           if key in keys :
              names.append (key)

       cnt = len (names)
       self.setRowCount (cnt)
       inx = 0
       for key in names :
           value = prop [key]
           self.showProperty (inx, key, value)
           inx = inx + 1

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)
   window = Property ()
   window.show ()
   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
