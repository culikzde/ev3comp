#!/usr/bin/env python

from __future__ import print_function

import sys, os, re

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from util import findColor, findIcon, Text
from tree import Tree, TreeItem
from info import Info
from edit import Editor

from lexer import fileNameToIndex
from grammar import Grammar, Rule, Expression, Alternative, Ebnf, Nonterminal, Terminal

# --------------------------------------------------------------------------

class Documents (Tree) :
   def __init__ (self, win, toolTabs, editTabs) :
       super (Documents, self).__init__ (win)
       self.active = False
       self.win = win
       self.toolTabs = toolTabs
       self.editTabs = editTabs
       self.toolTabs.currentChanged.connect (self.onToolChanged)
       self.editTabs.currentChanged.connect (self.onEditorChanged)
       # self.win.tabChanged.connect (self.onEditorChanged)
       self.itemActivated.connect (self.onTreeItemActivated)
       self.header ().hide ()

   def onToolChanged (self) :
       self.active = self.toolTabs.currentWidget () == self
       if self.active :
          self.showData ()

   def onEditorChanged (self) :
       if self.active :
          self.showData ()

   def onTreeItemActivated (self, tree_node) :
       if hasattr (tree_node, "widget") :
          self.editTabs.setCurrentWidget (tree_node.widget)

   def showData (self) :
       self.clear ()
       # print ("showDocuments")
       cnt = self.editTabs.count ()
       for inx in range (cnt) :
           w = self.editTabs.widget (inx)
           if isinstance (w, Editor) :
              fileName = w.getFileName ()
              text = os.path.basename (fileName)
              node = TreeItem (self, text)
              node.setToolTip (0, fileName)
              node.fileName = fileName
              node.setIcon (0, findIcon ("document-open"))
              node.widget = w # !?
           else :
              text = self.editTabs.tabText (inx)
              node = TreeItem (self, text)
              node.setIcon (0, findIcon ("document-closed"))
              node.widget = w

# --------------------------------------------------------------------------

class Bookmarks (Tree) :
   def __init__ (self, win, toolTabs, editTabs) :
       super (Bookmarks, self).__init__ (win)
       self.active = False
       self.win = win
       self.toolTabs = toolTabs
       self.editTabs = editTabs
       self.toolTabs.currentChanged.connect (self.onToolChanged)

   def onToolChanged (self) :
       self.active = self.toolTabs.currentWidget () == self
       if self.active :
          self.showBookmarks ()

   def activate (self) :
       if self.active :
          self.showBookmarks ()

   def showBookmarks (self) :
       self.clear ()
       # print ("showBookmarks")
       cnt = self.editTabs.count ()
       for inx in range (cnt) :
           editor = self.editTabs.widget (inx)
           any = False
           branch = None
           if isinstance (editor, Editor) :
              fileName = editor.getFileName ()
              bookmarks = editor.getBookmarks ()
              document = None
              for bookmark in bookmarks :
                  if branch == None :
                     text = os.path.basename (fileName)
                     # text = text + " in " + os.path.dirname (fileName)
                     branch = TreeItem (self, text)
                     branch.fileInx = fileNameToIndex (fileName)
                     branch.setToolTip (0, fileName)
                     branch.setIcon (0, findIcon ("document-open"))
                     branch.setExpanded (True)
                     document = editor.document ()

                  text = document.findBlockByLineNumber (bookmark.line-1).text ()
                  text = "line " + str (bookmark.line) + ": " + text
                  node = TreeItem (branch, text)
                  node.line = bookmark.line
                  node.column = bookmark.column
                  node.setIcon (0, findIcon ("bookmarks"))

                  index = bookmark.mark
                  if index >= 1 and index <= len (Editor.bookmark_colors) :
                     node.setBackground (0, Editor.bookmark_colors [index-1])

# --------------------------------------------------------------------------

class Navigator (Tree) :
   def __init__ (self, win, toolTabs) :
       super (Navigator, self).__init__ (win)
       self.active = False
       self.win = win
       self.toolTabs = toolTabs
       self.toolTabs.currentChanged.connect (self.onToolChanged)
       self.win.tabChanged.connect (self.onEditorChanged)

   def onToolChanged (self) :
       self.active = self.toolTabs.currentWidget () == self
       if self.active :
          self.showNavigator ()

   def onEditorChanged (self) :
       if self.active :
          self.showNavigator ()

   def showNavigator (self) : # called from edit.py
       # print ("show navigator")
       self.clear ()
       editor = self.win.getEditor ()
       if editor != None :
          if hasattr (editor, "navigator_data") :
             IdentifierTree (self, editor.navigator_data)
          else :
             fileName = editor.getFileName ()
             name, ext = os.path.splitext (fileName)
             if ext == ".py" :
                PythonTree (self, editor)
             elif ext == ".g" :
                self.showGrammarRules (editor)
             else :
                self.showTextProperties (editor)
       else :
          w = self.win.getCurrentTab ()
          if hasattr (w, "showNavigator") :
             w.showNavigator ()

   def showGrammarRules (self, editor) :
       tree = self
       fileName = editor.getFileName ()
       self.fileInx = fileNameToIndex (fileName)
       source = str (editor.toPlainText ())
       lineNum = 0
       for line in source.split ("\n") :
           lineNum = lineNum + 1
           pattern = "(\s*)(\w\w*)\s*(<.*>)\s*:\s*"
           m = re.match (pattern, line)
           if m :
              name = m.group (2)
              node = TreeItem (tree, name)
              node.fileInx = self.fileInx
              node.line = lineNum
              node.addIcon ("class")

   def showTextProperties (self, editor) :
       tree = self
       fileName = editor.getFileName ()
       fileInx = fileNameToIndex (fileName)
       branch = tree
       block = editor.document().firstBlock()
       while block.isValid () :
          iterator = block.begin ()
          while not iterator.atEnd () :
             fragment = iterator.fragment ();
             if fragment.isValid () :
                fmt = fragment.charFormat ()
                if fmt.hasProperty (Text.defnProperty) :
                   name = str (fmt.stringProperty (Text.defnProperty))
                   node = TreeItem (branch, name)
                   node.fileInx = fileInx
                   node.line = block.blockNumber () + 1
             iterator += 1
          block = block.next ()

# --------------------------------------------------------------------------

class Memo (Tree) :
   def __init__ (self, win, toolTabs) :
       super (Memo, self).__init__ (win)
       self.win = win
       self.toolTabs = toolTabs
       self.itemDoubleClicked.connect (self.onItemDoubleClicked)

   def showMemo (self, editor, name) : # called from view.py

       self.toolTabs.setCurrentWidget (self)

       # print ("show memo (1)", name)
       obj = editor.findIdentifier (name)
       if obj != None :
          # print ("show memo (2)", obj.item_name)
          branch = TreeItem (self, obj.item_name)
          branch.obj = obj
          branch.setupTreeItem ()
          branch.setExpanded (True)
          if hasattr (obj, "item_list") :
             for item in obj.item_list :
                 node = TreeItem (branch, item.item_name)
                 node.obj = item
                 node.setupTreeItem ()

   def onItemActivated (self, tree_node, column) :
       pass # do not jump to object declaration

   def onItemDoubleClicked (self, node, column) :
       name = node.text (0)
       editor = self.win.getEditor ()
       cursor = editor.textCursor ()
       cursor.insertText (name)

# --------------------------------------------------------------------------

class References (Tree) :
   def __init__ (self, win, toolTabs) :
       super (References, self).__init__ (win)
       self.win = win
       self.toolTabs = toolTabs

   def showReferences (self, editor, name) : # called from edit.py
       self.clear ()
       fileName = editor.getFileName ()
       fileInx = fileNameToIndex (fileName)
       block = editor.document().firstBlock()
       while block.isValid () :
          iterator = block.begin ()
          while not iterator.atEnd () :
             fragment = iterator.fragment();
             if fragment.isValid () :
                fmt = fragment.charFormat ()
                if fmt.hasProperty (Text.infoProperty) :
                   value = str (fmt.stringProperty (Text.infoProperty))
                   if value == name :
                      line = block.blockNumber() + 1
                      text = str (block.text ())
                      text = "line " + str (line) + ": " + text.strip()
                      node = TreeItem (self, text)
                      node.fileInx = fileInx
                      node.line = line
             iterator += 1
          block = block.next ()
       self.toolTabs.setCurrentWidget (self)

# --------------------------------------------------------------------------

class Structure (Tree) :
   def __init__ (self, win, toolTabs) :
       super (Structure, self).__init__ (win)

       self.active = False
       self.win = win
       self.toolTabs = toolTabs
       self.toolTabs.currentChanged.connect (self.onToolChanged)
       self.win.tabChanged.connect (self.onEditorChanged)

   def onToolChanged (self) :
       self.active = self.toolTabs.currentWidget () == self
       if self.active :
          self.showData ()

   def onEditorChanged (self) :
       if self.active :
          self.showData ()

   def showData (self) :
       # print ("show structure")
       self.clear ()
       editor = self.win.getCurrentTab ()
       if isinstance (editor, QPlainTextEdit):
          self.showTextDocument (editor)

   def showTextDocument (self, editor) :
       tree = self
       fileName = None
       fileInx = None
       if hasattr (editor, "getFileName") :
          fileName = editor.getFileName ()
          fileInx = fileNameToIndex (fileName)

       branch = TreeItem (tree, "extra selections")
       extraSelections = editor.extraSelections ()
       for selection in extraSelections :
           cursor = selection.cursor
           fmt = selection.format
           line = cursor.blockNumber ()
           node = TreeItem (branch, "block " + str (line))
           node.fileInx = fileInx
           node.line = line + 1
           self.displayFormat (node, fmt)

       block = editor.document().firstBlock()
       while block.isValid () :
          line = block.blockNumber ()
          branch = TreeItem (tree, "block " + str (line) + " " + block.text ())
          branch.fileInx = fileInx
          branch.line = line + 1

          fmt = block.blockFormat ()
          self.displayFormat (branch, fmt)
          iterator = block.begin ()
          while not iterator.atEnd () :
             fragment = iterator.fragment ();
             if fragment.isValid () :
                node = TreeItem (branch, "fragment " + fragment.text ())
                fmt = fragment.charFormat ()
                self.displayFormat (node, fmt)
             iterator += 1
          block = block.next ()

   def displayProperty (self, above, fmt, title, property) :
       if fmt.hasProperty (property) :
          TreeItem (above, title + ": " + str (fmt.stringProperty (property)))

   def displayFormat (self, above, fmt) :
       above.setForeground (0, fmt.foreground ())
       above.setBackground (0, fmt.background ())
       if fmt.hasProperty (Text.bookmarkProperty) :
          inx = fmt.intProperty (Text.bookmarkProperty)
          node = TreeItem (above, "bookmark: " +  str (fmt.intProperty (Text.bookmarkProperty)))
          if inx >= 1 and inx <= len (Editor.bookmark_colors) :
             node.setBackground (0, Editor.bookmark_colors [inx-1])
       self.displayProperty (above, fmt, "tooltip", QTextFormat.TextToolTip)
       self.displayProperty (above, fmt, "location", Text.locationProperty)
       self.displayProperty (above, fmt, "info", Text.infoProperty)
       self.displayProperty (above, fmt, "defn", Text.defnProperty)
       self.displayProperty (above, fmt, "open", Text.openProperty)
       self.displayProperty (above, fmt, "close", Text.closeProperty)
       self.displayProperty (above, fmt, "outline", Text.outlineProperty)

# --------------------------------------------------------------------------

class GrammarTree (object) :
   def __init__ (self, tree, grammar, fileInx) :
       self.grammar = grammar
       self.fileInx = fileInx
       self.addSymbols (tree)

       for rule in grammar.rules :
          self.addBranch (tree, rule)

   def addSymbols (self, above) :
       branch = TreeItem (above, "symbols")
       for symbol in self.grammar.symbols :
           TreeItem (branch, str (symbol.inx) + " " + symbol.alias)

   def addBranch (self, above, data) :
       txt = ""
       if isinstance (data, Rule) :
          txt = data.name
       elif isinstance (data, Expression) :
          txt = "expression"
       elif isinstance (data, Alternative) :
          txt = "alternative"
       elif isinstance (data, Ebnf) :
          txt = "ebnf " + data.mark
       elif isinstance (data, Nonterminal) :
          txt = "nonterminal "
          if data.variable != "" :
             txt = txt + data.variable + ":"
          txt = txt + data.rule_name
       elif isinstance (data, Terminal) :
          if data.multiterminal_name != "" :
             if data.variable != "" :
                txt = txt + data.variable + ":"
             txt = "terminal " + data.multiterminal_name
          else :
             txt = "terminal " + data.text
       else:
          txt = data.__class__.__name__

       node = TreeItem (above, txt)

       if hasattr (data, "line") :
          node.fileInx = self.fileInx
          node.line = data.line

       if isinstance (data, Rule) :
          node.setToolTip (0, "line " + str (data.line))
          node.addIcon ("class")
       if isinstance (data, Nonterminal) :
          node.addIcon ("function")
       if isinstance (data, Terminal) :
          node.addIcon ("variable")
       if isinstance (data, Ebnf) :
          node.addIcon ("block")

       if isinstance (data, Rule) :
          if hasattr (data, "first") :
             n = 0
             for inx in range (len (data.first)) :
                 if data.first [inx] :
                    n = n + 1
             if n > 1 :
                node.setForeground (0, QBrush (Qt.green))

       if hasattr (data, "nullable") :
          if data.nullable :
             node.setForeground (0, QBrush (Qt.red))

       self.addInfo (node, data)

       if isinstance (data, Rule) :
          # self.addInfo (node, data)
          self.addBranch (node, data.expr)
       elif isinstance (data, Expression) :
          for t in data.alternatives :
             self.addBranch (node, t)
       elif isinstance (data, Alternative) :
          for t in data.items :
             self.addBranch (node, t)
       elif isinstance (data, Ebnf) :
          self.addBranch (node, data.expr)

   def addInfo (self, above, rule) :
       if hasattr (rule, "first") :
          branch = TreeItem (above, "first")
          branch.addIcon ("info")
          branch.setForeground (0, QBrush (Qt.blue))
          if hasattr (rule, "nullable") and rule.nullable :
             node = TreeItem (branch, "<empty>")
             node.setForeground (0, QBrush (Qt.red))
          for inx in range (len (rule.first)) :
              if rule.first [inx] :
                 name = self.grammar.symbols [inx].alias
                 node = TreeItem (branch, name)
                 node.setForeground (0, QBrush (Qt.blue))

# --------------------------------------------------------------------------

class CompilerTree (object) :
   def __init__ (self, tree, data) :
       self.showStructure (tree, "", data)

   def showStructureItem  (self, above, data, ident) :
       if ident != "items" :
          # if not hasattr (data, ident) :
          #    print ("missing " + ident + " in " +  data.__class__.__name__)
          if hasattr (data, ident) :
             item = data.__dict__ [ident]
             # print ("showStructure " + ident + " in " + str (item))
             if item == data :
                print ("recursion",  ident, str (item))
                temp = TreeItem (above, "recursion")
                temp.addIcon ("error")
             else :
                self.showStructure (above, ident, item)

   def showStructure (self, above, name, data) :
       if name != "" :
          name = name + " : "

       if data == None :
          TreeItem (above, name + "None")
       elif isinstance (data, int) :
          TreeItem (above, name + "int = " + str (data))
       elif isinstance (data, str) :
          TreeItem (above, name + "string = " + data)
       else :
          text = name + "object " + data.__class__.__name__
          if hasattr (data, "item_name") :
             if not isinstance (data.item_name, str) :
                print (data)
             text = text + " (" + data.item_name + ")"
          elif hasattr (data, "item_label") :
             text = text + " (" + data.item_label + ")"
          branch = TreeItem (above, text)
          branch.obj = data # show data object in property window
          branch.setupTreeItem ()

          if hasattr (data, "_fields_") :
             for ident in data._fields_ :
                 self.showStructureItem (branch, data, ident)
          elif hasattr (data, "__dict__") : # !?
             for ident in data.__dict__ :
                 self.showStructureItem (branch, data, ident)

          if hasattr (data, "items") and not isinstance (data, dict) :
             for item in data.items :
                self.showStructure (branch, "", item)

# --------------------------------------------------------------------------

def findCompilerData (tree, editor, line, column) :
    if isinstance (editor, QPlainTextEdit) :
       fileName =  editor.getFileName ()
       fileInx = fileNameToIndex (fileName)
       # print ("line", line, "column", column)

       iter = QTreeWidgetItemIterator (tree)
       while iter.value () :
          node = iter.value ()
          # print ("compiler data ", node.text (0))
          if hasattr (node, "obj") :
             obj = node.obj
             if hasattr (obj, "fileInx") :
                if obj.fileInx == fileInx :
                   if hasattr (obj, "line") :
                      if hasattr (obj, "column") :
                         # print ("obj.line", obj.line, "obj.column", obj.column)
                         found = obj.line > line or obj.line == line and obj.column >= column
                      else :
                         found = obj.line >= line
                      if found :
                         tree.setCurrentItem (node)
                         tree.scrollToItem (node)
                         break
          iter += 1

# --------------------------------------------------------------------------

class IdentifierTree (object) :
   def __init__ (self, tree, data) :
       if getattr (data, "item_name", "") == "" :
          self.showScope (tree, data)
       else :
          self.showItem (tree, data)

   def showItem (self, above, data) :
       if getattr (data, "item_transparent", False) :
          node = above
       else :
          node = TreeItem (above, data.item_name)
          node.obj = data
          node.setupTreeItem ()

       self.showScope (node, data)

       expand = getattr (data, "item_expand", False)
       if expand :
          node.setExpanded (True)

   def showScope (self, above, data) :
       if hasattr (data, "item_list") :
          for item in data.item_list :
              self.showItem (above, item)

# --------------------------------------------------------------------------

# Python classes and methods

class PythonTree (object) :
   def __init__ (self, tree, editor) :
       fileName = editor.getFileName ()
       self.fileInx = fileNameToIndex (fileName)
       source = str (editor.toPlainText ())

       lineNum = 0
       cls_branch = None
       for line in source.split ("\n") :
           lineNum = lineNum + 1
           pattern = "(\s*)(class|def)\s\s*(\w*)"
           m = re.match (pattern, line)
           if m :
              target = tree
              if m.group (1) != "" and cls_branch != None :
                 target = cls_branch

              is_class = m.group (2) == "class"

              name = m.group (3)
              if is_class :
                 name = "class " + name

              node = TreeItem (target, name)
              node.fileInx = self.fileInx
              node.line = lineNum

              if is_class :
                 node.addIcon ("class")
              else :
                 node.addIcon ("function")

              if is_class :
                 cls_branch = node

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
