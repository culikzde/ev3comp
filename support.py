
# support.py

from __future__ import print_function

import sys, os

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from util import findColor, findIcon, Text
from lexer import fileNameToIndex
from tree import TreeItem

# --------------------------------------------------------------------------

class Support (object) :

   def __init__ (self, parser, edit) :
       self.parser = parser
       self.edit = edit

       self.cursor = self.edit.textCursor ()
       self.colorInx = 0
       self.regionInx = 0
       self.regions = [ ]

       self.colorMap = { }
       self.colorNames = [ str (name) for name in QColor.colorNames ()]

   def nextColor (self) :
       self.colorInx = self.colorInx + 1
       h = (15 * self.colorInx) % 300
       return QColor.fromHsv (h, 255, 255)

   def nextRegionColor (self) :
       self.regionInx = self.regionInx + 1
       h = (45 * self.regionInx) % 300
       return QColor.fromHsv (h, 255, 255).lighter (195)

   def colorByName (self, name) :
       c = None
       if name in self.colorNames : # is it color name
          if name in self.colorMap : # is name already known
             c = self.colorMap [name]
          else :
             if QColor.isValidColor (name) : # is it valid color
                c = QColor (name)
                # (h1,s1,v1,alpha) = c.getHsv ()
                # (h2,s2,l1,alpha) = c.getHsl ()
                # print ("color", name, h1,s1,v1, l1)
                h = c.hue ()
                if h < 0 :
                   v = c.value ()
                   if v < 16 or v > 240 :
                      c = None
             self.colorMap [name] = c # store color for next queries
       return c

   # color region

   def openRegion (self, obj) :
       self.regions.append (obj)
       obj.region_begin = self.parser.tokenByteOfs

   def closeRegion (self) :
       obj = self.regions.pop ()
       obj.region_end = self.parser.prevEndOfs

       # paper color
       if len (self.regions) == 0 :
          self.cursor.setPosition (obj.region_begin)
          self.cursor.setPosition (obj.region_end, QTextCursor.KeepAnchor)

          fmt = self.cursor.blockFormat ()
          color = self.nextRegionColor ()
          fmt.setBackground (color)
          self.cursor.setBlockFormat (fmt)

   # text completion area

   def selectToken (self, prev = False) :
       if prev :
          # select previous token
          self.cursor.setPosition (self.parser.prevByteOfs)
          self.cursor.setPosition (self.parser.prevEndOfs, QTextCursor.KeepAnchor)
       else :
          # select following token
          self.cursor.setPosition (self.parser.tokenByteOfs)
          self.cursor.setPosition (self.parser.charByteOfs, QTextCursor.KeepAnchor)

   def openCompletion (self, obj, outside = False) :
       self.selectToken (prev = not outside)
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.openProperty, obj.item_qual)
       self.cursor.setCharFormat (fmt)

   def closeCompletion (self, obj, outside = False) :
       self.selectToken (prev = outside)
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.closeProperty, obj.item_qual)
       self.cursor.setCharFormat (fmt)

   # token definition / usage

   def selectColor (self, obj, defn = True) :
       color = None
       back_color = None

       if hasattr (obj, "ink") :
          color = obj.ink
       if hasattr (obj, "paper") :
          back_color = obj.paper

       if not defn :
          decl = getattr (obj, "item_decl", None)
          if decl == None :
             decl = getattr (obj, "item_reference", None)

          if decl != None :
             if hasattr (decl, "ink") :
                color = decl.ink
             if hasattr (decl, "paper") :
                back_color = decl.paper

       if hasattr (obj, "item_name") :
          c = self.colorByName (obj.item_name)
          if c != None : # found color with name
             color = c
             back_color = c.lighter (180)

       if color == None :
          color = self.nextColor ()

       if back_color == None and getattr (obj, "item_bold", False) :
          # (h, s, v, a) = color.getHsv ()
          # h = (h + 30) % 360
          # back_color = QColor.fromHsv (h, s, v)
          back_color = color
          back_color = back_color.lighter (180)
          # back_color = self.nextColor ().lighter (180)

       obj.ink = color
       obj.paper = back_color

   def mark (self, obj, defn) :

       self.selectColor (obj, defn)
       color = obj.ink
       back_color = obj.paper

       info = getattr (obj, "item_qual", None)
       if info == None and not defn :
          decl = getattr (obj, "item_decl", None)
          if decl == None :
             decl = getattr (obj, "item_reference", None)
          if decl != None :
             info = getattr (decl, "item_qual", None)

       self.cursor.setPosition (obj.pos)
       self.cursor.setPosition (obj.final_pos, QTextCursor.KeepAnchor)

       # set attributes
       fmt = self.cursor.charFormat ()
       fmt.setForeground (color)
       if back_color != None :
          fmt.setBackground (back_color)
       if defn :
          fmt.setFontUnderline (True) # important
          fmt.setUnderlineColor (color)
       if info != None :
          fmt.setProperty (Text.infoProperty, info)
          if defn :
             fmt.setProperty (Text.defnProperty, info)
          if 1 :
             fmt.setProperty (QTextFormat.TextToolTip, info)
       else :
          fmt.setProperty (Text.infoProperty, "") # no highlighting
       self.cursor.setCharFormat (fmt)

   def markDefn (self, obj) :
       self.mark (obj, defn = True)

   def markUsage (self, obj) :
       self.mark (obj, defn = False)

   def markText (self, obj, color, info = "", tooltip = "") :
       self.cursor.setPosition (obj.pos)
       self.cursor.setPosition (obj.final_pos, QTextCursor.KeepAnchor)
       fmt = self.cursor.charFormat ()
       # fmt.setProperty (Text.infoProperty, "") # no higligting
       fmt.setProperty (Text.infoProperty, info)
       fmt.setToolTip (tooltip)
       fmt.setBackground (color)
       self.cursor.setCharFormat (fmt)

   # prev / next function

   def markOutline (self, obj) :
       if hasattr (obj, "item_qual") :
          self.selectToken (prev = True)
          fmt = self.cursor.charFormat ()
          fmt.setProperty (Text.outlineProperty, obj.item_qual)
          self.cursor.setCharFormat (fmt)

   # text attributes

   def setInk (self, ink, prev = False) :
       if isinstance (ink, str) :
          ink = findColor (ink)
       self.selectToken (prev)
       fmt = self.cursor.charFormat ()
       fmt.setForeground (ink)
       self.cursor.setCharFormat (fmt)

   def setPaper (self, paper, prev = False) :
       if isinstance (paper, str) :
          paper = findColor (paper)
       self.selectToken (prev)
       fmt = self.cursor.charFormat ()
       fmt.setBackground (paper)
       self.cursor.setCharFormat (fmt)

   def addToolTip (self, tooltip, prev = False) :
       self.selectToken (prev)
       fmt = self.cursor.charFormat ()
       fmt.setToolTip (tooltip)
       self.cursor.setCharFormat (fmt)

# --------------------------------------------------------------------------

class Sections (object) :
   def __init__ (self, branch, edit) :
       self.branch = branch
       self.edit = edit
       self.cursor = self.edit.textCursor ()
       self.fileInx = fileNameToIndex (self.edit.getFileName ())
       self.shortFileName = os.path.basename (self.edit.getFileName ())

   def openSection (self, obj) :
       # print ("Sections.openSection " + str (obj))
       text = ""
       if hasattr (obj, "item_name") :
          text = obj.item_name
       else :
          text = str (obj)

       new_branch = TreeItem (self.branch, text)
       new_branch.obj = obj
       new_branch.fileInx = self.fileInx
       new_branch.line = self.cursor.blockNumber () + 1
       new_branch.setupTreeItem ()
       new_branch.parent_branch = self.branch # keep reference
       self.branch = new_branch

       if hasattr (obj, "item_name") :
          self.cursor.select (QTextCursor.WordUnderCursor)
          fmt = self.cursor.charFormat ()
          fmt.setProperty (Text.openProperty, obj.item_name)
          self.cursor.setCharFormat (fmt)

       # if hasattr (obj, "item_name") :
       if not hasattr (obj, "jump_table") :
          obj.jump_table = [ ]
       obj.jump_table.append (self.branch)
       self.branch.jump_label = self.shortFileName

   def closeSection (self) :
       if self.branch != None :
          obj = self.branch.obj
          if hasattr (obj, "item_name") :
             self.cursor.select (QTextCursor.WordUnderCursor)
             fmt = self.cursor.charFormat ()
             fmt.setProperty (Text.closeProperty, obj.item_name)
             self.cursor.setCharFormat (fmt)

          above = self.branch.parent ()
          if above != None :
             self.branch = above

   def simpleSection (obj) :
       self.openSection (obj)
       self.closeSection ()

   # text output

   def open (self) :
       self.edit.clear () # empty file

   def close (self) :
       self.edit.saveFile () # flush

   def write (self, txt) :
       self.cursor.insertText (txt)

   def setInk (self, ink) :
       if isinstance (ink, str) :
          ink = findColor (ink)
       fmt = self.cursor.charFormat ()
       if ink == None :
          fmt.clearForeground ()
       else :
          fmt.setForeground (ink)
       fmt.setProperty (Text.infoProperty, "") # no highlighting
       self.cursor.setCharFormat (fmt)

   def setPaper (self, paper) :
       if isinstance (paper, str) :
          paper = findColor (paper)
       fmt = self.cursor.charFormat ()
       if paper == None :
          fmt.clearBackground ()
       else :
          fmt.setBackground (ink)
       fmt.setProperty (Text.infoProperty, "") # no highlighting
       self.cursor.setCharFormat (fmt)

   def addToolTip (self, text, tooltip) :
       fmt = self.cursor.charFormat ()
       fmt.setToolTip (tooltip)
       self.cursor.setCharFormat (fmt)
       self.cursor.insertText (text)
       fmt.clearProperty (QTextFormat.TextToolTip)
       self.cursor.setCharFormat (fmt)

   def addDefinition (self, text, defn) :
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.defnProperty, defn)
       fmt.setProperty (Text.infoProperty, defn)
       self.cursor.setCharFormat (fmt)
       self.cursor.insertText (text)
       fmt.clearProperty (Text.defnProperty)
       fmt.clearProperty (Text.infoProperty)
       self.cursor.setCharFormat (fmt)

   def addUsage (self, text, usage) :
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.infoProperty, usage)
       self.cursor.setCharFormat (fmt)
       self.cursor.insertText (text)
       fmt.clearProperty (Text.infoProperty)
       self.cursor.setCharFormat (fmt)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
