#!/usr/bin/env python

from __future__ import print_function

import os, sys, importlib
# import subprocess

use_webkit = True
use_javascript = True
use_javascript_debugger = True

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
   if use_webkit :
      try :
         from PyQt5.QtWebKit import *
         from PyQt5.QtWebKitWidgets import *
      except :
         use_webkit = False
         print ("Missing PyQt5.QtWebKit")
   if use_javascript :
      try :
         from PyQt5.QtScript import *
      except :
         use_javascript = False
         print ("Missing PyQt5.QtScript")
   if use_javascript_debugger :
      try :
         from PyQt5.QtScriptTools import *
      except :
         use_javascript_debugger = False
         print ("Missing PyQt5.QtScriptTools")
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *
   if use_webkit :
      try :
         from PyQt4.QtWebKit import *
      except :
         use_webkit = False
         print ("Missing PyQt4.QtWebKit")
   if use_javascript :
      try :
         from PyQt4.QtScript import *
      except :
         use_javascript = False
         print ("Missing PyQt5.QtScript")
   if use_javascript_debugger :
      try :
         from PyQt4.QtScriptTools import *
      except :
         use_javascript_debugger = False
         print ("Missing PyQt4.QtScriptTools")

from util import findIcon

# --------------------------------------------------------------------------

class WebWin (QWidget):

    def __init__ (self, win) :
        super (WebWin, self).__init__ (win)
        self.win = win

        self.view = QWebView (self)

        self.progress = 0

        self.view.loadFinished.connect (self.adjustLocation)
        self.view.titleChanged.connect (self.adjustTitle)
        self.view.loadProgress.connect (self.setProgress)
        self.view.loadFinished.connect (self.finishLoading)

        self.locationEdit = QLineEdit (self)
        self.locationEdit.setSizePolicy (QSizePolicy.Expanding,
                                         self.locationEdit.sizePolicy().verticalPolicy())
        self.locationEdit.returnPressed.connect (self.changeLocation)

        toolBar = QToolBar ()
        toolBar.addAction (self.view.pageAction (QWebPage.Back))
        toolBar.addAction (self.view.pageAction (QWebPage.Forward))
        toolBar.addAction (self.view.pageAction (QWebPage.Reload))
        toolBar.addAction (self.view.pageAction (QWebPage.Stop))
        toolBar.addWidget (self.locationEdit)

        layout = QVBoxLayout ()
        self.setLayout (layout)
        layout.addWidget (toolBar)
        layout.addWidget (self.view)

    def load (self, url) :
        self.view.load (url)

    def adjustLocation (self) :
        self.locationEdit.setText (str (self.view.url().toString()))

    def changeLocation (self) :
        url = QUrl.fromUserInput (self.locationEdit.text ())
        self.view.load (url)
        self.view.setFocus ()

    def adjustTitle (self) :
        if 0 < self.progress < 100:
           self.setWindowTitle ("%s (%s%%)" % (self.view.title(), self.progress))
        else:
           self.setWindowTitle (self.view.title())

    def setProgress (self, p) :
        self.progress = p
        self.adjustTitle ()

    def finishLoading (self):
        self.progress = 100
        self.adjustTitle ()

# https://github.com/Werkov/PyQt4/blob/master/examples/webkit/fancybrowser/fancybrowser.py

# --------------------------------------------------------------------------

class ClickAction (QAction) :

    def __init__ (self, win, group) :
        super (ClickAction, self).__init__ (win)
        self.win = win
        self.group = group

        # read menu parameters
        self.win.commands.beginGroup (self.group)

        title = self.win.commands.string ("title", group)
        self.setText (title)

        shortcut = self.win.commands.string ("shortcut")
        if shortcut != "" :
           self.setShortcut (shortcut)

        tooltip = self.win.commands.string ("tooltip")
        if tooltip != "" :
           self.setToolTip (tooltip)
           self.setStatusTip (tooltip)

        icon = self.win.commands.string ("icon")
        if icon != "" :
           icon = findIcon (icon)
           if icon != None :
              self.setIcon (icon)

        self.win.commands.endGroup () # return to top-level group

        self.triggered.connect (self.click)

    def click (self) :
        # read run parameters from commands group
        self.win.commands.beginGroup (self.group)

        url = None
        inx = 1
        while url == None and inx < 10 :
           key = self.win.commands.string ("url" + str (inx))
           if key != "" :
              url = QUrl (key)
              if not url.isValid () :
                 url = None
              elif url.isLocalFile () :
                 path = url.toLocalFile ()
                 if not QFile.exists (path) :
                    url = None
           inx = inx + 1
        if url == None :
           url = ""
        else :
           url = url.toString ()

        cmd = self.win.commands.string ("cmd")

        jscript = self.win.commands.string ("jscript")

        module = self.win.commands.string ("module")
        func = self.win.commands.string ("func")

        work_dir = self.win.commands.string ("cd")

        self.win.commands.endGroup () # return to top-level group

        try:
           if work_dir != "" :
              if module != "" :
                 sys.path.insert (0, work_dir)
                 # do not change current directory when importing Python module
              else :
                 save_dir = os.getcwd ()
                 os.chdir (work_dir)

           if url != "" :
              # print ("URL", url)
              self.showUrl (url)

           if cmd != "" :
              # print ("CMD", cmd)
              self.runCommand (cmd)

           if jscript != "" :
              self.runJavaScript (jscript)

           if module != "" and func != "":
              self.runPythonModule (module, func)

        finally:
           if work_dir != "" :
              if module != "" :
                 del sys.path [0]
              else :
                 os.chdir (save_dir)

    def showUrl (self, url) :
        if self.win != None and use_webkit :
           widget = WebWin (self.win)
           self.win.firstTabWidget.addTab (widget, self.text ())
           self.win.firstTabWidget.setCurrentWidget (widget)
           widget.load (QUrl (url))

    def runCommand (self, cmd) :
        os.system (cmd)
        # subprocess.call (str (cmd), shell=True)

    def runJavaScript (self, fileName) :
        if use_javascript :
           with open (fileName, "r") as f :
                code = f.read ()
                engine = QScriptEngine ()
                if use_javascript_debugger :
                   debugger = QScriptEngineDebugger ()
                   debugger.attachTo (engine)
                   debugWindow = debugger.standardWindow ()
                   debugWindow.resize (1024, 640)
                   self.debugWindow = debugWindow # important: keep reference
                infoObject = engine.newQObject (self.win.info)
                engine.globalObject().setProperty ("info", infoObject)
                engine.evaluate (code, fileName)

    def runPythonModule (self, module, func) :
        m = self.win.loadModule (module)
        f = getattr (m, func)
        f ()

# --------------------------------------------------------------------------

def setupItem (win, menu, group) :
    separator = win.commands.string (group + "/separator", "0") != "0"
    if separator :
       act = menu.addSeparator ()
       act.dynamic = True
    else :
       act = ClickAction (win, group)
       act.dynamic = True
       menu.addAction (act)

def setupItems (win, menu, prefix) :
    groups = win.commands.childGroups ()
    for group in groups :
        group_name = str (group)
        if group_name.startswith (prefix) :
           setupItem (win, menu, group_name)

def removeItems (win, menu) :
    for act in menu.actions () :
        if hasattr (act, "dynamic") :
           menu.removeAction (act)
           # del act

def refreshToolsAndHelp (win) :
    removeItems (win, win.toolsMenu)
    removeItems (win, win.helpMenu)
    setupItems (win, win.toolsMenu, "tool")
    setupItems (win, win.helpMenu, "help")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
