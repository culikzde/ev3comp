#!/usr/bin/env python

from __future__ import print_function

import os, sys

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from util import findColor, findIcon

from lexer import indexToFileName

# --------------------------------------------------------------------------

# class TreeItem
#
#    fileName
#    fileInx
#    line
#    column
#    pos
#
#    obj

# --------------------------------------------------------------------------

# class TreeDataObject
#
#    fileInx
#    line
#    column
#    pos
#    final_pos
#
#    region_begin
#    region_end
#
#    icon
#    ink
#    paper
#
#    item_name
#    item_context
#    item_qual
#
#    item_dict
#    item_list
#
#    item_decl
#
#    item_type
#    type_name
#
#    item_value
#
#    item_obj
#
#    item_bold
#    item_expand
#    item_transparent
#
#    item_label
#    item_reference
#
#    jump_table
#    jump_label
#
#    _properties_
#
#    _fields_
#    items
#
#    description
#
#    method showNavigator ()

# configuration file $HOME/.kdevelop4/share/apps/katepart/syntax/python.xml

# --------------------------------------------------------------------------

# class Editor
#
#    grammar_data
#    compiler_data
#    navigator_data

# --------------------------------------------------------------------------

class TreeItem (QTreeWidgetItem) :
   def __init__ (self, parent, text = "") :
       super (TreeItem, self).__init__ (parent)
       self.setText (0, text)

   def addIcon (self, icon_name) :
       self.setIcon (0, findIcon (icon_name))

   def setInk (self, color_name) :
       self.setForeground (0, findColor (color_name))

   def setPaper (self, color_name) :
       self.setBackground (0, findColor (color_name))

# --------------------------------------------------------------------------

   def setupTreeItem (self) :
       tree_node = self
       obj = tree_node
       if hasattr (tree_node, "obj") :
          obj = tree_node.obj

       if hasattr (obj, "icon") :
          if isinstance (obj.icon, str) :
             tree_node.setIcon (0, findIcon (obj.icon))
          elif isinstance (obj.icon, QIcon) :
             tree_node.setIcon (0, obj.icon)

       if hasattr (obj, "ink") :
          if isinstance (obj.ink, str) :
             tree_node.setForeground (0, findColor (obj.ink))
          elif obj.ink != None :
             tree_node.setForeground (0, obj.ink)

       if hasattr (obj, "paper") :
          if isinstance (obj.paper, str) :
             tree_node.setBackground (0, findColor (obj.paper))
          elif obj.paper != None :
             tree_node.setBackground (0, obj.paper)

# --------------------------------------------------------------------------

   def treeItemActivated (self, win) :
       tree_node = self

       fileName = None
       line = None
       column = None
       pos = None

       if 1 :
          node = tree_node
          while node != None :
             obj = node

             # !?
             if hasattr (node, "obj") :
                obj = node.obj # coordinates from object

             if fileName == None :
                if hasattr (obj, "fileInx") :
                   fileName = indexToFileName (obj.fileInx)
             if line == None :
                if hasattr (obj, "line") :
                   line = obj.line
             if column == None:
                if hasattr (obj, "column") :
                   column = obj.column
             if pos == None :
                if hasattr (obj, "pos") :
                   pos = obj.pos
             node = node.parent ()

       if fileName == None :
          node = tree_node
          while node != None :
             if hasattr (node, "obj") :
                obj = node.obj # coordinates from object
                if fileName == None :
                   if hasattr (obj, "fileInx") :
                      fileName = indexToFileName (obj.fileInx)
                if line == None :
                   if hasattr (obj, "line") :
                      line = obj.line
                if column == None:
                   if hasattr (obj, "column") :
                      column = obj.column
                if pos == None :
                   if hasattr (obj, "pos") :
                      pos = obj.pos
             node = node.parent ()

       if win != None :

          if fileName != None :
             text = "file: " + fileName
             if line != None :
                text = text + ", line: " + str (line)
                if column != None :
                   text = text + ", column: " + str (column)
             win.showStatus ("tree item activated: " + text)

          if fileName != None :
             edit = win.loadFile (fileName, line, column)
             if edit != None and line == None and pos != None :
                edit.selectLineByPosition (pos)

          obj = getattr (tree_node, "obj", None)
          if obj != None :
             win.showProperties (obj)
          else :
             win.showProperties (tree_node)

# --------------------------------------------------------------------------

   def treeObjectActivated (self, win) :
       obj = self

       fileName = None
       line = None
       column = None
       pos = None

       if fileName == None :
          if hasattr (obj, "fileInx") :
             fileName = indexToFileName (obj.fileInx)
       if line == None :
          if hasattr (obj, "line") :
             line = obj.line
       if column == None:
          if hasattr (obj, "column") :
             column = obj.column
       if pos == None :
          if hasattr (obj, "pos") :
             pos = obj.pos

       if win != None :

          if fileName != None :
             edit = win.loadFile (fileName, line, column)
             if edit != None and line == None and pos != None :
                edit.selectLineByPosition (pos)

          win.showProperties (obj)

# --------------------------------------------------------------------------

class Tree (QTreeWidget):

    def __init__ (self, win = None) :
        super (Tree, self).__init__ (win)
        self.win = win

        self.header ().hide ()
        self.setAlternatingRowColors (True)

        self.setContextMenuPolicy (Qt.CustomContextMenu)
        self.customContextMenuRequested.connect (self.onContextMenu)

        self.setDragEnabled (True)
        self.viewport().setAcceptDrops (True) # <--

        self.setDragDropMode (self.DragDrop | self.InternalMove)
        self.setAcceptDrops (True)

        self.itemActivated.connect (self.onItemActivated)

    def dragEnterEvent (self, event) :
        print ("dragEnter")

    def dropEvent (self, event) :
        print ("drop")
        mime = event.mimeData ()
        # stackoverflow.com/questions/25222906/how-to-stop-qtreewidget-from-creating-the-item-duplicates-on-drag-and-drop

    def expandAllItems (self, node) :
        node.setExpanded (True)
        cnt = node.childCount ()
        inx = 0
        while inx < cnt :
           t = node.child (inx)
           self.expandAllItems (t)
           inx = inx + 1

    def expandFirstItems (self, node) :
        cnt = node.childCount ()
        while cnt > 0 :
           node.setExpanded (True)
           node = node.child (0) # first
           cnt = node.childCount ()

    def expandLastItems (self, node) :
        cnt = node.childCount ()
        while cnt > 0 :
           node.setExpanded (True)
           node = node.child (cnt-1) # last
           cnt = node.childCount ()

    def onItemActivated (self, node, column) :
        mask = Qt.ShiftModifier | Qt.ControlModifier | Qt.AltModifier | Qt.MetaModifier
        mod = int (QApplication.keyboardModifiers () & mask)
        if mod == Qt.ControlModifier :
           # print ("CONTROL CLICK")
           self.expandFirstItems (node)
        elif mod == Qt.ShiftModifier :
           # print ("SHIFT CLICK")
           self.expandLastItems (node)
        elif mod == int (Qt.ShiftModifier | Qt.ControlModifier ):
           # print ("CONTROL SHIFT CLICK")
           self.expandAllItems (node)
        else :
           # print ("CLICK")
           node.treeItemActivated (self.win)

    def onContextMenu (self, pos) :
        node = self.itemAt (pos)

        menu = QMenu (self)

        if hasattr (node, "obj") :
           act = menu.addAction ("tree node properties")
           act.triggered.connect (lambda param, self=self, node=node: self.showTreeNodeProperties (node))

        if hasattr (node, "obj") :
           obj = node.obj
        else :
           obj = node

        if hasattr (obj, "description") :
           act = menu.addAction ("macro description")
           # act.triggered.connect (lambda self=self, obj=obj: DescriptionWindow (self.win, obj.description))
           act.triggered.connect (lambda param, self=self, obj=obj: self.showDescription (obj))

        if hasattr (node, "fileInx") or  hasattr (node, "line") :
           act = menu.addAction ("jump to declaration")
           act.triggered.connect (lambda param, self=self, node=node: node.treeObjectActivated (self.win))

        # jump table

        if hasattr (obj, "jump_table") :
           jump_table = obj.jump_table

           if len (jump_table) != 0 :
              menu.addSeparator ()
              for item in jump_table :
                  text = getattr (item, "jump_label", "")
                  if text == "" :
                     text = str (item)
                  act = menu.addAction ("jump to " + text)
                  act.triggered.connect (lambda param, self=self, item=item: item.treeObjectActivated (self.win))

        # expand branch

        menu.addSeparator ()

        act = menu.addAction ("expand subitems")
        act.triggered.connect (lambda param, self=self, node=node: self.expandAllItems (node))

        act = menu.addAction ("expand first subitems")
        act.triggered.connect (lambda param, self=self, node=node: self.expandFirstItems (node))

        act = menu.addAction ("expand last subitems")
        act.triggered.connect (lambda param, self=self, node=node: self.expandLastItems (node))

        # show menu

        menu.exec_ (self.mapToGlobal (QPoint (pos)))

    def showTreeNodeProperties (self, node) :
        if self.win != None :
           self.win.showProperties (node)

    def showDescription (self, obj) :
        DescriptionWindow (self.win, obj.description)

# --------------------------------------------------------------------------

class Description (object) :
   def __init__ (self) :
      self.defn_ref = None
      self.in_parameters = False

      self.invoke_doc = QTextDocument ()
      self.source_doc = QTextDocument ()
      self.result_doc = QTextDocument ()

      self.invoke_cursor = QTextCursor (self.invoke_doc)
      self.source_cursor = QTextCursor (self.source_doc)
      self.result_cursor = QTextCursor (self.result_doc)

# --------------------------------------------------------------------------

class DescriptionWindow (QMainWindow):

   def __init__ (self, parent, desc) :
       super (DescriptionWindow, self).__init__ (parent)

       defn = desc.defn_ref
       code = "#define " + defn.name
       if defn.params != None :
          code = code + "("
          any = False
          for param in defn.params :
              if any :
                 code = code + ","
              any = True
              code = code + param
          code = code + ")"
       code = code + " " + defn.code

       self.splitter = QSplitter ()
       self.splitter.setOrientation (Qt.Vertical)

       self.define_edit = QTextEdit ()
       self.invoke_edit = QTextEdit ()
       self.source_edit = QTextEdit ()
       self.result_edit = QTextEdit ()

       self.splitter.addWidget (self.define_edit)
       self.splitter.addWidget (self.invoke_edit)
       self.splitter.addWidget (self.source_edit)
       self.splitter.addWidget (self.result_edit)

       self.define_edit.setPlainText (code)
       self.invoke_edit.setDocument (desc.invoke_doc)
       self.source_edit.setDocument (desc.source_doc)
       self.result_edit.setDocument (desc.result_doc)

       self.setCentralWidget (self.splitter)
       self.setWindowTitle (defn.name)
       self.show ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
