#!/usr/bin/env python

import sys

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

# --------------------------------------------------------------------------

class TreeDir (QTreeView):

   def __init__ (self, win) :
       super (TreeDir, self).__init__ (win)
       self.win = win

       path = QDir.currentPath ()

       if 0 :
          model = QDirModel ()
       else :
          model = QFileSystemModel ()
          model.setRootPath (path)

       model.setReadOnly (True)

       self.setModel (model)
       # NO self.setRootIndex (model.index (path))
       self.setCurrentIndex (model.index (path))

       # self.setAnimated (False)
       # self.setIndentation (20)
       self.setSortingEnabled (True)

       header = self.header ()
       if not use_qt5 :
          header.setResizeMode (0, QHeaderView.ResizeToContents)
          header.setResizeMode (1, QHeaderView.Fixed)
       header.hideSection (2)
       header.hideSection (3)

       self.activated.connect (self.onActivated)

   def onActivated (self, index) :
       if self.win != None :
          fileName = self.model().filePath (index)
          # self.win.showStatus (fileName)
          self.win.loadFile (fileName)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)
   window = TreeDir ()
   window.show ()
   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
