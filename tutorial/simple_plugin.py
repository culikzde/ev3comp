
# simple.py

from __future__ import print_function

import sys, os

from util import use_qt5

if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from util import findColor, findIcon, Text, defineColor

from lexer import Lexer, fileNameToIndex

from area import Area
from edit import Editor
from tree import TreeItem

# --------------------------------------------------------------------------

class Simple (object) :
   def __init__ (self) :
       self.ink = None
       self.paper = None
       self.tooltip = None

class Module (Simple) :
   def __init__ (self) :
       super (Module, self).__init__ ()
       self.name = ""
       self.items = [ ]

class Class (Simple) :
   def __init__ (self) :
       super (Class, self).__init__ ()
       self.name = ""
       self.items = [ ]

class Function (Simple) :
   def __init__ (self) :
       super (Function, self).__init__ ()
       self.name = ""
       self.type = ""
       self.parameters = [ ]

class Variable (Simple) :
   def __init__ (self) :
       super (Variable, self).__init__ ()
       self.name = ""
       self.type = ""
       self.value = ""
       self.items = [ ]

# --------------------------------------------------------------------------

class SimpleLexer (Lexer) :

   def __init__ (self) :
       super (SimpleLexer, self).__init__ ()
       self.cursor = None
       self.path = [ ]

   def openEditor (self, editor) :
       self.cursor = editor.textCursor ()
       self.openString (str (editor.toPlainText ()))
       self.setFileName (editor.getFileName ())
       self.fileInx = fileNameToIndex (editor.getFileName ())

   def startPos (self) :
       return self.tokenByteOfs

   def stopPos (self) :
       return self.byteOfs

   def moveCursor (self) :
       self.cursor.setPosition (self.tokenByteOfs)
       self.cursor.setPosition (self.tokenByteOfs + len (self.tokenText), QTextCursor.KeepAnchor)

   # set character format

   def setForeground (self, brush) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setForeground (brush)
       self.cursor.setCharFormat (fmt)

   def setBackground (self, brush) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setBackground (brush)
       self.cursor.setCharFormat (fmt)

   def setFontItalic (self, italic = True) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setFontItalic (italic)
       self.cursor.setCharFormat (fmt)

   def setUnderlineColor (self, color) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setFontUnderline (True) # important
       fmt.setUnderlineColor (color)
       self.cursor.setCharFormat (fmt)

   def setToolTip (self, tooltip) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setToolTip (tooltip)
       self.cursor.setCharFormat (fmt)

   def setInfo (self, info) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.infoProperty, info)
       self.cursor.setCharFormat (fmt)

   def setDefn (self, info) :
       self.moveCursor ()
       fmt = self.cursor.charFormat ()
       fmt.setProperty (Text.infoProperty, info)
       fmt.setProperty (Text.defnProperty, info)
       self.cursor.setCharFormat (fmt)

   # lexer

   def getIdentifier (self, msg = "") :
       name = ""
       if not self.isIdentifier () :
          if msg == "" :
             msg = "Identifier"
          self.error (msg + " expected")
       else :
          name = self.tokenText
       return name

   def readIdentifier (self, msg = "") :
       name = self.getIdentifier (msg)
       self.nextToken ()
       return name

   def readString (self) :
       if self.token != self.string_literal :
          self.error ("String expected")
       value = self.tokenText
       self.nextToken ()
       return value

   def optionalSemicolon (self) :
       if self.isSeparator (";") :
          self.nextToken ()

   # scope

   def openScope (self, name) :
       self.path.append (name)

   def closeScope (self) :
       self.path.pop ()

   def scopeName (self) :
       return "." . join (self.path)

# --------------------------------------------------------------------------

class SimpleParser (SimpleLexer) :

   def __init__ (self) :
       super (SimpleParser, self).__init__ ()
       self.fileName = ""

   def parseNamespace (self) :
       result = Module ()
       result.fileInx = self.fileInx
       result.line = self.tokenLineNum
       color = findColor ("namespace")
       self.setForeground (color)
       self.nextToken ()

       result.name = self.getIdentifier ("Module name")
       self.openScope (result.name)
       self.setDefn (self.scopeName ())
       self.setToolTip ("namespace identifier")
       self.setForeground (color)
       self.nextToken ()

       self.setToolTip ("opening namespace " + result.name)
       self.setForeground (color)
       self.checkSeparator ("{")

       while not self.isSeparator ("}") :
          if not self.parseAttributes (result) :
             item = self.parseCode ()
             result.items.append (item)

       self.setToolTip ("closing namespace " + result.name)
       self.setForeground (color)
       self.checkSeparator ("}")
       self.optionalSemicolon ()

       self.closeScope ()
       return result

   def parseClass (self) :
       result = Class ()
       result.line = self.tokenLineNum
       color = findColor ("class")
       self.setForeground (color)
       self.nextToken ()

       result.name = self.getIdentifier ("Class name")
       self.openScope (result.name)
       self.setDefn (self.scopeName ())
       self.setForeground (color)
       self.nextToken ()

       self.setForeground (color)
       self.checkSeparator ("{")
       while not self.isSeparator ("}") :
          if not self.parseAttributes (result) :
             item = self.parseCode ()
             result.items.append (item)
       self.setForeground (color)
       self.checkSeparator ("}")
       self.optionalSemicolon ()

       self.closeScope ()
       return result

   def parseParameter (self, func) :
       item = Variable ()
       # self.setUnderlineColor (Qt.blue)
       item.type = self.readIdentifier ("Parameter type")
       item.name = self.readIdentifier ("Parameter name")
       func.parameters.append (item)

   def parseFunction (self) :
       result = Function ()
       result.line = self.tokenLineNum
       color = findColor ("function")

       result.type = self.readIdentifier ("Function type")
       if self.isIdentifier () :
          result.name = self.readIdentifier ("Function name")
       else :
          result.name = result.type
          result.type = ""

       self.openScope (result.name)
       self.setDefn (self.scopeName ())

       self.setForeground (color)
       self.checkSeparator ("(")

       if not self.isSeparator (")") :
          self.parseParameter (result)
          while self.isSeparator (",") :
             self.nextToken ()
             self.parseParameter (result)

       self.setForeground (color)
       self.checkSeparator (")")

       if self.isSeparator ("{") :
          self.setForeground (color)
          self.checkSeparator ("{")

          level = 1
          while level > 0 :
             if not self.parseAttributes (result) :
                if self.isSeparator ("{") :
                   level = level + 1
                if self.isSeparator ("}") :
                   level = level - 1
                if level > 0 :
                   self.nextToken ()

          self.setForeground (color)
          self.checkSeparator ("}")

       self.optionalSemicolon ()
       self.closeScope ()

       return result

   def parseVariable (self) :
       result = Variable ()
       result.line = self.tokenLineNum
       color = findColor ("variable")

       result.type = self.readIdentifier ("Variable type")
       if self.isIdentifier () :
          result.name = self.readIdentifier ("variable name")
       else :
          result.name = result.type
          result.type = ""

       if result.name != "" :
          self.openScope (result.name)
          self.setDefn (self.scopeName ())
       self.setForeground (color)

       if self.isSeparator ("{") :
          self.setForeground (color)
          self.checkSeparator ("{")

          while not self.isSeparator ("}") :
             if not self.parseAttributes (result) :
                item = self.parseCode ()
                result.items.append (item)

          self.setForeground (color)
          self.checkSeparator ("}")
          self.optionalSemicolon ()

       else :
          if self.isSeparator ("=") :
             self.nextToken ()
             if self.token == self.identifier or self.token == self.string_literal :
                result.value = self.tokenText
                self.nextToken ()
             else :
                self.error ("Variable value expected")

          while not self.isSeparator (";") and not self.isSeparator ("}") and self.token != self.eos :
             self.nextToken ();

          self.checkSeparator (";")

       if result.name != "" :
          self.closeScope ()
       return result

   def parseAttributeWithParameter (self) :
       self.nextToken () # attribute name
       self.checkSeparator ("(")
       text = self.readString ()
       self.checkSeparator (")")
       self.optionalSemicolon ()
       return text

   def parseAttributes (self, target) :
       cont = True
       count = 0
       while cont :
          count = count + 1
          if self.isKeyword ("warning") :
             line = self.tokenLineNum
             col = self.tokenColNum
             text = self.parseAttributeWithParameter ()
             print (self.fileName + ":" + str (line) + ":" + str (col) + ": warning: " + text)
          elif self.isKeyword ("error") :
             line = self.tokenLineNum
             col = self.tokenColNum
             text = self.parseAttributeWithParameter ()
             print (self.fileName + ":" + str (line) + ":" + str (col) + ": error: " + text)
          elif self.isKeyword ("ink") :
             target.ink = self.parseAttributeWithParameter ()
          elif self.isKeyword ("paper") :
             target.paper = self.parseAttributeWithParameter ()
          elif self.isKeyword ("tooltip") :
             target.tooltip = self.parseAttributeWithParameter ()
          else :
             count = count - 1
             cont = False
       return count > 0

   def parseCode (self) :
       result = None

       if self.isKeyword ("namespace") :
          result = self.parseNamespace ()

       elif self.isKeyword ("class") :
          result = self.parseClass ()

       elif self.isIdentifier () :
          func = False
          pos = self.mark ()
          self.nextToken () # first identifier
          if self.isIdentifier () :
             self.nextToken () # next (optional) identifier
          if self.isSeparator ("(") :
             func = True
          self.rewind (pos)

          if func :
             result = self.parseFunction ()
          else:
             result = self.parseVariable ()

       else :
          self.error ("Declaration expected")

       return result

# --------------------------------------------------------------------------

class SimpleAttributes (object) :

    def __init__ (self, code) :
       self.branch (None, code)

    def branch (self, above, code) :

        color = None
        icon = None

        if isinstance (code, Module) :
           text = "namespace " + code.name
           color = "namespace"
           icon = "namespace"
        elif isinstance (code, Class) :
           text = "class " + code.name
           color = "class"
           icon = "class"
        elif isinstance (code, Function) :
           text = "function " + code.name
           color = "function"
           icon = "function"
        elif isinstance (code, Variable) :
           text = "variable " + str (code.name)
           if code.type != None :
              text = text + " : " + code.type
           color = "variable"
           icon = "variable"
        else :
           text = str (code)

        if color != None and getattr (code, "ink", None) == None :
           code.ink = color

        if icon != None and getattr (code, "icon", None) == None :
           code.icon = color

        if text != "" and getattr (code, "tooltip", None)  == None :
           code.tooltip = text

        if isinstance (code, Module) or isinstance (code, Class) or isinstance (code, Variable) :
           for item in code.items :
               self.branch (code, item)
        elif isinstance (code, Function) :
           for item in code.parameters :
               self.branch (code, item)

# --------------------------------------------------------------------------

class SimpleNames (object) :

    def __init__ (self, code) :
       self.branch (None, code)

    def branch (self, above, code) :
        if isinstance (code, Module) or isinstance (code, Class) or isinstance (code, Function) or isinstance (code, Variable) :
           code.item_name = code.name
           if above == None :
              code.item_qual = code.item_name
           else :
              code.item_qual = above.item_qual + "." + code.item_name

        if isinstance (code, Module) or isinstance (code, Class) :
           code.item_dict = { }
           code.item_list = [ ]
           for item in code.items :
               self.branch (code, item)
               code.item_dict [item.item_name] = item
               code.item_list.append (item)

# --------------------------------------------------------------------------

class SimpleDesigner (object) :

    def __init__ (self, design, code) :
       super (SimpleDesigner, self).__init__ ()
       self.design = design
       self.designBranch (code, 100, 100)
       design.addArea (code.area)

    def designBranch (self, code, x0, y0) :
        if isinstance (code, Variable) and (code.type.startswith ("Q") or code.type == "" and code.name.startswith ("Q")):
           self.designWidget (code, x0, y0)
        else :
           self.designArea (code, x0, y0)

    def designArea (self, code, x0, y0) :
        area = Area ()
        area.setToolTip (code.name)

        if isinstance (code, Module) :
           color = "namespace"
        elif isinstance (code, Class) :
           color = "class"
        elif isinstance (code, Function) :
           color = "function"
        elif isinstance (code, Variable) :
           color = "variable"
        else :
           color = ""

        code.area = area

        color = findColor (color)
        area.setBrush (QBrush (color))
        area.setPen (QPen (Qt.red))

        if code.ink != None:
           area.setPen (QColor (code.ink))
        if code.paper != None :
           area.setBrush (QColor (code.paper))
        if code.tooltip != None :
           area.setToolTip (code.tooltip)

        xspace = 20
        yspace = 20
        x = x0 + xspace
        y = y0 + yspace
        w = 60 + 2 * xspace
        h = 2 * yspace
        if hasattr (code, "items") :
           for item in code.items :
               self.designBranch (item, x, y)

               rect = item.area.boundingRect ()
               width = rect.width ()
               height = rect.height ()

               area.addArea (item.area)
               if isinstance (item.area, QGraphicsProxyWidget) :
                  item.area.setPos (x, y + yspace)
               else :
                  item.area.setRect (x, y, width, height)

               if w < width + 2*xspace :
                  w = width + 2*xspace
               y = y + height + yspace
               h = h + height + yspace

        # rect = area.boundingRect ()
        area.setRect (x0, y0, w, h)

    def designWidget (self, code, x0, y0) :
        if code.type == "" :
           code.type = code.name
           code.name = ""

        widget = eval (code.type + " ()")

        proxy = QGraphicsProxyWidget ()
        proxy.setWidget (widget)
        # proxy.setWindowFlags (Qt.Dialog)
        proxy.setFlag (QGraphicsItem.ItemIsMovable)
        proxy.setFlag (QGraphicsItem.ItemIsSelectable)

        code.area = proxy
        self.design.addArea (proxy)

        for item in code.items :
           if isinstance (item, Variable) :
              if item.value != None :
                 # setattr (widget, item.name, item.value)
                 quote = '\"'
                 if item.type == "QColor" :
                     param = "QColor (" + quote + item.value + quote + ")"
                 else :
                     param = quote + item.value + quote
                 eval ("widget.set" + item.name.capitalize() + " (" + param + ")")

# --------------------------------------------------------------------------

class SimpleHighlighter (QSyntaxHighlighter) :
   def __init__ (self, parent = None) :
       super (SimpleHighlighter, self).__init__ (parent)
       self.lexer = Lexer ()

       self.moduleFormat = self.format (Qt.green)
       self.classFormat = self.format (Qt.blue)
       self.identifierFormat = self.format (Qt.darkRed)
       self.qtFormat = self.format (findColor ("lime"))
       # self.variableFormat = self.format (QColor ("orange"))

   def format (self, color) :
       result = QTextCharFormat ()
       result.setForeground (color)
       return result

   def highlightBlock (self, text) :
       lexer = self.lexer
       lexer.openString (text)
       while not lexer.isEndOfSource () :
          index = lexer.tokenByteOfs
          length = lexer.charByteOfs - lexer.tokenByteOfs
          if lexer.token == lexer.identifier :
             if lexer.tokenText == "namespace" :
                self.setFormat (index, length, self.moduleFormat)
             elif lexer.tokenText == "class" :
                self.setFormat (index, length, self.classFormat)
             elif lexer.tokenText.startswith ("Q") :
                self.setFormat (index, length, self.qtFormat)
             else:
                self.setFormat (index, length, self.identifierFormat)
          lexer.nextToken ()

# --------------------------------------------------------------------------

class SimpleCompleter (QCompleter) :
   def __init__ (self, parent=None) :
       words = [ "namespace", "class", "function" ]
       super (SimpleCompleter, self).__init__ (words, parent)

# --------------------------------------------------------------------------

class SimplePlugin (QObject) :

   def __init__ (self, main_window) :
       super (SimplePlugin, self).__init__ (main_window)
       self.win = main_window

       self.simpleMenu = self.win.menuBar().addMenu ("Simple")
       self.simpleMenu.aboutToShow.connect (self.onShowSimpleMenu)

       act = QAction ("&Compile", self.win)
       # act.setShortcut ("Ctrl+F11")
       act.triggered.connect (self.simpleCompile)
       self.simpleMenu.addAction (act)

   def onShowSimpleMenu (self) :
       is_simple = self.win.hasExtension (self.win.getEditor(), ".sim")
       for act in self.simpleMenu.actions () :
          act.setEnabled (is_simple)

   def simpleCompile (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".sim") :
          source = str (e.toPlainText ())
          e.setPlainText (source) # reset colors and properties

          parser = SimpleParser ()
          parser.openEditor (e)

          code = parser.parseCode ()
          print ("parsed")

          if 1 :
             e.highlighter = SimpleHighlighter (e.document ()) # keep highlighter reference

          e.setCompleter (SimpleCompleter ())

          SimpleNames (code) # add item_name, item_qual, item_list, item_dict
          SimpleAttributes (code) # add tree attributes

          self.win.initProject (e) # Project

          self.win.showClasses (code) # Classes

          self.win.displayCompilerData (e, code) # Tree

          self.win.addIdentifiers (e, code) # Navigator

          SimpleDesigner (self.win.design, code)

# --------------------------------------------------------------------------

if __name__ == "__main__" :
    parser = SimpleParser ()
    parser.openFile ("tutorial/example.sim")
    code = parser.parseCode ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
