#!/usr/bin/env python

import sys, os, importlib

# --------------------------------------------------------------------------

if sys.version_info >= (3,) :
   use_python3 = True
else :
   use_python3 = False

# --------------------------------------------------------------------------

if not use_python3 :
   use_qt5 = False
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *
else :
   try :
      print ("trying PyQt5")
      from PyQt5.QtCore import *
      from PyQt5.QtGui import *
      from PyQt5.QtWidgets import *
      use_qt5 = True
      print ("PyQt5 found")
   except :
      use_qt5 = False
      print ("PyQt5 not found")
      from PyQt4.QtCore import *
      from PyQt4.QtGui import *
      print ("PyQt4 found")

# --------------------------------------------------------------------------

use_new_api = use_python3 or use_qt5

# --------------------------------------------------------------------------

class Text (object) :

   bookmarkProperty = QTextFormat.UserProperty
   locationProperty = QTextFormat.UserProperty + 1
   infoProperty = QTextFormat.UserProperty + 2
   defnProperty = QTextFormat.UserProperty + 3
   openProperty = QTextFormat.UserProperty + 4
   closeProperty = QTextFormat.UserProperty + 5
   contextProperty = QTextFormat.UserProperty + 6
   outlineProperty = QTextFormat.UserProperty + 7

# --------------------------------------------------------------------------

# Colors

color_cache = { }

def findColor (name):
    global color_cache
    if name in color_cache :
       return color_cache [name]
    else :
       color = QColor (name)
       color_cache [name] = color
       return color

def defineColor (alias, color):
    global color_cache
    if isinstance (color, QColor) :
       color_cache [alias] = color
    else :
       color_cache [alias] = findColor (color)

# --------------------------------------------------------------------------

# Icons

icon_path = [ ]
icon_cache = { }

def addIconDir (path):
    global icon_path
    path = os.path.expanduser (path)
    path = os.path.abspath (path)
    icon_path.append (path)

def findIcon ( name):
    global icon_path, icon_cache
    if name in icon_cache :
       icon = icon_cache [name]
    else :
       icon = None
       for path in icon_path :
           file_name = os.path.join (path, name + ".png")
           if os.path.isfile (file_name) :
              icon = QIcon (os.path.join (path, name + ".png"))
              break
       if icon == None :
          icon = QIcon.fromTheme (name)
       if icon != None :
          icon_cache [name] = icon
    return icon

def defineIcon (alias, name):
    global icon_cache
    if alias not in icon_cache : # if alias is not already defined
       icon = findIcon (name)
       if icon != None :
          icon_cache [alias] = icon # store icon under new name

# --------------------------------------------------------------------------

# Fonts

def setEditFont (edit) :
    font = QFont ("Luxi Mono", 14)
    font.setFixedPitch (True)
    edit.setFont (font)

def setZoomFont (edit) :
    edit.setStyleSheet ("QPlainTextEdit {font-size: 24px}")
    # font = QFont ("Luxi Mono", 24)
    # font.setFixedPitch (True)
    # e.setFont (font)

# --------------------------------------------------------------------------

def resizeDetailWindow (window) :
    window.resize (800, 640)

# def resizeBrowserWindow (detail_window) :
#     detail_window.resize (400, 600)

# --------------------------------------------------------------------------

def setApplStyle (app) :
    # app.setStyleSheet ("QWidget {font-size: 16px} Edit {font-family: \"Luxi Mono\" ; font-size: 16px} QDetailWindow {font-size: 24px} ")
    app.setStyleSheet ("QTabBar::tab:selected {color: blue }"
                       "QWidget {font-size: 16px}"
                       # "QToolTip { color: #000; background-color: yellow; border 1px solid}"
                       )

    QIcon.setThemeName ("oxygen") # /usr/share/icons/oxygen

    # defineIcon ("module",    "code-block")
    # defineIcon ("namespace", "code-block")
    defineIcon ("class",     "code-class")
    defineIcon ("enum",      "code-class")
    defineIcon ("function",  "code-function")
    defineIcon ("variable",  "code-variable")
    defineIcon ("type",      "code-typedef")
    defineIcon ("block",     "code-block")

    if 1 :
       addIconDir ("~/temp/monodevelop-2.8.8.4/src/core/MonoDevelop.Ide/icons")
       addIconDir ("~/temp/monodevelop-4.0.14/src/core/MonoDevelop.Ide/icons")
       # addIconDir ("~/temp/monodevelop-5.7/src/core/MonoDevelop.Ide/icons/light")
       # addIconDir ("~/temp/monodevelop-5.9/src/core/MonoDevelop.Ide/icons")

       defineIcon ("namespace", "element-namespace-16")
       defineIcon ("class",     "element-class-16")
       defineIcon ("function",  "element-method-16")
       defineIcon ("variable",  "element-field-16")
       defineIcon ("enum",      "element-enumeration-16")

    defineColor ("ink", QColor (0, 0, 0))
    defineColor ("paper", QColor (255, 255, 255))

    defineColor ("namespace", "green")
    defineColor ("class",     "blue")
    defineColor ("function",  "brown")
    defineColor ("variable",  "orange")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
