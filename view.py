#!/usr/bin/env python

from __future__ import print_function

import sys, os, importlib

work_dir = os.getcwd ()
sys.path.insert (1, os.path.join (work_dir, "grm")) # directory with parser
# sys.path.insert (1, os.path.join (work_dir, "ckit"))
# sys.path.insert (1, os.path.join (work_dir, "cmm"))
# sys.path.insert (1, os.path.join (work_dir, "tkit"))
# sys.path.insert (1, os.path.join (work_dir, "pas"))
sys.path.insert (1, os.path.join (work_dir, "ev3"))
sys.path.insert (1, os.path.join (work_dir, "tutorial"))

from util import use_qt5
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from lexer import indexToFileName, fileNameToIndex

from util import findIcon, findColor, setApplStyle, resizeDetailWindow, setZoomFont, use_new_api

from edit import Editor, FindBox, GoToLineBox, Bookmark
# from area import GraphicsWin, GraphicsWithPalette
from tree import Tree, TreeItem
# from treedir import TreeDir
# from grep import GrepWin
# from prop import Property
# from treeprop import TreeProperty
# from info import Info
from tools import refreshToolsAndHelp
from options import Options

# from small import Documents, Bookmarks, Navigator, Memo, References, Structure
from small import GrammarTree, CompilerTree, findCompilerData, IdentifierTree, PythonTree

from window import CentralWindow, CentralApplication

from support import Support, Sections

import lexer
import output

import gram_plugin
# import ckit_plugin
# import cmm_plugin
# import tkit_plugin
# import pas_plugin
import ev3_plugin
import tutorial_plugin
# import simple_plugin

# --------------------------------------------------------------------------

class ViewWindow (CentralWindow):

   def initTabWidget (self) :
       # design
       # self.design = GraphicsWithPalette (self)
       # self.firstTabWidget.addTab (self.design, "Design")
       pass

   def __init__ (self, parent = None) :
       super (ViewWindow, self).__init__ (parent)

       self.currentProject = None
       self.loaded_modules = { }
       self.options_dialog = Options (self)

       lexer.supportGenerator = self
       output.sectionGenerator = self

       # project menu

       self.runMenu = self.menuBar().addMenu ("&Project")
       menu = self.runMenu

       act = QAction ("&Grep", self)
       act.setShortcut ("F2")
       act.triggered.connect (self.findInFiles)
       menu.addAction (act)

       act = QAction ("Grep in output", self)
       act.triggered.connect (self.grepInOutput)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Configure", self)
       act.triggered.connect (self.configure)
       menu.addAction (act)

       act = QAction ("&Build", self)
       act.setShortcut ("F7")
       act.triggered.connect (self.build)
       menu.addAction (act)

       act = QAction ("&Make", self)
       act.setShortcut ("F8")
       act.triggered.connect (self.make)
       menu.addAction (act)

       act = QAction ("&Run", self)
       act.triggered.connect (self.run)
       menu.addAction (act)

       act = QAction ("&Debug", self)
       act.triggered.connect (self.debug)
       menu.addAction (act)

       act = QAction ("Install", self)
       act.triggered.connect (self.install)
       menu.addAction (act)

       act = QAction ("Clean", self)
       act.triggered.connect (self.clean)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("&Stop", self)
       act.setShortcut ("Ctrl+Esc")
       act.triggered.connect (self.info.stop)
       menu.addAction (act)
       self.info.stopAction = act

       act = QAction ("&Clear output", self)
       act.triggered.connect (self.info.clearOutput)
       menu.addAction (act)

       # grammar menu

       gram_plugin.GrammarPlugin (self)

       # CKit menu

       # ckit_plugin.Plugin (self)

       # C -- menu

       # cmm_plugin.Plugin (self)

       # TKit menu

       # tkit_plugin.Plugin (self)

       # Pascal menu

       # pas_plugin.PascalPlugin (self)

       # EV3 menu

       ev3_plugin.EV3Plugin (self)

       # tutorial menu

       tutorial_plugin.TutorialPlugin (self)

       # simple menu

       # simple_plugin.SimplePlugin (self)

       # settings menu

       self.settingsMenu = self.menuBar().addMenu ("&Settings")
       menu = self.settingsMenu

       act = QAction ("Re-read menu commands (.cmd)", self)
       act.setStatusTip ("Re-read project, tools and help menu commands from view.cmd file")
       act.triggered.connect (self.refreshCommands)
       menu.addAction (act)

       act = QAction ("Re-read settings (.ini)", self)
       act.setStatusTip ("Re-read settings from view.ini file")
       act.triggered.connect (self.refreshSettings)
       menu.addAction (act)

       act = QAction ("Re-read session (.cfg)", self)
       act.setStatusTip ("Re-read session from view.cfg file")
       act.triggered.connect (self.reloadSession)
       menu.addAction (act)

       # act = QAction ("Re-load modules", self)
       # act.setStatusTip ("Re-load some python modules and editor files")
       # act.triggered.connect (self.reloadModules)
       # menu.addAction (act)

       act = QAction ("Options ...", self)
       act.triggered.connect (self.showOptionsDialog)
       menu.addAction (act)

       # window and tabs menu

       self.additionalMenuItems ()

       # tools menu

       self.toolsMenu = self.menuBar().addMenu ("&Tools")
       menu = self.toolsMenu

       # help menu

       self.helpMenu = self.menuBar().addMenu ("&Help")
       menu = self.helpMenu

       refreshToolsAndHelp (self)

   # Project menu

   def findInFiles (self) :
       self.leftTabs.setCurrentWidget (self.grep)
       self.grep.dialog ()

   def grepInOutput (self) :
       text = ""
       text, ok = QInputDialog.getText (self, "Grep", "Find in files", QLineEdit.Normal, text)
       if ok and text != "" :
          self.info.grep (text)

   def configure (self) :
       self.info.run ("configure")

   def build (self) :
       self.info.run ("build")

   def make (self) :
       self.info.run ("make")

   def run (self) :
       self.info.run ("run")

   def debug (self) :
       self.info.run ("debuf")

   def clean (self) :
       self.info.run ("clean")

   def install (self) :
       self.info.run ("install")

   # Settings

   def refreshCommands (self) :
       self.commands.sync ()
       refreshToolsAndHelp (self)

   def refreshSettings (self) :
       self.settings.sync ()

   def showOptionsDialog (self) :
       self.options_dialog.show ()

   # Properties

   def showProperties (self, data) :
       self.prop.showProperties (data)

   # Project

   def initProject (self, prj) :
       self.info.clearOutput ()
       self.classes.clear ()
       self.tree.clear ()
       if isinstance (prj, Editor) :
          fileName = prj.getFileName ()
          text = os.path.basename (fileName)
          node = TreeItem (self.project, text)
          node.setToolTip (0, fileName)
          node.setIcon (0, findIcon ("folder"))
          node.fileInx = fileNameToIndex (fileName)
       elif isinstance (prj, str) :
          node = TreeItem (self.project, prj)
          node.setToolIcon (0, findIcon ("folder"))
       else :
          node = None # !?
       self.currentProject = node

   def joinProject (self, edit) :
       fileName = edit.getFileName ()
       text = os.path.basename (fileName)
       node = TreeItem (self.currentProject, text)
       node.setToolTip (0, fileName)
       node.setIcon (0, findIcon ("document-open"))
       node.fileInx = fileNameToIndex (fileName)

   # Clases

   def showClasses (self, data) :
       IdentifierTree (self.classes, data)
       # self.classes.expandAll ()

   # Tree

   def displayFile (self, fileName) :
       branch = TreeItem (self.tree, "output " + os.path.basename (fileName))
       branch.fileInx = fileNameToIndex (fileName)
       return branch

   def displayPythonCode (self, editor) :
       fileName =  editor.getFileName ()
       node = TreeItem (self.tree, "Python code " + os.path.basename (fileName))
       node.fileInx = fileNameToIndex (fileName)
       node.addIcon ("code")
       PythonTree (node, editor)

   def displayGrammarData (self, editor, grammar) :
       editor.grammar_data = grammar
       fileName =  editor.getFileName ()
       fileInx = fileNameToIndex (fileName)
       node = TreeItem (self.tree, "grammar " + os.path.basename (fileName))
       node.fileInx = fileInx
       GrammarTree (node, grammar, fileInx)

   def displayCompilerData (self, editor, data) :
       editor.compiler_data = data
       fileName =  editor.getFileName ()
       node = TreeItem (self.tree, "compiler data " + os.path.basename (fileName))
       node.fileInx = fileNameToIndex (fileName)
       CompilerTree (node, data)
       return node

   def findCompilerData (self, editor, line, col) :
       self.leftTabs.setCurrentWidget (self.tree)
       findCompilerData (self.tree, editor, line, col)

   # Navigator

   def addIdentifiers (self, editor, data) :
       editor.navigator_data = data

   # Memo and References

   def showMemo (self, editor, name) :
       self.memo.showMemo (editor, name)

   def showReferences (self, editor, name) :
       self.references.showReferences (editor, name)

   # Input files

   def inputFile (self, fileName) :
       edit = self.loadFile (fileName)
       if edit == None :
          raise  IOError ("File not found: " + fileName)
       if edit.isModified () :
          edit.saveFile ()
       return edit

   def rebuildFile (self, source, target) :
       return not os.path.isfile (target) or os.path.getmtime (source) > os.path.getmtime (target)

   def createSupport (self, parser, fileName) :
       edit = self.inputFile (fileName)
       return Support (parser, edit)

   # Output files

   def createDir (self, fileName) :
       dirName = os.path.dirname (fileName)
       if not os.path.isdir (dirName) :
          os.makedirs (dirName)

   def outputFileName (self, fileName, extension = "") :
       fileName = os.path.basename (fileName)
       fileName = os.path.join ("_output", fileName)
       if extension != "" :
          fileName, ext = os.path.splitext (fileName)
          fileName = fileName + extension
       fileName = os.path.abspath (fileName)
       self.createDir (fileName)
       return fileName

   def createSections (self, outputFileName) :
       outputEdit = self.newFile (outputFileName) # empty output file
       outputEdit.closeWithoutQuestion = True
       # outputEdit.highlighter.enabled = False
       self.joinProject (outputEdit)

       branch = self.displayFile (outputFileName)
       sections = Sections (branch, outputEdit)
       return sections

   # Modules

   def loadModule (self, fileName) :
       module = None
       try :
          sys.path.insert (0, os.path.dirname (fileName))
          name, ext = os.path.splitext (os.path.basename (fileName))
          if name in self.loaded_modules :
             reload (sys.modules [name])
          module = importlib.import_module (name)
          self.loaded_modules [name] = True
       finally:
          del sys.path [0]
       return module

   # Utilities

   def showHtml (self, fileName) :
       os.system ("( firefox" + " " + 1 * "--new-window " + fileName + " & ) &")

   def showPreview (self, fileName) :
       os.system ("caja" + " " + fileName + " &")

   def showPdf (self, fileName) :
       os.system ("atril" + " " + fileName + " &")

   def showLout (self, fileName, toPdf = False) :
       # create PDF or PostScript file

       subdir = os.path.dirname (fileName)
       save_dir = os.getcwd ()
       os.chdir (subdir)

       localName = os.path.basename (fileName) # local file name

       if toPdf :

          pdfFileName, ext = os.path.splitext (localName)
          pdfFileName = pdfFileName + ".pdf"

          os.system ("lout" + " " +  localName + " -PDF -o " + pdfFileName)
          self.showPdf (pdfFileName)

       else :

          psFileName, ext = os.path.splitext (localName)
          psFileName = psFileName + ".ps"

          # yum install lout
          os.system ("lout" + " " +  localName + " -o " + psFileName)
          # lout also creates .li file in current directory

          self.showPdf (psFileName)

       os.chdir (save_dir)

   # Reload modules

   def reloadModules (self) :

       # reload (sys.modules ["lexer"])

       # reload (gram_plugin)
       # gram_plugin.GrammarPlugin (self)

       # self.reloadFile ("grm/pas.g")
       # self.reloadFile ("grm/cmm.g")

       pass

# --------------------------------------------------------------------------

app = CentralApplication (sys.argv)
setApplStyle (app)

win = ViewWindow ()
app.win = win
win.appl = app
win.show ()

win.info.redirectOutput ()

win.loadFile ("tutorial/example.sim")

# win.reloadSession ()

app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
