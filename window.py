#!/usr/bin/env python

from __future__ import print_function

import sys, os

if __name__ == '__main__' :
   work_dir = os.getcwd ()
   sys.path.insert (1, os.path.join (work_dir, "grm")) # directory with parser


from util import use_qt5, use_new_api
if use_qt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *

from lexer import indexToFileName, fileNameToIndex
from util import findIcon, findColor, setApplStyle, resizeDetailWindow, setEditFont, setZoomFont

from edit import Editor, FindBox, GoToLineBox, Bookmark
from tree import Tree, TreeItem
from treedir import TreeDir
from grep import GrepWin
from prop import Property
# from treeprop import TreeProperty
from info import Info

from small import Documents, Bookmarks, Navigator, Memo, References, Structure

# --------------------------------------------------------------------------

class TabWidget (QTabWidget):

   def __init__ (self, parent = None, win = None) :
       super (TabWidget, self).__init__ (parent)
       self.win = win
       self.currentChanged.connect (self.onCurrentChanged)

   def onCurrentChanged (self, index) :
       if self.win != None :
          self.win.emitTabChanged (self, index)

# --------------------------------------------------------------------------

class MainWindow (QMainWindow):

   def __init__ (self, parent = None) :
       super (MainWindow, self).__init__ (parent)

# --------------------------------------------------------------------------

class DetailWindow (MainWindow):

   def __init__ (self, win) :
       super (DetailWindow, self).__init__ (win)
       self.win = win

       self.items = { }

       self.tabWidget = TabWidget (self, self.win)
       self.tabWidget.setMovable (True)
       self.setCentralWidget (self.tabWidget)

       resizeDetailWindow (self)

   def addItem (self, fileName) :
       if fileName not in self.items :
          w = Editor ()
          w.win = self.win
          w.tabWidget = self.tabWidget
          self.items [fileName] = w
          name = os.path.basename (fileName)
          self.tabWidget.addTab (w, name)
       w = self.items [fileName]
       self.tabWidget.setCurrentWidget (w)
       return w

# --------------------------------------------------------------------------

class Settings (QSettings):

   def __init__ (self, fileName, formatType) :
       super (QSettings, self).__init__ (fileName, formatType)

   def string (self, name, init = "") :
       result = self.value (name, init)
       if use_new_api :
          return str (result)
       else :
          return str (result.toString ())

# --------------------------------------------------------------------------

use_dock = False

class CentralWindow (MainWindow):

   tabChanged = pyqtSignal (TabWidget, int)

   def emitTabChanged (self, tab, index) :
       self.tabChanged.emit (tab, index)

   def initTabWidget (self) :
       pass

   def __init__ (self, parent = None) :
       super (CentralWindow, self).__init__ (parent)

       self.appl = None

       self.editors = { }

       self.commands = Settings ("view.cmd", QSettings.IniFormat)
       self.settings = Settings ("view.ini", QSettings.IniFormat)
       self.session = Settings ("view.cfg", QSettings.IniFormat)

       self.toolbar = self.addToolBar ("Main")
       self.status = self.statusBar ()

       self.generateCompletionList = None

       # center

       self.firstTabWidget = TabWidget (self, self)
       self.firstTabWidget.setMovable (True)

       self.initTabWidget ()

       self.findBox = FindBox ()
       self.findBox.hide ()

       self.lineBox = GoToLineBox ()
       self.lineBox.hide ()

       self.secondTabWidget = TabWidget (self, self)
       self.secondTabWidget.setMovable (True)
       self.secondTabWidget.hide ()

       # left

       self.leftTabs = QTabWidget ()
       self.leftTabs.setMovable (True)
       self.leftTabs.setTabPosition (QTabWidget.West)

       self.project = Tree (self)
       self.leftTabs.addTab (self.project, "Project")

       self.classes = Tree (self)
       self.leftTabs.addTab (self.classes, "Classes")

       self.tree = Tree (self)
       self.leftTabs.addTab (self.tree, "Tree")

       self.navigator = Navigator (self, self.leftTabs)
       self.leftTabs.addTab (self.navigator, "Navigator")

       self.files = TreeDir (self)
       self.leftTabs.addTab (self.files, "Files")

       self.grep = GrepWin (self)
       self.leftTabs.addTab (self.grep, "Grep")

       self.documents = Documents (self, self.leftTabs, self.firstTabWidget)
       self.leftTabs.addTab (self.documents, "Documents")

       self.bookmarks = Bookmarks (self, self.leftTabs, self.firstTabWidget)
       self.leftTabs.addTab (self.bookmarks, "Bookmarks")

       self.structure = Structure (self, self.leftTabs)
       inx = self.leftTabs.addTab (self.structure, "Structure")
       self.leftTabs.setTabToolTip (inx, "Text Structure")
       self.leftTabs.setTabWhatsThis (inx, "Text Structure")

       # right

       self.rightTabs = QTabWidget ()
       self.rightTabs.setMovable (True)
       self.rightTabs.setTabPosition (QTabWidget.East)

       self.prop = Property ()
       self.rightTabs.addTab (self.prop, "Properties")

       # self.treeProp = TreeProperty ()
       # self.rightTabs.addTab (self.treeProp, "Tree Properties")

       self.memo = Memo (self, self.rightTabs)
       self.rightTabs.addTab (self.memo, "Memo")

       self.references = References (self, self.rightTabs)
       self.rightTabs.addTab (self.references, "References")

       # bottom

       self.info = Info (self)

       # layout

       self.esplitter = QSplitter ()
       self.esplitter.setOrientation (Qt.Horizontal)
       self.esplitter.addWidget (self.firstTabWidget)
       self.esplitter.addWidget (self.secondTabWidget)
       self.esplitter.setStretchFactor (1, 1)

       self.vlayout = QVBoxLayout ()
       self.vlayout.addWidget (self.esplitter)
       self.vlayout.addWidget (self.findBox)
       self.vlayout.addWidget (self.lineBox)

       self.middle = QWidget ()
       self.middle.setLayout (self.vlayout)

       if not use_dock :
          self.hsplitter = QSplitter ()
          self.hsplitter.addWidget (self.leftTabs)
          self.hsplitter.addWidget (self.middle)
          self.hsplitter.addWidget (self.rightTabs)

          self.vsplitter = QSplitter ()
          self.vsplitter.setOrientation (Qt.Vertical)
          self.vsplitter.addWidget (self.hsplitter)
          self.vsplitter.addWidget (self.info)
          self.vsplitter.setStretchFactor (0, 3)
          self.vsplitter.setStretchFactor (1, 1)

          self.setCentralWidget (self.vsplitter)

       if use_dock :

          self.setCentralWidget (self.middle)

          self.setCorner (Qt.TopLeftCorner, Qt.LeftDockWidgetArea)
          self.setCorner (Qt.BottomLeftCorner, Qt.BottomDockWidgetArea)
          self.setCorner (Qt.TopRightCorner, Qt.RightDockWidgetArea)
          self.setCorner (Qt.BottomRightCorner, Qt.BottomDockWidgetArea)

          self.projectDock = self.newDock ("Project", self.project)
          self.addDockWidget (Qt.LeftDockWidgetArea, self.projectDock)

          self.classesDock = self.newDock ("Classes", self.classes)
          self.tabifyDockWidget (self.projectDock, self.classesDock)

          self.treeDock = self.newDock ("Tree", self.tree)
          self.tabifyDockWidget (self.classesDock, self.treeDock)

          self.navigatorDock = self.newDock ("Navigator", self.navigator)
          self.tabifyDockWidget (self.treeDock, self.navigatorDock)

          self.filesDock = self.newDock ("Files", self.files)
          self.tabifyDockWidget (self.navigatorDock, self.filesDock)

          self.grepDock = self.newDock ("Grep", self.grep)
          self.tabifyDockWidget (self.filesDock, self.grepDock)

          self.documentsDock = self.newDock ("Documents", self.documents)
          self.tabifyDockWidget (self.grepDock, self.documentsDock)

          self.bookmarksDock = self.newDock ("Bookmarks", self.bookmarks)
          self.tabifyDockWidget (self.documentsDock, self.bookmarksDock)

          self.structureDock = self.newDock ("Structure", self.structure)
          self.tabifyDockWidget (self.bookmarksDock, self.structureDock)

          self.propDock = self.newDock ("Properties", self.prop)
          self.addDockWidget (Qt.RightDockWidgetArea, self.propDock)

          self.treePropDock = self.newDock ("Tree Properties", self.treeProp)
          self.tabifyDockWidget (self.propDock, self.treePropDock)

          self.memoDock = self.newDock ("Memo", self.memo)
          self.tabifyDockWidget (self.treePropDock, self.memoDock)

          self.referencesDock = self.newDock ("References", self.references)
          self.tabifyDockWidget (self.memoDock, self.referencesDock)

          self.infoDock = self.newDock ("Output", self.info)
          self.addDockWidget (Qt.BottomDockWidgetArea, self.infoDock)

          self.dock_list = [self.treeDock, self.propDock, self.infoDock]
          self.dock_list = self.dock_list + self.tabifiedDockWidgets (self.treeDock)
          self.dock_list = self.dock_list + self.tabifiedDockWidgets (self.propDock)

          self.treeDock.raise_ ()
          self.propDock.raise_ ()

       # other windows

       self.other_win = DetailWindow (self)
       self.zoom_win = DetailWindow (self)

       self.other_win.setWindowTitle ("Other")
       self.zoom_win.setWindowTitle ("Zoom")

       self.left_win = QMainWindow (self)
       self.right_win = QMainWindow (self)
       self.bottom_win = QMainWindow (self)

       self.left_win.setWindowTitle ("Tree")
       self.right_win.setWindowTitle ("Properties")
       self.bottom_win.setWindowTitle ("Output")

       self.window_list = [ self, self.other_win, self.zoom_win,
                            self.left_win, self.right_win, self.bottom_win ]

       self.active_tab_widget = self.firstTabWidget
       self.alternate_tab_widget = self.secondTabWidget

       # file menu

       self.fileMenu = self.menuBar().addMenu ("&File")
       self.fileMenu.aboutToShow.connect (self.onShowFileMenu)
       menu = self.fileMenu

       act = QAction ("&Open...", self)
       act.setShortcut ("Ctrl+O")
       act.triggered.connect (self.openFile)
       menu.addAction (act)

       act = QAction ("&Save", self)
       act.setShortcut ("Ctrl+S")
       act.triggered.connect (self.saveFile)
       act.need_editor = True
       menu.addAction (act)

       act = QAction ("Save As...", self)
       act.triggered.connect (self.saveFileAs)
       act.need_editor = True
       menu.addAction (act)

       act = QAction ("&Close", self)
       act.setShortcut ("Ctrl+W")
       act.triggered.connect (self.closeFile)
       act.need_editor = True
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("&Quit", self)
       act.setShortcut ("Ctrl+Q")
       act.triggered.connect (self.close)
       menu.addAction (act)

       # edit menu

       self.editMenu = self.menuBar().addMenu ("&Edit")
       self.editMenu.aboutToShow.connect (self.onShowEditMenu)
       menu = self.editMenu

       act = QAction ("&Indent", self)
       act.setShortcut ("Ctrl+I")
       act.setStatusTip ("Indent the current line or selection")
       act.triggered.connect (self.indent)
       menu.addAction (act)

       act = QAction ("&Unindent", self)
       act.setShortcut ("Ctrl+U")
       act.setStatusTip ("Unindent the current line or selection")
       act.triggered.connect (self.unindent)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Comment", self)
       act.setShortcut ("Ctrl+D")
       act.triggered.connect (self.comment)
       menu.addAction (act)

       act = QAction ("Uncomment", self)
       act.setShortcut ("Ctrl+E")
       act.triggered.connect (self.uncomment)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("&Find", self)
       act.setShortcut ("Ctrl+F")
       act.triggered.connect (self.findText)
       menu.addAction (act)

       act = QAction ("&Replace", self)
       act.setShortcut ("Ctrl+R")
       act.triggered.connect (self.replaceText)
       menu.addAction (act)

       act = QAction ("Find &Next", self)
       act.setShortcut ("F3")
       act.triggered.connect (self.findNext)
       menu.addAction (act)

       act = QAction ("Find &Previous", self)
       act.setShortcut ("Shift+F3")
       act.triggered.connect (self.findPrev)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("&Go to Line", self)
       act.setShortcut ("Ctrl+G")
       act.triggered.connect (self.goToLine)
       menu.addAction (act)

       act = QAction ("Find Selected", self)
       act.setShortcut ("Ctrl+H")
       act.triggered.connect (self.findSelected)
       menu.addAction (act)

       act = QAction ("Find Incremental", self)
       act.setShortcut ("Ctrl+J")
       act.triggered.connect (self.findIncremental)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Set &Bookmark", self)
       act.setShortcut ("Ctrl+B")
       act.triggered.connect (lambda: self.setBookmark (1))
       menu.addAction (act)

       act = QAction ("Clear Bookmarks", self)
       act.setStatusTip ("Remove all bookmarks of the current document")
       act.triggered.connect (self.clearBookmarks)
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Enable highlighting ", self)
       act.triggered.connect (self.enableHighlighting)
       menu.addAction (act)

       # view menu

       self.viewMenu = self.menuBar().addMenu ("&View")
       self.viewMenu.aboutToShow.connect (self.onShowViewMenu)
       menu = self.viewMenu

       act = QAction ("Memo", self)
       act.setShortcut ("Alt+M")
       act.triggered.connect (self.viewMemo)
       act.need_editor = True
       menu.addAction (act)

       act = QAction ("References", self)
       act.setShortcut ("Alt+R")
       act.triggered.connect (self.viewReferences)
       act.need_editor = True
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Previous Function", self)
       act.setShortcut ("Alt+Up")
       act.triggered.connect (self.gotoPrevFunction)
       act.need_editor = True
       menu.addAction (act)

       act = QAction ("Next Function", self)
       act.setShortcut ("Alt+Down")
       act.triggered.connect (self.gotoNextFunction)
       act.need_editor = True
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Previous Bookmark", self)
       act.setShortcut ("Alt+PgUp")
       act.triggered.connect (self.gotoPrevBookmark)
       act.need_editor = True
       menu.addAction (act)

       act = QAction ("Next Bookmark", self)
       act.setShortcut ("Alt+PgDown")
       act.triggered.connect (self.gotoNextBookmark)
       act.need_editor = True
       menu.addAction (act)

       menu.addSeparator ()

       act = QAction ("Next output mark", self)
       act.setShortcut ("F4")
       act.setStatusTip ("Jump to next output mark")
       act.triggered.connect (self.info.jumpToNextMark)
       menu.addAction (act)

       act = QAction ("Previous output mark", self)
       act.setShortcut ("Shift+F4")
       act.setStatusTip ("Jump to previous output mark")
       act.triggered.connect (self.info.jumpToPrevMark)
       menu.addAction (act)

       act = QAction ("Last output mark", self)
       act.setShortcut ("Ctrl+F4")
       act.setStatusTip ("Jump to last output mark")
       act.triggered.connect (self.info.jumpToLastMark)
       menu.addAction (act)

   def additionalMenuItems (self) :

       # window menu

       self.windowMenu = self.menuBar().addMenu ("&Window")
       self.windowMenu.aboutToShow.connect (self.onShowWindowMenu)
       menu = self.windowMenu

       self.localAction (menu, "Show in other window",      self.showInOther,     "Alt+X")
       self.localAction (menu, "Show in zoom window",       self.showInZoom,      "Alt+Z")

       menu.addSeparator ()

       self.localAction (menu, "Move to left/top view",     self.moveEditorLeft,  "Ctrl+[")
       self.localAction (menu, "Move to right/bottom view", self.moveEditorRight, "Ctrl+]")

       menu.addSeparator ()

       self.globalAction (menu, "Split view left/right", self.splitViewRight,   "Ctrl+;")
       self.globalAction (menu, "Split view top/bottom", self.splitViewBottom,  "Ctrl+'")

       menu.addSeparator ()

       self.globalAction (menu, "Move properties left",  self.movePropertiesLeft,  "Meta+;")
       self.globalAction (menu, "Move properties right", self.movePropertiesRight, "Meta+'")

       menu.addSeparator ()

       if not use_dock :
           self.leftAction   = self.showSideAction (menu, "Left",   self.leftTabs,  "Ctrl+,")
           self.rightAction  = self.showSideAction (menu, "Right",  self.rightTabs, "Ctrl+.")
           self.bottomAction = self.showSideAction (menu, "Bottom", self.info,      "Ctrl+/")

           menu.addSeparator ()

           self.floatingAction (menu, "Floating &left window", "Meta+,", self.floatingLeft)
           self.floatingAction (menu, "Floating &right window", "Meta+.", self.floatingRight)
           self.floatingAction (menu, "Floating &bottom window", "Meta+/", self.floatingBottom)

       if use_dock :
           self.showBorderAction (menu, "Left",   Qt.LeftDockWidgetArea,   "Ctrl+,")
           self.showBorderAction (menu, "Right",  Qt.RightDockWidgetArea,  "Ctrl+.")
           self.showBorderAction (menu, "Bottom", Qt.BottomDockWidgetArea, "Ctrl+/")

       menu.addSeparator ()

       self.globalAction (menu, "Left window",  self.selectLeftWindow,  "Meta+Left")
       self.globalAction (menu, "Right window", self.selectRightWindow, "Meta+Right")
       self.globalAction (menu, "Above window", self.selectAboveWindow, "Meta+Up")
       self.globalAction (menu, "Below window", self.selectBelowWindow, "Meta+Down")

       # tabs menu

       self.tabsMenu = self.menuBar().addMenu ("T&abs")
       menu = self.tabsMenu

       if use_dock :
          self.leftTabs = None
          self.rightTabs = None

       self.showSideTabAction (menu, "Project",    self.leftTabs, self.project,   "Meta+P")
       self.showSideTabAction (menu, "Classes",    self.leftTabs, self.classes,   "Meta+C")
       self.showSideTabAction (menu, "Tree",       self.leftTabs, self.tree,      "Meta+T")
       self.showSideTabAction (menu, "Navigator",  self.leftTabs, self.navigator, "Meta+N")
       self.showSideTabAction (menu, "Files",      self.leftTabs, self.files,     "Meta+F")
       self.showSideTabAction (menu, "Grep",       self.leftTabs, self.grep,      "Meta+G")
       self.showSideTabAction (menu, "Documents",  self.leftTabs, self.documents, "Meta+D")
       self.showSideTabAction (menu, "Bookmarks",  self.leftTabs, self.bookmarks, "Meta+B")
       self.showSideTabAction (menu, "Structure",  self.leftTabs, self.structure, "Meta+S")

       menu.addSeparator ()

       self.globalAction (menu, "Edit",   self.showEditor, "Meta+E")
       self.globalAction (menu, "Output", self.showOutput, "Meta+O")

       menu.addSeparator ()

       self.showSideTabAction (menu, "Properties", self.rightTabs, self.prop,       "Meta+L")
       self.showSideTabAction (menu, "Memo",       self.rightTabs, self.memo,       "Meta+M")
       self.showSideTabAction (menu, "References", self.rightTabs, self.references, "Meta+R")

       menu.addSeparator ()

       act = self.globalAction (menu, "&Other window", self.showOther, "Meta+X")
       act.setStatusTip ("Open text in another window")
       self.otherAction = act

       act = self.globalAction (menu, "&Zoom window", self.showZoom, "Meta+Z")
       act.setStatusTip ("Open text in zoom window")
       self.zoomAction = act

       # keyboard shortcuts

       for inx in range (10) :
           self.setTabShortcut (inx)

       self.addShortcut (self.setFirstTab, "Alt+Home")
       self.addShortcut (self.setLastTab,  "Alt+End")
       self.addShortcut (self.setPrevTab,  "Alt+Left")
       self.addShortcut (self.setNextTab,  "Alt+Right")

       self.addShortcut (self.moveTabLeft,   ["Alt+,", "Alt+Shift+Left"])
       self.addShortcut (self.moveTabRight,  ["Alt+.", "Alt+Shift+Right"])
       self.addShortcut (self.placeTabFirst, ["Alt+[", "Alt+Shift+Home"])
       self.addShortcut (self.placeTabLast,  ["Alt+]", "Alt+Shift+End"])

       if not use_dock :
          self.addShortcut (self.leftToolUp,    "Meta+Ctrl+Up")
          self.addShortcut (self.leftToolDown,  "Meta+Ctrl+Down")
          self.addShortcut (self.leftToolHome,  "Meta+Ctrl+Home")
          self.addShortcut (self.leftToolEnd,   "Meta+Ctrl+End")
          self.addShortcut (self.rightToolUp,   "Meta+Ctrl+PgUp")
          self.addShortcut (self.rightToolDown, "Meta+Ctrl+PgDown")

   # Window menu

   def onShowWindowMenu (self) :
       if not use_dock :
          self.leftAction.setChecked  (self.leftTabs.isVisible ())
          self.rightAction.setChecked (self.rightTabs.isVisible ())
          self.bottomAction.setChecked  (self.info.isVisible ())

       is_edit = self.getEditor() != None
       self.otherAction.setEnabled (is_edit)
       self.zoomAction.setEnabled (is_edit)

   def addShortcuts (self, action, shortcut) :
       if shortcut != None and shortcut != "" :
          if isinstance (shortcut, list) :
             action.setShortcuts (shortcut)
          else :
             action.setShortcut (shortcut)

   def globalAction (self, menu, text, func, shortcut) :
       act = QAction (text, self)
       self.addShortcuts (act, shortcut)
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.triggered.connect (func)
       menu.addAction (act)
       return act

   def localAction (self, menu, text, func, shortcut) :
       act = QAction (text, self)
       self.addShortcuts (act, shortcut)
       act.triggered.connect (func)
       menu.addAction (act)
       return act

   # toggle (show/hide) widget

   def toggleWidgetAction (self, menu, text, widget, shortcut) :
       act = QAction (text, self)
       self.addShortcuts (act, shortcut)
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.triggered.connect (lambda: self.toggleWidget (widget))
       menu.addAction (act)
       return act

   def toggleWidget (self, widget) :
       widget.setVisible (not widget.isVisible ())

   # show output

   def showOutput (self) :
       "set focus and activate"
       widget = self.Info()
       widget.setFocus ()
       self.activateWindow ()

   # show editor

   def showEditor (self) :
       "set focus to current tab and activate"
       widget = self.firstTabWidget.currentWidget ()
       widget.setFocus ()
       self.activateWindow ()

   # change side tab (tool tab)

   def changeSideTab (self, widget, delta) :
       if delta == -2 :
          widget.setCurrentIndex (0)
       elif delta == 2:
          widget.setCurrentIndex (widget.count() - 1)
       elif delta == -1 :
          inx = widget.currentIndex ()
          if inx == 0 :
             inx = widget.count ()
          else :
             inx = inx - 1
          widget.setCurrentIndex (inx)
       elif delta == 1 :
          inx = widget.currentIndex ()
          inx = inx + 1
          if inx > widget.count () :
             inx = 0
          widget.setCurrentIndex (inx)

   def leftToolUp (self) :
       self.changeSideTab (self.leftTabs, -1)

   def leftToolDown (self) :
       self.changeSideTab (self.leftTabs, 1)

   def leftToolHome (self) :
       self.changeSideTab (self.leftTabs, -2)

   def leftToolEnd (self) :
       self.changeSideTab (self.leftTabs, 2)

   def rightToolUp (self) :
       self.changeSideTab (self.rightTabs, -1)

   def rightToolDown (self) :
       self.changeSideTab (self.rightTabs, 1)

   # show one tab (in side panel)

   def showSideTabAction (self, menu, text, sideTabs, widget, shortcut = None) :
       act = QAction (text, self)
       if shortcut != None and shortcut != "":
          self.addShortcuts (act, shortcut)
          act.setShortcutContext (Qt.ApplicationShortcut)
       act.triggered.connect (lambda: self.showSideTab (sideTabs, widget))
       menu.addAction (act)
       return act

   def showSideTab (self, sideTabs, widget) :
       if not use_dock :
          sideTabs.setVisible (True)
          sideTabs.setCurrentWidget (widget)
          widget.setFocus ()
       else :
          dockWidget = widget.parent ()
          dockWidget.setVisible (True)
          dockWidget.raise_ ()
          widget.setFocus ()

   # show side panel

   def showSideAction (self, menu, text, widget, shortcut) :
       act = QAction (text, self)
       act.setCheckable (True)
       act.setChecked (True)
       self.addShortcuts (act, shortcut)
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.triggered.connect (lambda: self.showSide (widget))
       menu.addAction (act)
       return act

   def showSide (self, widget) :
       w = widget.parent()
       if w != self and isinstance (w, QMainWindow):
          w.setVisible (not w.isVisible ())
          widget.setVisible (True)
       else :
          widget.setVisible (not widget.isVisible())

   # floating side panels (without docking)

   def floatingAction (self, menu, text, shortcut, func) :
       act = QAction (text, self)
       act.setCheckable (True)
       self.addShortcuts (act, shortcut)
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.toggled.connect (func)
       menu.addAction (act)

   def floatingLeft (self, checked) :
       if checked :
          self.left_win.setCentralWidget (self.leftTabs)
          self.left_win.setVisible (True)
       else :
          self.left_win.setVisible (False)
          self.hsplitter.insertWidget (0, self.leftTabs)
       self.leftTabs.setVisible (True)

   def floatingRight (self, checked) :
       if checked :
          self.right_win.setCentralWidget (self.rightTabs)
          self.right_win.setVisible (True)
       else :
          self.right_win.setVisible (False)
          self.hsplitter.addWidget (self.rightTabs)
       self.rightTabs.setVisible (True)

   def floatingBottom (self, checked) :
       if checked :
          self.bottom_win.setCentralWidget (self.info)
          self.bottom_win.setVisible (True)
       else :
          self.bottom_win.setVisible (False)
          self.vsplitter.addWidget (self.info)
       self.info.setVisible (True)

   # place rightTabs and leftTabs together

   def movePropertiesLeft (self) :
       inx = self.hsplitter.indexOf (self.rightTabs)
       if inx > 0 :
          self.hsplitter.insertWidget (inx-1, self.rightTabs)
          self.rightTabs.setTabPosition (QTabWidget.West)

   def movePropertiesRight (self) :
       inx = self.hsplitter.indexOf (self.rightTabs)
       cnt = self.hsplitter.count ()
       if inx >= 0 and inx < cnt-1 :
          self.hsplitter.insertWidget (inx+1, self.rightTabs)
          if inx+1 == cnt-1 :
             self.rightTabs.setTabPosition (QTabWidget.East)

   # side panel docking

   def newDock (self, title, widget) :
       result = QDockWidget (title, self)
       result.setWidget (widget)
       return result

   def showBorderAction (self, menu, text, dock_area, shortcut) :
       act = QAction (text, self)
       self.addShortcuts (act, shortcut)
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.triggered.connect (lambda: self.showBorder (dock_area))
       menu.addAction (act)
       return act

   def showBorder (self, dock_area) :
       widgets = [ w for w in self.dock_list if self.dockWidgetArea (w) == dock_area ]

       any = False
       for widget in widgets :
           if widget.isVisible () :
              any = True

       for widget in widgets :
          if any :
             widget.hide ()
          else :
             widget.show ()

   # detail windows

   def showOther (self) :
       if self.other_win.tabWidget.count () == 0 :
          self.showInOther ()
       self.other_win.show ()
       # self.other_win.setFocus ()
       self.other_win.activateWindow ()

   def showZoom (self) :
       if self.zoom_win.tabWidget.count () == 0 :
          self.showInZoom ()
       self.zoom_win.show ()
       # self.zoom_win.setFocus ()
       self.zoom_win.activateWindow ()

   def showInOther (self) :
       editor = self.getEditor ()
       if editor != None :
          fileName = editor.getFileName ()
          e = self.other_win.addItem (fileName)
          e.setDocument (editor.document ())
       if not self.other_win.isVisible() :
          self.other_win.show ()

   def showInZoom (self) :
       editor = self.getEditor ()
       if editor != None :
          fileName = editor.getFileName ()
          e = self.zoom_win.addItem (fileName)
          e.setReadOnly (True)

          document = editor.document ().clone()

          # QPlainTextEdit - change document layout
          layout = QPlainTextDocumentLayout (document)
          document.setDocumentLayout (layout)

          e.setDocument (document)

          setZoomFont (e)
          e.setLineWrapMode (QPlainTextEdit.NoWrap)
       if not self.zoom_win.isVisible() :
          self.zoom_win.show ()

   # top level windows

   def selectWindow (self, compare)  :
       done = False
       if QApplication.activeWindow () == self :
          done = self.selectLocalWindow (compare)
       if not done :
          self.selectTopLevelWindow (compare)

   def selectLocalWindow (self, compare)  :
       win_list = [ self.firstTabWidget,
                    self.secondTabWidget,
                    # self.leftTabs,
                    # self.rightTabs,
                    # self.info
                    ]

       win_list = [ w for w in win_list if w.isVisible () and w.parentWidget() != None ]
       win_list.sort (compare)

       focused = QApplication.focusWidget ()
       while focused != None and focused not in win_list :
          focused = focused.parentWidget ()
       # print ("focused", focused)
       # print ("win_list", win_list)

       answer = False
       if focused in win_list :
          inx = win_list.index (focused)
          if inx > 0 :
             inx = inx - 1
             widget = win_list [inx]
             if isinstance (widget, TabWidget) :
                widget = widget.currentWidget ()
             widget.setFocus ()
             print ("select", widget)
             answer = True
       return answer

   def selectTopLevelWindow (self, compare)  :
       win_list = [ w for w in self.window_list if w.isVisible () ]
       win_list.sort (compare)

       active = QApplication.activeWindow ()
       # print ("SELECT WINDOW")
       # for w in win_list :
       #     print ("WINDOW", w.windowTitle(), w.__class__, w.x (), w.y (), w == active)

       target = None
       cnt = len (win_list)
       inx = -1
       if active in win_list :
          inx = win_list.index (active)

       if cnt > 0 and inx >= 0 :
          if inx > 0 :
             target = win_list [inx-1]
          else :
             target = win_list [-1]

       if target != None and target != active :
          target.activateWindow ()

   def selectLeftWindow (self) :
       self.selectWindow (lambda a, b : cmp (a.x (), b.x ()))

   def selectRightWindow (self) :
       self.selectWindow (lambda a, b : cmp (b.x (), a.x ()))

   def selectAboveWindow (self) :
       self.selectWindow (lambda a, b : cmp (a.y (), b.y ()))

   def selectBelowWindow (self) :
       self.selectWindow (lambda a, b : cmp (b.y (), a.y ()))

   # split view, move editor between tab widgets

   def splitViewRight (self) :
       if self.esplitter.orientation () == Qt.Horizontal and self.secondTabWidget.isVisible() :
          self.secondTabWidget.hide ()
       else :
          self.esplitter.setOrientation (Qt.Horizontal)
          self.secondTabWidget.show ()

   def splitViewBottom (self) :
       if self.esplitter.orientation () == Qt.Vertical and self.secondTabWidget.isVisible() :
          self.secondTabWidget.hide ()
       else :
          self.esplitter.setOrientation (Qt.Vertical)
          self.secondTabWidget.show ()

   def moveEditor (self, source, target) :
       inx = source.currentIndex ()
       if inx >= 0 :
          text = source.tabText (inx)
          widget = source.widget (inx)
          source.removeTab (inx)
          inx = target.addTab (widget, text)
          target.setCurrentIndex (inx)
          if not target.isVisible() :
             target.show ()
          widget.setFocus ()

   def moveEditorLeft (self) :
       self.moveEditor (self.secondTabWidget, self.firstTabWidget)

   def moveEditorRight (self) :
       self.moveEditor (self.firstTabWidget, self.secondTabWidget)

   # show editor tabs

   def getTabWidget (self) :
       widget = None
       active = QApplication.activeWindow ()
       if active == self.zoom_win :
          widget = self.zoom_win.tabWidget
       elif active == self.other_win :
          widget = self.other_win.tabWidget
       else :
          widget = self.firstTabWidget
       return widget

   def setTabShortcut (self, inx) :
       act = QShortcut (self)
       act.setKey ("Alt+" + str (inx))
       act.setContext (Qt.ApplicationShortcut)
       act.activated.connect (lambda: self.setTab (inx))

   def setTab (self, inx, tabWidget = None, passive = False) :
       # print ("TAB", inx)
       if tabWidget == None :
          tabWidget = self.getTabWidget ()
       cnt = tabWidget.count ()
       if inx == 0 :
          inx = cnt # 0 ... last tabs
       if inx >= 1 and inx <= cnt :
          tabWidget.setCurrentIndex (inx-1)
          if not passive :
             tabWidget.currentWidget ().setFocus ()

   def addShortcut (self, func, shortcut) :
       if isinstance (shortcut, list) :
          for s in shortcut :
              self.addShortcut (func, s)
       else :
          act = QShortcut (self)
          act.setKey (shortcut)
          act.setContext (Qt.ApplicationShortcut)
          act.activated.connect (func)

   def setFirstTab (self, tabWidget = None, passive = False) :
       if tabWidget == None :
          tabWidget = self.getTabWidget ()
       self.setTab (1, tabWidget, passive)

   def setLastTab (self, tabWidget = None, passive = False) :
       if tabWidget == None :
          tabWidget = self.getTabWidget ()
       cnt = tabWidget.count ()
       self.setTab (cnt, tabWidget, passive)

   def setPrevTab (self, tabWidget = None, passive = False) :
       if tabWidget == None :
          tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex () + 1
       cnt = tabWidget.count ()
       if inx > 1 :
          inx = inx - 1
       else :
          inx = cnt
       self.setTab (inx, tabWidget, passive)

   def setNextTab (self, tabWidget = None, passive = False) :
       if tabWidget == None :
          tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex () + 1
       cnt = tabWidget.count ()
       if inx < cnt :
          inx = inx + 1
       else :
          inx = 1
       self.setTab (inx, tabWidget, passive)

   # move editor tabs

   def placeTab (self, new_inx) :
       tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex ()

       text = tabWidget.tabText (inx)
       tooltip = tabWidget.tabToolTip (inx)
       widget = tabWidget.widget (inx)

       tabWidget.removeTab (inx)

       tabWidget.insertTab (new_inx, widget, text)
       tabWidget.setTabToolTip (new_inx, tooltip)

       tabWidget.setCurrentIndex (new_inx)

   def moveTabLeft (self) :
       tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex ()
       cnt = tabWidget.count ()
       if inx > 0 :
          self.placeTab (inx - 1)

   def moveTabRight (self) :
       tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex ()
       cnt = tabWidget.count ()
       if inx < cnt - 1 :
          self.placeTab (inx + 1)

   def placeTabFirst (self) :
       tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex ()
       cnt = tabWidget.count ()
       if inx > 0 :
          self.placeTab (0)

   def placeTabLast (self) :
       tabWidget = self.getTabWidget ()
       inx = tabWidget.currentIndex ()
       cnt = tabWidget.count ()
       if inx < cnt - 1 :
          self.placeTab (cnt - 1)

   # Files

   def newEditor (self, fileName, noHighliting = False) :
       editor = Editor ()
       editor.win = self
       editor.tabWidget = self.firstTabWidget
       editor.findBox = self.findBox
       editor.lineBox = self.lineBox

       if noHighliting :
          editor.highlighter.enabled = False

       self.editors [fileName] = editor
       name = os.path.basename (fileName)

       inx = self.firstTabWidget.addTab (editor, name)
       self.firstTabWidget.setTabToolTip (inx, fileName)
       return editor

   def loadFile (self, fileName, line = 0, column = 0, reload = False, newFile = False, noHighlighting = False) :
       fileName = str (fileName) # optionally convert QStrint to string
       if not newFile and (fileName == "" or not os.path.isfile (fileName)) :
          # raise  IOError ("bad file name: " + fileName)
          return None
       else :
          fileName = os.path.abspath (fileName)
          if fileName in self.editors :
             editor = self.editors [fileName]
             if reload :
                if noHighlighting :
                   editor.highlighter.enabled = False
                editor.readFile (fileName)
          else :
             editor = self.newEditor (fileName, noHighlighting)
             if newFile :
                editor.writeFile (fileName)
             else :
                editor.readFile (fileName)
          self.firstTabWidget.setCurrentWidget (editor)
          if line != None and line != 0 :
             editor.selectLine (line)
          editor.setFocus ()
          return editor

   def reloadFile (self, fileName) :
       return self.loadFile (fileName, reload = True)

   def newFile (self, fileName) :
       return self.loadFile (fileName, newFile = True)

   # File menu

   def onShowFileMenu (self) :
       is_edit = self.getEditor() != None
       for act in self.fileMenu.actions () :
           if hasattr (act, "need_editor") :
              act.setEnabled (is_edit)

   def openFile (self) :
       fileName = str (QFileDialog.getOpenFileName (self, "Open File"))
       if fileName != "" :
          self.loadFile (fileName)

   def saveFile (self) :
       editor = self.getEditor ()
       if editor != None :
          # editor.saveFile ()
          editor.writeFile (editor.getFileName ())

   def saveFileAs (self) :
       editor = self.getEditor ()
       if editor != None :
          # editor.saveFileAs ()
          oldFileName = editor.getFileName ()
          fileName = str (QFileDialog.getSaveFileName (self, "Save File As", editor.getFileName ()))
          if fileName != "" :
             del self.editors [editor.getFileName ()]
             editor.writeFile (fileName)
             self.editors [editor.getFileName ()] = editor

   def closeFile (self) :
       editor = self.getEditor ()
       if editor != None :
          if self.checkSave (editor) :
             del self.editors [editor.getFileName ()]
             inx = self.firstTabWidget.indexOf (editor)
             if inx >= 0 :
                self.firstTabWidget.removeTab (inx)

   # Edit menu

   def onShowEditMenu (self) :
       is_edit = self.getEditor() != None
       for act in self.editMenu.actions () :
          act.setEnabled (is_edit)

   def indent (self) :
       e = self.getEditor ()
       if e != None :
          e.indent ()

   def unindent (self) :
       e = self.getEditor ()
       if e != None :
          e.unindent ()

   def comment (self) :
       e = self.getEditor ()
       if e != None :
          e.comment ()

   def uncomment (self) :
       e = self.getEditor ()
       if e != None :
          e.uncomment ()

   def findText (self) :
       e = self.getEditor ()
       if e != None :
          e.findText ()

   def findNext (self) :
       e = self.getEditor ()
       if e != None :
          e.findNext ()

   def findPrev (self) :
       e = self.getEditor ()
       if e != None :
          e.findPrev ()

   def replaceText (self) :
       e = self.getEditor ()
       if e != None :
          e.replaceText ()

   def findSelected (self) :
       e = self.getEditor ()
       if e != None :
          e.findSelected ()

   def findIncremental (self) :
       e = self.getEditor ()
       if e != None :
          e.findIncremental ()

   def goToLine (self) :
       e = self.getEditor ()
       if e != None :
          e.goToLine ()

   def setBookmark (self, markType = 1) :
       e = self.getEditor ()
       if e != None :
          e.setBookmark (markType)

   def clearBookmarks (self) :
       e = self.getEditor ()
       if e != None :
          e.clearBookmarks ()

   def enableHighlighting (self) :
       e = self.getEditor ()
       if e != None :
          e.highlighter.enabled = True

   # View menu

   def onShowViewMenu (self) :
       is_edit = self.getEditor() != None
       for act in self.viewMenu.actions () :
           if hasattr (act, "need_editor") :
              act.setEnabled (is_edit)

   def gotoPrevFunction (self) :
       e = self.getEditor ()
       if e != None :
          e.gotoPrevFunction ()

   def gotoNextFunction (self) :
       e = self.getEditor ()
       if e != None :
          e.gotoNextFunction ()

   def gotoPrevBookmark (self) :
       e = self.getEditor ()
       if e != None :
          e.gotoPrevBookmark ()

   def gotoNextBookmark (self) :
       e = self.getEditor ()
       if e != None :
          e.gotoNextBookmark ()

   def viewMemo (self) :
       e = self.getEditor ()
       if e != None :
          e.showMemo ()

   def viewReferences (self) :
       e = self.getEditor ()
       if e != None :
          e.showReferences ()

   # Store session

   def storeSession (self) :
       self.session.clear ()
       inx = 0
       for key in self.editors :
           editor = self.editors [key]
           inx = inx + 1
           self.session.beginGroup ("edit-" + str (inx))
           self.session.remove ("")
           self.session.setValue ("fileName", editor.getFileName ())
           cursor = editor.textCursor ()
           self.session.setValue ("line", cursor.blockNumber () + 1)
           self.session.setValue ("column", cursor.positionInBlock () + 1)

           bookmarks = editor.getBookmarks ()
           self.session.beginWriteArray ("bookmarks")
           n = 0
           for item in bookmarks :
               self.session.setArrayIndex (n)
               self.session.setValue ("line", item.line)
               self.session.setValue ("column", item.column)
               self.session.setValue ("mark", item.mark)
               n = n + 1
           self.session.endArray ()
           self.session.endGroup ()

   def reloadSession (self) :
       groups = self.session.childGroups ()
       for group in groups :
           group_name = str (group)
           if group_name.startswith ("edit") :
              self.session.beginGroup (group_name)
              fileName = self.session.value ("fileName", "", str)
              line = self.session.value ("line", 0, int)
              column = self.session.value ("column", 0, int)
              editor = self.loadFile (fileName) # , line, column)

              bookmarks = [ ]
              count = self.session.beginReadArray ("bookmarks")
              for inx in range (count) :
                  self.session.setArrayIndex (inx)
                  answer = Bookmark ()
                  answer.line = self.session.value ("line", 0, int)
                  answer.column = self.session.value ("column", 0, int)
                  answer.mark =  self.session.value ("mark", 0, int)
                  bookmarks.append (answer)
              self.session.endArray ()
              self.session.endGroup ()
              editor.setBookmarks (bookmarks)

   def checkSave (self, editor) :
       if not editor.isModified () or editor.closeWithoutQuestion :
          return True
       else :
          dialog = QMessageBox (self)
          dialog.setIcon (QMessageBox.Warning)
          dialog.setText ("The " + editor.getFileName () + " has been modified")
          dialog.setInformativeText ("Do you want to save your changes?")
          dialog.setStandardButtons (QMessageBox.Save | QMessageBox.Cancel | QMessageBox.Discard)
          dialog.setDefaultButton (QMessageBox.Save)
          answer = dialog.exec_ ()
          if answer == QMessageBox.Save :
             editor.writeFile (editor.getFileName ())
             return True
          elif answer == QMessageBox.Discard :
             return True
          else :
             return False

   def closeEvent (self, e) :
       self.storeSession ()
       for key in self.editors :
           editor = self.editors [key]
           if not self.checkSave (editor) :
              e.ignore ()
              return
       e.accept ()
       for w in self.window_list :
           if w != self:
              w.close ()

   # Editors

   def getCurrentTab (self) :
       return self.active_tab_widget.currentWidget ()

   def getEditor (self) :
       e = self.active_tab_widget.currentWidget ()
       if isinstance (e, Editor) :
          return e
       else :
          return None

   def hasExtension (self, editor, extension) :
       if editor == None :
          return False
       else :
          name, ext = os.path.splitext (editor.getFileName ())
          return ext == extension

   # Status Bar

   def showStatus (self, text) :
       self.status.showMessage (text)

   # def processEvents (self) :
   #     QApplication.processEvents ()

   # Memo and References

   def showMemo (self, editor, name) :
       pass

   def showReferences (self, editor, name) :
       pass

# --------------------------------------------------------------------------

class CentralApplication (QApplication) :
   def __init__ (self, * args) :
       QApplication.__init__ (self, *args)
       self.win = None
       self.report_click = None

   def notify (self, receiver, e) :
       if self.report_click != None :
          if e.type() == QEvent.MouseButtonPress :
             # print "Mouse Press"
             self.report_click (receiver)

       if e.type () == QEvent.KeyPress :
          modifiers = int (e.modifiers ())

          # key = int (e.key ())
          # if key in [Qt.Key_Print, Qt.Key_ScrollLock, Qt.Key_Pause, Qt.Key_SysReq] :
          #    print ("key", format (key, "32x"), format (modifiers, "08x"),  format (e.nativeModifiers (), "08x"))
          #    return False

          " Numeric Keypad "
          if modifiers & Qt.KeypadModifier != 0:
             key = int (e.key ())
             if key in [Qt.Key_Delete, Qt.Key_Insert,
                        Qt.Key_End, Qt.Key_Down, Qt.Key_PageDown,
                        Qt.Key_Left, Qt.Key_Clear, Qt.Key_Right,
                        Qt.Key_Home, Qt.Key_Up, Qt.Key_PageUp,
                        # Qt.Key_Slash, Qt.Key_Asterisk, Qt.Key_Minus, Qt.Key_Plus, Qt.Key_Enter,
                        ] :
                # print ("keypad", format (modifiers, "08x"),  format (e.nativeModifiers (), "08x"))
                t = self.win.alternate_tab_widget
                w = t.currentWidget ()
                if isinstance (w, Editor) :
                   mask = int (Qt.ShiftModifier | Qt.ControlModifier | Qt.AltModifier | Qt.MetaModifier)
                   mod = modifiers & mask
                   " no modifier "
                   if mod == 0 :
                      if key == Qt.Key_Home :     w.gotoBegin ()
                      if key == Qt.Key_End :      w.gotoEnd ()
                      if key == Qt.Key_Up :       w.scrollUp ()
                      if key == Qt.Key_Down :     w.scrollDown ()
                      if key == Qt.Key_PageUp :   w.gotoPageUp ()
                      if key == Qt.Key_PageDown : w.gotoPageDown ()
                      if key == Qt.Key_Left :     w.scrollLeft ()
                      if key == Qt.Key_Right :    w.scrollRight ()
                      if key == Qt.Key_Insert :   self.win.setPrevTab (t, passive = True)
                      if key == Qt.Key_Delete :   self.win.setNextTab (t, passive = True)
                      if key == Qt.Key_Clear :
                         # if QApplication.activeWindow () == self.win :
                         #    t.activateWindow ()
                         # else :
                         #    self.win.activateWindow ()
                         self.win.alternate_tab_widget = self.win.active_tab_widget
                         self.win.active_tab_widget = t
                         self.win.alternate_tab_widget.setStyleSheet ("QTabBar::tab:selected {color: orange }")
                         self.win.active_tab_widget.setStyleSheet ("QTabBar::tab:selected {color: red }")
                         # self.win.active_tab_widget.currentWidget().setFocus()
                         self.win.active_tab_widget.activateWindow()
                   " Ctrl "
                   if mod == Qt.ControlModifier :
                      if key == Qt.Key_Home :     self.win.leftToolHome ()
                      if key == Qt.Key_End :      self.win.leftToolEnd ()
                      if key == Qt.Key_Up :       self.win.leftToolUp ()
                      if key == Qt.Key_Down :     self.win.leftToolDown ()
                      if key == Qt.Key_PageUp :   self.win.rightToolUp ()
                      if key == Qt.Key_PageDown : self.win.rightToolDown ()
                      if key == Qt.Key_Left :     self.win.moveEditorLeft ()
                      if key == Qt.Key_Right :    self.win.moveEditorRight ()
                      if key == Qt.Key_Insert :   pass
                      if key == Qt.Key_Delete :   pass
                      if key == Qt.Key_Clear :    pass
                   " Alt "
                   if mod == Qt.AltModifier :
                      if key == Qt.Key_Home :     self.win.setFirstTab (t)
                      if key == Qt.Key_End :      self.win.setLastTab (t)
                      if key == Qt.Key_Left :     self.win.setPrevTab (t)
                      if key == Qt.Key_Right :    self.win.setNextTab (t)
                      if key == Qt.Key_Up :       w.gotoPrevFunction ()
                      if key == Qt.Key_Down :     w.gotoNextFunction ()
                      if key == Qt.Key_PageUp :   w.gotoPrevBookmark ()
                      if key == Qt.Key_PageDown : w.gotoNextBookmark ()
                      if key == Qt.Key_Insert :   pass
                      if key == Qt.Key_Delete :   pass
                      if key == Qt.Key_Clear :    pass
                   " Win "
                   if mod == Qt.MetaModifier:
                      if key == Qt.Key_Home :
                         self.win.alternate_tab_widget = self.win.firstTabWidget
                         # print ("left view")
                      if key == Qt.Key_End :
                         self.win.alternate_tab_widget = self.win.secondTabWidget
                         # print ("right view")
                      if key == Qt.Key_PageUp :
                         self.win.alternate_tab_widget = self.win.zoom_win.tabWidget
                         # print ("zoom win")
                      if key == Qt.Key_PageDown :
                         self.win.alternate_tab_widget = self.win.other_win.tabWidget
                         # print ("other win")
                return False
       " Mouse "
       if e.type () == QEvent.MouseButtonPress :
          b = e.button ()
          " additional mouse button on left side "
          if b == Qt.XButton1 :
             # print ("left button")
             self.win.setPrevTab ()
             return False
          " additional mouse button on right side "
          if b == Qt.XButton2 :
             # print ("right button")
             self.win.setNextTab ()
             return False
          if b == Qt.LeftButton :
             m = e.modifiers ()
             " Win + standard left button "
             if m == Qt.MetaModifier :
                w = receiver
                while w != None and not isinstance (w, TabWidget) :
                   w = w.parent ()
                if w != None :
                   if self.win.alternate_tab_widget != None :
                      self.win.alternate_tab_widget.setStyleSheet ("QTabBar::tab:selected {color: blue }")
                   w.setStyleSheet ("QTabBar::tab:selected {color: orange }")
                   self.win.alternate_tab_widget = w
                return False
       " Focus "
       if e.type () == QEvent.FocusIn :
          w = receiver
          while w != None and not isinstance (w, TabWidget) :
             w = w.parent ()
          if w != None :
             if w != self.win.active_tab_widget :
                if self.win.active_tab_widget != None :
                   if self.win.active_tab_widget == self.win.alternate_tab_widget :
                      self.win.active_tab_widget.setStyleSheet ("QTabBar::tab:selected {color: orange }")
                   else :
                      self.win.active_tab_widget.setStyleSheet ("QTabBar::tab:selected {color: blue }")
                w.setStyleSheet ("QTabBar::tab:selected {color: red }")
                self.win.active_tab_widget = w
       " default handler "
       return QApplication.notify (self, receiver, e)

   # def x11EventFilter (self, e) :
       # if et.type == KeyPress :
          # print ("key press")
       # return QApplication.x11EventFilter (self, e)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = CentralApplication (sys.argv)
   setApplStyle (app)

   win = CentralWindow ()
   win.additionalMenuItems ()

   app.win = win
   win.appl = app
   win.show ()

   win.info.redirectOutput ()

   win.loadFile ("examples/example.cc")
   win.loadFile ("examples/example.py")

   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
